var searchData=
[
  ['mask',['mask',['../structtimlCNNDropoutParams.html#a0572bfae6e5a9ca14e128f8de5f01e33',1,'timlCNNDropoutParams']]],
  ['max',['max',['../structtimlUtilInitializer.html#adeb987a00803c1c7218954618f1f56cc',1,'timlUtilInitializer']]],
  ['maxindex',['maxIndex',['../structtimlCNNPoolingParams.html#a2669235835b476d787b0da88a4f8c7fe',1,'timlCNNPoolingParams']]],
  ['mean',['mean',['../structtimlCNNInputParams.html#a39d74a3a7382c1b82ea64aa4289adbe3',1,'timlCNNInputParams::mean()'],['../structtimlUtilInitializer.html#a9c842edeeceb0c66a29f08d1fadebce8',1,'timlUtilInitializer::mean()'],['../structtimlUtilImageSet.html#afb9385a310c68071907dea070fcf37e2',1,'timlUtilImageSet::mean()']]],
  ['mempool',['memPool',['../struct__timlConvNeuralNetwork__.html#aa1364cc01763023a9442a647cf678dc6',1,'_timlConvNeuralNetwork_']]],
  ['mempoolsize',['memPoolSize',['../struct__timlConvNeuralNetwork__.html#aa5f6181a4aaf4d5241e4bbc923c0fb1f',1,'_timlConvNeuralNetwork_']]],
  ['min',['min',['../structtimlUtilInitializer.html#a2771e09d4ead50b451ed38e6cba99150',1,'timlUtilInitializer']]]
];
