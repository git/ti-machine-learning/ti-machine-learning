var searchData=
[
  ['data',['data',['../structtimlCNNDataSet.html#aedc507e5f143b3e5a925b245ba54cdd3',1,'timlCNNDataSet']]],
  ['delta',['delta',['../struct__timlCNNLayer__.html#a6efc77ceacdcb3db3160157794e244cd',1,'_timlCNNLayer_']]],
  ['denom',['denom',['../structtimlCNNNormParams.html#a787885ffb4a611b736875f31f3b42e09',1,'timlCNNNormParams']]],
  ['dim',['dim',['../structtimlCNNLinearParams.html#acd07cee7c3110f460541e4e1c073176a',1,'timlCNNLinearParams']]],
  ['dropoutparams',['dropoutParams',['../struct__timlCNNLayer__.html#addc500b652e0362ca23e60ff7eb937de',1,'_timlCNNLayer_']]]
];
