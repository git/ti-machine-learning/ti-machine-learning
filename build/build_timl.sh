################################################################################
#
# build_timl.sh
#
# Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#    Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
#    Neither the name of Texasset -e Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
################################################################################

################################################################################
#
# USE
#
#    ./build_timl.sh <timl_dir> <mcsdk_hpc_dir> <output_dir>
#
# NOTES
#
#
#
################################################################################

#!/bin/bash
set -e


##################### Param check ##############################################

if [ $# -ne 3 ]; then
  echo "Usage: $0 <timl_dir> <mcsdk_hpc_dir> <output_dir>";
  exit 1;
fi

cd $1
TIML_DIR=`pwd`
cd - 

cd $2
MCSDK_HPC_DIR=`pwd`
cd -

cd $3
OUTPUT_DIR=`pwd`
cd -

[ ! -z $MCSDK_HPC_DIR ] || { echo "Error: $MCSDK_HPC_DIR not set!"; exit 1; }
[ ! -z $TIML_DIR ] || { echo "Error: $TIML_DIR not set!"; exit 1; }
[ ! -z $OUTPUT_DIR ] || { echo "Error: $OUTPUT_DIR not set!"; exit 1; }


##################### env setup ##################################################

# two environment variables below need to be set by setup_hpc_env.sh
#export TI_OCL_INSTALL_DIR=/opt/ti/mcsdk-hpc_03_00_01_04/ti-opencl_0.14.0/
#export C6X_C_DIR=/home/uda0869574local/ti/c6000_7.6.0/include

cd $MCSDK_HPC_DIR/scripts
set +e
. setup_hpc_env.sh -s $TI_INSTALL_DIR:$TI_INSTALL_DIR/linaro -t $TARGET_ROOTDIR
set -e

echo "env" 
env


####################### Generate TIML debian package  ############################

cd $TIML_DIR
echo "Generating Debian package ..."
TIML_VER=`cat build/build_version.txt`
tar -czf timl_$TIML_VER.tar.gz --exclude='*.git' --exclude="*.obj" --exclude="*.o" --exclude="*.a" --exclude="*.bin" --exclude="*~" --files-from=build/build_files.txt
mkdir timl_$TIML_VER
tar -xf timl_$TIML_VER.tar.gz -C timl_$TIML_VER
tar -czf timl_$TIML_VER.tar.gz timl_$TIML_VER
cp -v timl_$TIML_VER.tar.gz $OUTPUT_DIR
rm -r timl_$TIML_VER


####################### Build TIML ###############################################

make build


####################### Generate TIML IPK package ############################

echo "Generating IPK ..."
mkdir -pv ./ipk_install
make ipk DESTDIR=./ipk_install

$MCSDK_HPC_DIR/mkrel/ipk/create_ipk.sh $MCSDK_HPC_DIR/mkrel/ipk/timl ./ipk_install
mv -v *.ipk $OUTPUT_DIR

