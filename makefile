################################################################################
#
# makefile
#
# Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#    Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
################################################################################


.PHONY: install build clean ipk

build: 
	cd ./build; make all; cd -;

install:	
	install -m 755 -d ${DESTDIR}/usr/include
	install -m 755 -d ${DESTDIR}/usr/lib
	install -m 755 -d ${DESTDIR}/usr/share/ti/examples/timl
	install -m 755 -d ${DESTDIR}/usr/share/doc/timl
	install -m 755 -d ${DESTDIR}/usr/src/timl
	install -m 755 -d ${DESTDIR}/usr/src/timl/build
	cd ./build; make install DESTDIR=${DESTDIR}; cd -;
	cp ./build/build_app ${DESTDIR}/usr/share/ti/examples/timl/Makefile
	cp -r ./src/app ${DESTDIR}/usr/share/ti/examples/timl/app
	cp -r ./src/database ${DESTDIR}/usr/share/ti/examples/timl/database
	cp -r ./doc ${DESTDIR}/usr/src/timl
	cp -r ./doc/timl.pdf ${DESTDIR}/usr/share/doc/timl
	cp -r ./bin ${DESTDIR}/usr/src/timl
	cp -r ./src ${DESTDIR}/usr/src/timl
	cp ./build/makefile ${DESTDIR}/usr/src/timl/build/Makefile
	cp ./build/build_version.txt ${DESTDIR}/usr/src/timl/build
 
ipk:
	install -m 755 -d ${DESTDIR}/usr/include
	install -m 755 -d ${DESTDIR}/usr/lib
	install -m 755 -d ${DESTDIR}/usr/share/ti/examples/timl
	install -m 755 -d ${DESTDIR}/usr/share/doc/timl
	install -m 755 -d ${DESTDIR}/usr/src/timl
	install -m 755 -d ${DESTDIR}/usr/src/timl/build
	cd ./build; make install DESTDIR=${DESTDIR}; cd -;
	cp ./build/build_app ${DESTDIR}/usr/share/ti/examples/timl/Makefile
	cp -r ./src/app ${DESTDIR}/usr/share/ti/examples/timl/app
	cp -r ./src/database ${DESTDIR}/usr/share/ti/examples/timl/database
	cp -r ./doc ${DESTDIR}/usr/src/timl
	cp -r ./doc/timl.pdf ${DESTDIR}/usr/share/doc/timl
	cp -r ./bin ${DESTDIR}/usr/src/timl
	cp -r ./src ${DESTDIR}/usr/src/timl
	cp ./build/makefile ${DESTDIR}/usr/src/timl/build/Makefile
	cp ./build/build_version.txt ${DESTDIR}/usr/src/timl/build

clean:
	cd ./build; make clean; cd -;

