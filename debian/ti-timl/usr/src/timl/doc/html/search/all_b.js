var searchData=
[
  ['patchsize',['patchSize',['../structappCNNSceneDataSet.html#a056a6e767d7d0d43ea1f18d0c5688c9a',1,'appCNNSceneDataSet']]],
  ['prev',['prev',['../struct__timlCNNLayer__.html#a1c5210fa230c366b390732386064eeb8',1,'_timlCNNLayer_']]],
  ['prevdim',['prevDim',['../structtimlCNNLinearParams.html#ae13aaf42aad15b64b3ad50dbf039e2a1',1,'timlCNNLinearParams']]],
  ['prevfeaturemapreshape',['prevFeatureMapReshape',['../structtimlCNNConvParams.html#a177849440786d4c2f3e1a083e164bc34',1,'timlCNNConvParams']]],
  ['prevfeaturemapreshapeindex',['prevFeatureMapReshapeIndex',['../structtimlCNNConvParams.html#aa25d829029e1fd35df54c92c1bdc2028',1,'timlCNNConvParams']]],
  ['prob',['prob',['../structtimlCNNDropoutParams.html#a37847e94544923a1dc888306e0bc9e2e',1,'timlCNNDropoutParams']]]
];
