var searchData=
[
  ['scalecol',['scaleCol',['../structtimlCNNPoolingParams.html#a133453423032b3422922d6ad9e9ce681',1,'timlCNNPoolingParams']]],
  ['scalerow',['scaleRow',['../structtimlCNNPoolingParams.html#add78009b025b15de78ef69a0ef44d591',1,'timlCNNPoolingParams']]],
  ['shared',['shared',['../structtimlCNNLinearParams.html#aef1359b54369b95cd21ad069d3109d26',1,'timlCNNLinearParams::shared()'],['../structtimlCNNInputParams.html#a903f1cd3c732a0a9420937bf278ee6d0',1,'timlCNNInputParams::shared()']]],
  ['std',['std',['../structtimlUtilInitializer.html#a37e42634a42df16a5f460b266bce968a',1,'timlUtilInitializer']]],
  ['stridex',['strideX',['../structtimlCNNPoolingParams.html#adeabf9e05e8fce29f0232a8362038224',1,'timlCNNPoolingParams']]],
  ['stridey',['strideY',['../structtimlCNNPoolingParams.html#a27c516ed5dd5692d92f8f31108a55e34',1,'timlCNNPoolingParams']]]
];
