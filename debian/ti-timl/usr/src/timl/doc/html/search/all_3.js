var searchData=
[
  ['channel',['channel',['../structtimlCNNInputParams.html#a4eab5767395e0c043b677bad2bbc4562',1,'timlCNNInputParams']]],
  ['channelpermute',['channelPermute',['../structtimlCNNInputParams.html#a4ef62950f27f476170744c50d18daade',1,'timlCNNInputParams']]],
  ['cnn',['cnn',['../group__cnn.html',1,'']]],
  ['col',['col',['../structtimlCNNInputParams.html#a86376cd441d9f3a2c1d041282b0ba330',1,'timlCNNInputParams']]],
  ['connectivity',['connectivity',['../structtimlCNNConvParams.html#af13df46a47c988639b3c3030f467ec58',1,'timlCNNConvParams']]],
  ['costtype',['costType',['../structtimlCNNTrainingParams.html#a7037ed9b5aa77cb921f21fa338abf26e',1,'timlCNNTrainingParams']]],
  ['count',['count',['../structtimlCNNTrainingParams.html#ae54e9bf53e695070047e6fe4711d42e2',1,'timlCNNTrainingParams']]]
];
