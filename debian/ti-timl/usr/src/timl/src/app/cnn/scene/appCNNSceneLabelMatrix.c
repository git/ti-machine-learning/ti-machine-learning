/******************************************************************************/
/*!
 * \file appCNNSceneLabelMatrix.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "appCNNScene.h"
#ifdef TIML_ALT
#include "../../alt/timlAlt.h"
#include "../../altApp/sl/appSLAlt.h"
#endif


/******************************************************************************/
/*!
 * \ingroup    appCNNScene
 * \brief      Fill the label matrix
 * \param[in]  map         Feature map output of a CNN
 * \param[in]  row         Image row
 * \param[in]  col         Image col
 * \param[in]  channel     Image channel
 * \param[in]  m           Row Index
 * \param[in]  k           Col Index
 * \param[out] labelMatrix Label matrix
 * \param[in]  numRow      Num of rows
 * \param[in]  numCol      Num of cols
 * \return     Error code
 */
/******************************************************************************/

int appCNNSceneLabelMatrix(float *map, int row, int col, int channel, int m, int k, int *labelMatrix, int numRow, int numCol)
{
#ifdef TIML_CPU
   int   i;
   int   j;
   int   p;
   float max;
   int resolutionLossRow;
   int resolutionLossCol;
   int err;

   err = 0;
   resolutionLossRow = numRow/row;
   resolutionLossCol = numCol/col;

   for (i = 0; i < row; i++) {
      for (j = 0; j < col; j++) {
         max = -FLT_MAX;
         for (p = 0; p < channel; p++) {
            if (map[j + i*col + p*row*col] >= max) {
               labelMatrix[k + resolutionLossCol/2 + j*resolutionLossCol + (m + resolutionLossRow/2 + i*resolutionLossRow)*numCol] = p;
               max = map[j + i*col + p*row*col];
            }
         }
      }
   }
   return err;

#elif defined TIML_ALT
   return appSLLabelMatrixAlt(map, row, col, channel, m, k, labelMatrix, numRow, numCol);
#endif
}
