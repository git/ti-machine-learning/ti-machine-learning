/******************************************************************************/
/*!
 * \file appCNNClassCIFAR10Testing.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../appCNNClass.h"


/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define MODEL_PATH       "../../../../database/model/cifar10/databaseModelCIFAR10.m"
#define DATABASE_PATH    "../../../../database/cifar10"
#define TOP_N            1
#define IMAGE_ROW        32
#define IMAGE_COL        32
#define IMAGE_CHANNEL    3


/*******************************************************************************
 *
 * main()
 *
 ******************************************************************************/

int main()
{
   return appCNNClassCIFAR10Testing();
}


/******************************************************************************/
/*!
 * \ingroup appCNNClass
 * \brief   CIFAR10 testing example
 */
/******************************************************************************/

int appCNNClassCIFAR10Testing()
{
   int              err;
   int              classifyNum;
   float            classifyPercent;
   int              dim;
   long             mem;
   struct timespec  startTime;
   struct timespec  endTime;
   long             testingTime;
   int              topN;
   int              *label;
   timlUtilImageSet training;
   timlUtilImageSet testing;

   // init
   err         = 0;
   dim         = IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL;
   classifyNum = 0;
   topN        = TOP_N;

   setbuf(stdout, NULL); // do not buffer the console output

   // read the CNN config file
   printf("1. Read the CNN config\n");
   timlConvNeuralNetwork *cnn = timlCNNReadFromFile(MODEL_PATH, 0);
   timlCNNSetMode(cnn, Util_Test);
   mem = timlCNNMemory(cnn);
   timlCNNPrint(cnn);
   printf("CNN memory allocation = %.10f MB.\n", (float)mem/1024.0/1024.0);
   printf("CNN parameter #       = %lu.\n", timlCNNGetParamsNum(cnn));

   // read CIFAR10 database
   printf("2. Read CIFAR10 database\n");
   timlUtilReadCIFAR10(DATABASE_PATH, &training, &testing);

   // testing
   printf("3. Start testing\n");
   label = malloc(sizeof(int)*topN*testing.num);
   clock_gettime(CLOCK_REALTIME, &startTime);
   timlCNNClassifyTopNBatchMode(cnn, testing.data, dim, testing.num, label, NULL, topN);
   clock_gettime(CLOCK_REALTIME, &endTime);
   testingTime = timlUtilDiffTime(startTime, endTime);
   classifyNum = timlUtilClassifyAccuracy(label, topN, testing.num, testing.label);
   classifyPercent = (float)classifyNum/(float)testing.num;
   printf("Testing time      = %.3f s\n", testingTime/1000000.0);
   printf("Classify accuracy = %.3f %%\n", classifyPercent*100.00);

   // cleaning
   printf("4. Clean up\n");
   free(training.data);
   free(training.label);
   free(testing.data);
   free(testing.label);
   free(label);
   timlCNNDelete(cnn);

   return err;
}
