/******************************************************************************/
/*!
 * \file appCNNSceneClassify.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "appCNNScene.h"


/******************************************************************************/
/*!
 * \ingroup       appCNNScene
 * \brief         Pixel label classification
 * \param[in,out] cnn         CNN
 * \param[in]     image       Image
 * \param[in,out] labelMatrix Generated label matrix
 * \param[in]     scale       Down scaling factor of the label matrix
 * \return        Error code
 */
/******************************************************************************/

int appCNNSceneClassify(timlConvNeuralNetwork *cnn, timlUtilImage image, int *labelMatrix, int scale)
{
   int   i;
   int   j;
   int   p;
   int   m;
   int   k;
   int   err;
   int   numRow;
   int   numCol;
   int   numChannel;
   float imageMean;
   float imageDeviation;
   int   paddedRow;
   int   paddedCol;
   int   paddedDim;
   float *paddedImage;
   int   resolutionLossRow;
   int   resolutionLossCol;
   int   rowStart;
   int   rowEnd;
   int   colStart;
   int   colEnd;
   int   rowDown;
   int   colDown;

   // init
   err               = 0;
   numRow            = image.row;
   numCol            = image.col;
   numChannel        = image.channel;
   paddedRow         = cnn->head->row;
   paddedCol         = cnn->head->col;
   paddedDim         = paddedRow*paddedCol*numChannel;
   paddedImage       = malloc(sizeof(float)*paddedDim);
   resolutionLossRow = numRow/cnn->tail->row;
   resolutionLossCol = numCol/cnn->tail->col;

   // image normalization (per image)
   for (k = 0; k < numChannel; k++) {
      imageMean = 0.0;
      imageDeviation = 0.0;
      for (i = 0; i < numRow * numCol; i++) {
         imageMean += image.data[i + k*numRow*numCol];
      }
      imageMean /= numRow*numCol;
      for (i = 0; i < numRow*numCol; i++) {
         image.data[i + k*numRow*numCol] -= imageMean;
      }
      for (i = 0; i < numRow * numCol; i++) {
         imageDeviation += image.data[i + k*numRow*numCol] * image.data[i + k*numRow*numCol];
      }
      imageDeviation /= numRow*numCol;
      imageDeviation = sqrtf(imageDeviation);
      for (i = 0; i < numRow*numCol; i++) {
         image.data[i + k*numRow*numCol] /= imageDeviation;
      }
   }

   // main loop over each pixel on the image
   for (m = -resolutionLossRow/2; m < resolutionLossRow/2; m += scale) {
      for (k = -resolutionLossCol/2; k < resolutionLossCol/2; k += scale) {
         rowStart = (paddedRow - numRow)/2 - m;
         rowEnd = rowStart + numRow - 1;
         colStart = (paddedCol - numCol)/2 - k;
         colEnd = colStart + numCol - 1;

         // zero padding
         for (i = 0; i < paddedRow; i++) {
            for (j = 0; j < paddedCol; j++) {
               if (i < rowStart || i > rowEnd || j < colStart || j > colEnd) {
                  for (p = 0; p < numChannel; p++)
                     paddedImage[j + i*paddedCol + p*paddedRow*paddedCol] = 0.0;
               }
               else {
                  for (p = 0; p < numChannel; p++)
                     paddedImage[j + i*paddedCol + p*paddedRow*paddedCol] = image.data[j - colStart + (i - rowStart)*numCol + p*numRow*numCol];
               }
            }
         }

         // cnn Forward Propagation
         err = timlCNNForwardPropagation(cnn, paddedImage, paddedDim);

         // labeling
         appCNNSceneLabelMatrix(cnn->tail->featureMap, cnn->tail->row, cnn->tail->col, cnn->tail->channel, m, k, labelMatrix, numRow, numCol);
      }
   }

   // up-sample the label matrix
   for (i = 0; i < image.row; i++) {
      for (j = 0; j < image.col; j++) {
         rowDown = i/scale;
         colDown = j/scale;
         labelMatrix[j + i*image.col] = labelMatrix[colDown*scale + rowDown*scale*image.col];
      }
   }

   free(paddedImage);

   return err;
}
