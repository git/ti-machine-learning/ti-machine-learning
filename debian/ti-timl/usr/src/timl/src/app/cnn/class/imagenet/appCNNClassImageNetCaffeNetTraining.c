/******************************************************************************/
/*!
 * \file appCNNClassImageNetCaffeNetTraining.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../appCNNClass.h"


/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define LABEL_PATH       "../../../../database/imagenet/train/label.txt"
#define IMAGE_PATH       "../../../../database/imagenet/train/%010d.jpg"
#define TOP_N            5
#define IMAGE_NUM        100
#define IMAGE_BATCH_SIZE 10
#define IMAGE_ROW        256
#define IMAGE_COL        256
#define IMAGE_CROP_ROW   227
#define IMAGE_CROP_COL   227
#define IMAGE_CHANNEL    3


/*******************************************************************************
 *
 * main()
 *
 ******************************************************************************/

int main()
{
	return appCNNClassImageNetCaffeNetTraining();
}


/******************************************************************************/
/*!
 * \ingroup appCNNClass
 * \brief   CaffeNet training example
 */
/******************************************************************************/

int appCNNClassImageNetCaffeNetTraining()
{
   int                   i;
   int                   j;
   int                   k;
   int                   read;
   timlConvNeuralNetwork *cnn;
   int                   err;
   timlUtilImage         image;
   int                   *trainLabel;
   float                 *trainImage;
   char                  str[TIML_UTIL_MAX_STR];
   int                   dim;
   int                   trainImageBatchNum;
   timlCNNInputParams    inputParams;
   timlCNNConvParams     convParams;
   struct timespec       startTime;
   struct timespec       endTime;
   long                  trainingTime;
   long                  mem;
   long                  paramsNum;
   FILE                  *fp;

   // init
   err                = 0;
   dim                = IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL;
   trainImageBatchNum = IMAGE_NUM/IMAGE_BATCH_SIZE;
   setbuf(stdout, NULL); // do not buffer the console output

   // setup CNN
   printf("1. Build up CNN\n");
   cnn = timlCNNCreateConvNeuralNetwork(timlCNNTrainingParamsDefault(), 0);
   cnn->params.batchSize = IMAGE_BATCH_SIZE;
   cnn->params.learningRate = 0.1;
   inputParams = timlCNNInputParamsDefault();
   inputParams.row = IMAGE_ROW;
   inputParams.col = IMAGE_COL;
   inputParams.channel = IMAGE_CHANNEL;
   inputParams.mean = NULL;
   timlCNNAddInputLayer(cnn, IMAGE_CROP_ROW, IMAGE_CROP_COL, IMAGE_CHANNEL, inputParams);       // (1) input layer
   timlCNNAddConvLayer(cnn, 11, 11, 4, 4, 96, timlCNNConvParamsDefault());                      // (2) conv layer
   timlCNNAddNonlinearLayer(cnn, Util_Relu);                                                    // (3) relu layer
   timlCNNAddPoolingLayer(cnn, 3, 3, 2, 2, CNN_MaxPooling, timlCNNPoolingParamsDefault());      // (4) max pooling layer
   timlCNNAddNormLayer(cnn, timlCNNNormParamsDefault());                                        // (5) norm layer
   convParams = timlCNNConvParamsDefault();
   convParams.padUp = 2;
   convParams.padDown = 2;
   convParams.padLeft = 2;
   convParams.padRight = 2;
   timlCNNAddConvLayer(cnn, 5, 5, 1, 1, 256, convParams);                                       // (6) conv layer
   timlCNNAddNonlinearLayer(cnn, Util_Relu);                                                    // (7) relu layer
   timlCNNAddPoolingLayer(cnn, 3, 3, 2, 2, CNN_MaxPooling, timlCNNPoolingParamsDefault());      // (8) max pooling layer
   timlCNNAddNormLayer(cnn, timlCNNNormParamsDefault());                                        // (9) norm layer
   convParams = timlCNNConvParamsDefault();
   convParams.padUp = 1;
   convParams.padDown = 1;
   convParams.padLeft = 1;
   convParams.padRight = 1;
   timlCNNAddConvLayer(cnn, 3, 3, 1, 1, 384, convParams);                                       // (10) conv layer
   timlCNNAddNonlinearLayer(cnn, Util_Relu);                                                    // (11) relu layer
   convParams = timlCNNConvParamsDefault();
   convParams.padUp = 1;
   convParams.padDown = 1;
   convParams.padLeft = 1;
   convParams.padRight = 1;
   timlCNNAddConvLayer(cnn, 3, 3, 1, 1, 384, convParams);                                      // (12) conv layer
   timlCNNAddNonlinearLayer(cnn, Util_Relu);                                                   // (13)relu layer
   convParams = timlCNNConvParamsDefault();
   convParams.padUp = 1;
   convParams.padDown = 1;
   convParams.padLeft = 1;
   convParams.padRight = 1;
   timlCNNAddConvLayer(cnn, 3, 3, 1, 1, 256, convParams);                                      // (14) conv layer
   timlCNNAddNonlinearLayer(cnn, Util_Relu);                                                   // (15) relu layer
   timlCNNAddPoolingLayer(cnn, 3, 3, 2, 2, CNN_MaxPooling, timlCNNPoolingParamsDefault());     // (16) max pooling layer
   timlCNNAddLinearLayer(cnn, 4096, timlCNNLinearParamsDefault());                             // (17) linear layer
   timlCNNAddNonlinearLayer(cnn, Util_Relu);                                                   // (18) relu layer
   timlCNNAddDropoutLayer(cnn, 0.5);                                                           // (19) dropout layer
   timlCNNAddLinearLayer(cnn, 4096, timlCNNLinearParamsDefault());                             // (20) linear layer
   timlCNNAddNonlinearLayer(cnn, Util_Relu);                                                   // (21) relu layer
   timlCNNAddDropoutLayer(cnn, 0.5);                                                           // (22) dropout layer
   timlCNNAddLinearLayer(cnn, 1000, timlCNNLinearParamsDefault());                             // (23) linear layer
   timlCNNAddNonlinearLayer(cnn, Util_Softmax);                                                // (24) softmax layer
   timlCNNInitialize(cnn);
   timlCNNReset(cnn);
   mem = timlCNNMemory(cnn);
   timlCNNPrint(cnn);
   printf("CNN memory   = %.10f MB.\n", (float)mem/1024.0/1024.0);
   paramsNum = timlCNNGetParamsNum(cnn);
   printf("CNN params # = %ld.\n", paramsNum);

   // read test data
   printf("2. Start training\n");
   trainLabel = malloc(sizeof(int)*IMAGE_BATCH_SIZE);
   trainImage = malloc(sizeof(float)*dim*IMAGE_BATCH_SIZE);
   timlCNNSetMode(cnn, Util_Train);
   fp = fopen(LABEL_PATH, "rt");
   clock_gettime(CLOCK_REALTIME, &startTime);

   // read one batch of images at a time
   for (k = 0; k < trainImageBatchNum; k++) {
      // read train labels
      for (i = 0; i < IMAGE_BATCH_SIZE; i++) {
         read = fscanf(fp, "%d", trainLabel + i);
      }

      // read train images
      for (i = 0; i < IMAGE_BATCH_SIZE; i++) {
         sprintf(str, IMAGE_PATH, k*IMAGE_BATCH_SIZE + i);
         image = timlUtilReadJPEG(str);
         if (image.channel == 1) { // duplicate channels
            for (j = 0; j < IMAGE_CHANNEL; j++) {
               cblas_scopy(IMAGE_ROW*IMAGE_COL, image.data, 1, trainImage + i*dim + j*IMAGE_ROW*IMAGE_COL, 1);
            }
         }
         else {
            cblas_scopy(dim, image.data, 1, trainImage + i*dim, 1);
         }
         free(image.data);
      }

      // training
      timlCNNSupervisedTrainingWithLabelBatchMode(cnn, trainImage, trainLabel, dim, IMAGE_BATCH_SIZE);
   }
   clock_gettime(CLOCK_REALTIME, &endTime);
   trainingTime = timlUtilDiffTime(startTime, endTime);
   printf("Training time = %.2fs.\n", trainingTime/1000000.0);

   // clean up
   printf("4. Clean up\n");
   fclose(fp);
   timlCNNDelete(cnn);
   free(trainLabel);
   free(trainImage);

   return err;
}
