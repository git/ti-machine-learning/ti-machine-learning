/******************************************************************************/
/*!
 * \file appCNNSceneClassifyOpenMP.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "appCNNScene.h"


/******************************************************************************/
/*!
 * \ingroup       appCNNScene
 * \brief         Supervised training on the dataset using openmp
 * \param[in,out] cnnTeam     An array of CNNs that share the same parameters
 * \param[in]     teamNum     Team number
 * \param[in]     data        Image data
 * \param[in]     row         Image row
 * \param[in]     col         Image col
 * \param[in]     channel     Image channel
 * \param[in,out] labelMatrix Generated label matrix
 * \param[in] scale           Down scaling factor of the label matrix
 * \return        Error code
 */
/******************************************************************************/

int appCNNSceneClassifyOpenMP(timlConvNeuralNetwork **cnnTeam, int teamNum, float *data, int row, int col, int channel, int *labelMatrix, int scale)
{
   int   i;
   int   j;
   int   p;
   int   m;
   int   k;
   int   err;
   float imageMean;
   float imageDeviation;
   int   paddedRow;
   int   paddedCol;
   int   paddedDim;
   float *paddedImage;
   int   resolutionLossRow;
   int   resolutionLossCol;
   int   rowStart;
   int   rowEnd;
   int   colStart;
   int   colEnd;
   int   rowDown;
   int   colDown;
   int   threadIndex;

   // init
   err               = 0;
   paddedRow         = cnnTeam[0]->head->row;
   paddedCol         = cnnTeam[0]->head->col;
   paddedDim         = paddedRow*paddedCol*channel;
   paddedImage       = malloc(sizeof(float)*paddedDim*teamNum);
   resolutionLossRow = row/cnnTeam[0]->tail->row;
   resolutionLossCol = col/cnnTeam[0]->tail->col;

   // image normalization (per image)
   for (k = 0; k < channel; k++) {
      imageMean = 0.0;
      imageDeviation = 0.0;
      for (i = 0; i < row*col; i++) {
         imageMean += data[i + k*row*col];
      }
      imageMean /= row*col;
      for (i = 0; i < row*col; i++) {
         data[i + k*row*col] -= imageMean;
      }
      for (i = 0; i < row*col; i++) {
         imageDeviation += data[i + k*row*col] * data[i + k*row*col];
      }
      imageDeviation /= row*col;
      imageDeviation = sqrtf(imageDeviation);
      for (i = 0; i < row*col; i++) {
         data[i + k*row*col] /= imageDeviation;
      }
   }

   #pragma omp parallel num_threads(teamNum) private(threadIndex, m, k, i, j, p)
   {
      #pragma omp for collapse(2)

      // main loop over each pixel on the image
      for (m = -resolutionLossRow/2; m < resolutionLossRow/2; m += scale) {
         for (k = -resolutionLossCol/2; k < resolutionLossCol/2; k += scale) {
            threadIndex = omp_get_thread_num();
            rowStart = (paddedRow - row)/2 - m;
            rowEnd = rowStart + row - 1;
            colStart = (paddedCol - col)/2 - k;
            colEnd = colStart + col - 1;

            // zero padding
            for (i = 0; i < paddedRow; i++) {
               for (j = 0; j < paddedCol; j++) {
                  if (i < rowStart || i > rowEnd || j < colStart || j > colEnd) {
                     for (p = 0; p < channel; p++)
                        paddedImage[j + i*paddedCol + p*paddedRow*paddedCol + paddedDim*threadIndex] = 0.0;
                  }
                  else {
                     for (p = 0; p < channel; p++)
                        paddedImage[j + i*paddedCol + p*paddedRow*paddedCol + paddedDim*threadIndex] = data[j - colStart + (i - rowStart)*col + p*row*col];
                  }
               }
            }

            // cnn Forward Propagation
            err = timlCNNForwardPropagation(cnnTeam[threadIndex], paddedImage + paddedDim*threadIndex, paddedDim);

            // labeling
            appCNNSceneLabelMatrix(cnnTeam[threadIndex]->tail->featureMap, cnnTeam[threadIndex]->tail->row, cnnTeam[threadIndex]->tail->col, cnnTeam[threadIndex]->tail->channel, m, k, labelMatrix, row, col);
         } // end of k loop
      } // end of m loop
   } // end of OpenMP

   // up-sampling label matrix
   for (i = 0; i < row; i++) {
      for (j = 0; j < col; j++) {
         rowDown = i / scale;
         colDown = j / scale;
         labelMatrix[j + i*col] = labelMatrix[colDown*scale + rowDown*scale*col];
      }
   }

   free(paddedImage);

   return err;

}

