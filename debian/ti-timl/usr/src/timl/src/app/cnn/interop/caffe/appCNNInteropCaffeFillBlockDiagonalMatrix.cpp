/******************************************************************************/
/*!
 * \file appCNNInteropCaffeFillBlockDiagonalMatrix.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 *  INCLUDES
 *
 ******************************************************************************/

#include "appCNNInteropCaffe.hpp"


/******************************************************************************/
/*!
 * \ingroup   appCNNInteropCaffe
 * \brief     Fill a block diagonal matrix
 * \param[out] a     Block diagonal matrix
 * \param[in]  M     Rows of a
 * \param[in]  N     Cols of a
 * \param[in]  group Number of groups
 * \param[in]  b     Diagonal blocks
 * \return     Error code
 */
/******************************************************************************/

int appCNNInteropCaffeFillBlockDiagonalMatrix(float *a, int M, int N, int group, float *b)
{
   int i;
   int j;
   int m;
   int aIndex;
   int bIndex;
   timlUtilVectorResetFloat(a, M*N, 0.0, 0, 0);
   for (m = 0; m < group; m++) {
      for (i = 0; i < M/group; i++) {
         for (j = 0; j < N/group; j++) {
            aIndex = (m*M/group + i)*N + (m*N/group + j);
            bIndex = m*M*N/group/group + i*N/group + j;
            a[aIndex] = b[bIndex];
         }
      }
   }

   return 0;
}

