/******************************************************************************/
/*!
 * \file appCNNInteropCaffeFlipMatrixFloat.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 *  INCLUDES
 *
 ******************************************************************************/

#include "appCNNInteropCaffe.hpp"


/******************************************************************************/
/*!
 * \ingroup       appCNNInteropCaffe
 * \brief         Flip a matrix
 * \param[in,out] a Matrix
 * \param[in]     m Rows
 * \param[in]     n Cols
 * \return        Error code
 */
/******************************************************************************/

int appCNNInteropCaffeFlipMatrixFloat(float *a, int m, int n)
{
   int i;
   int j;
   // flipud
   for (i = 0; i < m/2; i++) {
      for (j = 0; j < n; j++) {
         swap(a[i*n + j], a[(m - i - 1)*n + j]);
      }
   }
   // fliplr
   for (i = 0; i < m; i++) {
      for (j = 0; j < n/2; j++) {
         swap(a[i*n + j], a[i*n + (n - j - 1)]);
      }
   }

   return 0;
}
