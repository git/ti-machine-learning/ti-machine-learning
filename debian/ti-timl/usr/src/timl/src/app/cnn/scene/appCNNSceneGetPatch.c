/******************************************************************************/
/*!
 * \file appCNNSceneGetPatch.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "appCNNScene.h"


/******************************************************************************/
/*!
 * \ingroup       appCNNScene
 * \brief         Return the image patch for (image, row, col) index combination
 * \param[in]     imageIdx image index
 * \param[in]     rowIdx row index
 * \param[in]     colIdx col index
 * \param[in]     dataSet data set
 * \param[in,out] patch image patch
 * \return        Error code
 */
/******************************************************************************/

int appCNNSceneGetPatch(int imageIdx, int rowIdx, int colIdx, appCNNSceneDataSet *dataSet, float *patch)
{
   int           i;
   int           j;
   int           k;
   int           imageRow;
   int           imageCol;
   int           numRow;
   int           numCol;
   int           numChannel;
   int           patchSize;
   float         imageMean;
   float         imageDeviation;
   timlUtilImage image;
   char          str[TIML_UTIL_MAX_STR];

   // init
   numRow         = dataSet->row;
   numCol         = dataSet->col;
   numChannel     = dataSet->channel;
   patchSize      = dataSet->patchSize;
   imageMean      = 0;
   imageDeviation = 0;

   sprintf(str, dataSet->imageFileNameStr, imageIdx);
   image = timlUtilReadJPEG(str);

   //	 normalization
   for (k = 0; k < numChannel; k++) {
      for (i = 0; i < numRow*numCol; i++) {
         imageMean += image.data[i + k*numRow*numCol];
      }
      imageMean /= numRow*numCol;
      for (i = 0; i < numRow*numCol; i++) {
         image.data[i + k*numRow*numCol] -= imageMean;
      }
      for (i = 0; i < numRow*numCol; i++) {
         imageDeviation +=
            image.data[i + k*numRow*numCol] * image.data[i + k*numRow*numCol];
      }
      imageDeviation /= numRow*numCol;
      imageDeviation = sqrt(imageDeviation);
      for (i = 0; i < numRow*numCol; i++) {
         image.data[i + k*numRow*numCol] /= imageDeviation;
      }
   }

   // loading patch
   for (i = 0; i < patchSize; i++) {
      for (j = 0; j < patchSize; j++) {
         imageRow = rowIdx - patchSize/2 + i;
         imageCol = colIdx - patchSize/2 + j;
         if (imageRow < 0 || imageRow >= numRow || imageCol < 0 || imageCol >= numCol) {
            for (k = 0; k < numChannel; k++) {
               patch[j + i*patchSize + k*patchSize*patchSize] = 0.0;
            }
         }
         else {
            for (k = 0; k < numChannel; k++)
               patch[j + i*patchSize + k*patchSize*patchSize] = image.data[imageCol + imageRow*numCol + k*numRow*numCol];
         }
      }
   }

   free(image.data);
   return 0;

}
