/******************************************************************************/
/*!
 * \file appCNNInteropCaffeConvert.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 *  INCLUDES
 *
 ******************************************************************************/

#include "appCNNInteropCaffe.hpp"


/******************************************************************************/
/*!
 * \ingroup   appCNNInteropCaffe
 * \brief     Convert Caffe to TIML CNN
 * \param[in] netStructurePrototxtFileName Net structure prototxt file name
 * \param[in] netParamPrototxtFileName     Net params prototxt file name
 * \return    CNN
 */
/******************************************************************************/

timlConvNeuralNetwork* appCNNInteropCaffeConvert(const char *netStructurePrototxtFileName, const char *netParamPrototxtFileName)
{
   FILE                   *fp;
   float                  *data;
   int                    layerNum;
   int                    row;
   int                    col;
   int                    channel;
   int                    i;
   int                    offset;
   SolverParameter        solverParam;
   NetParameter           netStructure;
   NetParameter           netParam;
   BlobProto              blob;
   timlCNNLayerType       cnnLayerType;
   timlUtilActivationType actType;
   timlCNNPoolingType     poolingType;
   timlCNNTrainingParams  trainingParams;
   timlConvNeuralNetwork  *cnn;;
   int k;

   cnn = timlCNNCreateConvNeuralNetwork(timlCNNTrainingParamsDefault(), 0);
   appCNNInteropCaffeReadProtoFromTextFile(netStructurePrototxtFileName, &netStructure);
   appCNNInteropCaffeReadProtoFromBinaryFile(netParamPrototxtFileName, &netParam);
   layerNum = netStructure.layers_size();

   // write input layer
   row = netStructure.input_dim(2);
   col = netStructure.input_dim(3);
   channel = netStructure.input_dim(1);
   timlCNNAddInputLayer(cnn, row, col, channel, timlCNNInputParamsDefault());
   timlCNNInputInitialize(cnn->tail);

   // for alexnet and caffenet, netStructure.layers(0).type() == conv, netParam.layers(0).type() == data --> offset = 1
   // for vggnet,               netStructure.layers(0).type() == conv, netParam.layers(0).type() == conv --> offset = 0
   // make sure that both netParam and netStructure start with conv layer, the data layer is skipped
   if (netParam.layers(0).type() == LayerParameter_LayerType_DATA) {
      offset = 1;
   }
   else {
      offset = 0;
   }

   // convert all layer types
   for (i = 0; i < layerNum; i++) {
      printf("Converting layer %d\n", i);
      cnnLayerType = appCNNInteropCaffeLayerTypeConvert(netStructure.layers(i).type());
      switch (cnnLayerType) {
      case CNN_Conv:
         appCNNInteropCaffeConvLayerConvert(cnn, netStructure.layers(i), netParam.layers(i + offset));
         break;
      case CNN_Pooling:
         appCNNInteropCaffePoolingLayerConvert(cnn, netStructure.layers(i), netParam.layers(i + offset));
         break;
      case CNN_Norm:
         appCNNInteropCaffeNormLayerConvert(cnn, netStructure.layers(i), netParam.layers(i + offset));
         break;
      case CNN_Nonlinear:
         appCNNInteropCaffeNonlinearLayerConvert(cnn, netStructure.layers(i), netParam.layers(i + offset));
         break;
      case CNN_Linear:
         appCNNInteropCaffeLinearLayerConvert(cnn, netStructure.layers(i), netParam.layers(i + offset));
         break;
      case CNN_Dropout:
         appCNNInteropCaffeDropoutLayerConvert(cnn, netStructure.layers(i), netParam.layers(i + offset));
         break;
      default:
         break;
      }
   }

   return cnn;
}
