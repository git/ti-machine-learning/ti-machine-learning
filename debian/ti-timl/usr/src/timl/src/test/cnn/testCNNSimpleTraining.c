/******************************************************************************/
/*!
 * \file testCNNSimpleTraining.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "testCNN.h"


/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define DATABASE_PATH    "../../database/mnist"
#define BATCH_SIZE       100
#define TEST_NUM         10000
#define TRAIN_NUM        60000
#define IMAGE_ROW        28
#define IMAGE_COL        28
#define IMAGE_CHANNEL    1


/******************************************************************************/
/*!
 * \ingroup testCNN
 * \brief   Simple training function test
 * \return  Error code
 */
/******************************************************************************/

int testCNNSimpleTraining()
{
   int                   i;
   int                   dim;
   int                   misClassifyNum;
   float                 misClassifyPercent;
   int                   label;
   struct timespec       startTime;
   struct timespec       endTime;
   long                  trainingTime;
   long                  testingTime;
   int                   err;
   long                  mem;
   int                   batchNum;
   int                   batchSize;
   timlUtilImageSet      training;
   timlUtilImageSet      testing;
   timlCNNInputParams    inputParams;
   timlCNNTrainingParams trainingParams;
   timlConvNeuralNetwork *cnn;

   batchSize      = BATCH_SIZE;
   dim            = IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL;
   batchNum       = TRAIN_NUM/BATCH_SIZE;
   misClassifyNum = 0;
   err            = 0;

   setbuf(stdout, NULL); // do not buffer the console output

   printf("[Test] CNN simple training\n");
   printf("1. Build up the CNN\n");
   trainingParams              = timlCNNTrainingParamsDefault();
   trainingParams.batchSize    = BATCH_SIZE;
   trainingParams.learningRate = 0.1;
   cnn = timlCNNCreateConvNeuralNetwork(trainingParams, 0);
   inputParams       = timlCNNInputParamsDefault();
   inputParams.scale = 1.0/256.0;
   timlCNNAddInputLayer(cnn, IMAGE_ROW, IMAGE_COL, IMAGE_CHANNEL, inputParams);            // input layer
   timlCNNAddConvLayer(cnn, 5, 5, 1, 1, 6, timlCNNConvParamsDefault());                    // conv layer
   timlCNNAddNonlinearLayer(cnn, Util_Relu);                                               // relu layer
   timlCNNAddPoolingLayer(cnn, 4, 4, 4, 4, CNN_MaxPooling, timlCNNPoolingParamsDefault()); // max pooling layer
   timlCNNAddNormLayer(cnn, timlCNNNormParamsDefault());                                   // norm layer
   timlCNNAddDropoutLayer(cnn, 0.5);                                                       // dropout layer
   timlCNNAddLinearLayer(cnn, 10, timlCNNLinearParamsDefault());                           // linear layer
   timlCNNAddNonlinearLayer(cnn, Util_Softmax);                                            // softmax layer
   timlCNNInitialize(cnn);
   timlCNNReset(cnn);
   timlCNNPrint(cnn);
   mem = timlCNNMemory(cnn);
   printf("CNN memory = %.10f MB.\n", (float) mem/1024.0/1024.0);

   printf("2. Load the MNIST database\n");
   err = timlUtilReadMNIST(DATABASE_PATH, &training, &testing);
   if (err) {
      printf("MNIST database reading error\n");
   }

   // training
   printf("3. Start training\n");
   timlCNNSetMode(cnn, Util_Train);
   clock_gettime(CLOCK_REALTIME, &startTime);
   for (i = 0; i < batchNum; i++) {
      timlCNNSupervisedTrainingWithLabelBatchMode(cnn, training.data + i*batchSize*dim, training.label + i*batchSize, dim, batchSize);
   }
   clock_gettime(CLOCK_REALTIME, &endTime);
   trainingTime = timlUtilDiffTime(startTime, endTime);
   printf("Training time = %.3f s.\n", trainingTime/1000000.0);

   // testing
   printf("4. Start testing\n");
   timlCNNSetMode(cnn, Util_Test);
   clock_gettime(CLOCK_REALTIME, &startTime);
   for (i = 0; i < testing.num; i++) {
      label = timlCNNClassifyTop1SingleMode(cnn, testing.data + i*dim, dim);
      if (label != testing.label[i]) {
         misClassifyNum++;
      }
   }
   clock_gettime(CLOCK_REALTIME, &endTime);
   testingTime = timlUtilDiffTime(startTime, endTime);
   misClassifyPercent = (float)misClassifyNum/(float)testing.num;
   printf("Testing time = %.3f s.\nFail percent = %.3f %%.\n", testingTime/1000000.0, misClassifyPercent*100.0);

   printf("5. Clean up\n");
   free(training.data);
   free(training.label);
   free(training.mean);
   free(testing.data);
   free(testing.label);
   timlCNNDelete(cnn);

   return err;
}
