/******************************************************************************/
/*!
 * \file testCNNSimpleResize.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "testCNN.h"


/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define MODEL_PATH           "../../database/test/cnn/databaseTestCNNSimpleResize1.m"
#define RESIZED_MODEL_PATH   "../../database/test/cnn/databaseTestCNNSimpleResize2.m"
#define IMAGE_ROW            28
#define IMAGE_COL            28
#define IMAGE_CHANNEL        1
#define IMAGE_RESIZE_ROW     56
#define IMAGE_RESIZE_COL     56
#define IMAGE_RESIZE_CHANNEL 1
#define INT_FORMAT           "%10d"
#define FLOAT_FORMAT         "%12.4f"
#define CNN_NAME             "cnn"
#define PARAMS_LEVEL         Util_ParamsLevel2


/******************************************************************************/
/*!
 * \ingroup testCNN
 * \brief   Simple resize function test
 * \return  Error code
 */
/******************************************************************************/

int testCNNSimpleResize()
{
   timlConvNeuralNetwork *cnn;

   printf("[Test] CNN simple resize\n");
   // build up the CNN
   printf("1. Build up CNN\n");
   cnn = timlCNNCreateConvNeuralNetwork(timlCNNTrainingParamsDefault(), 0);
   timlCNNAddInputLayer(cnn, IMAGE_ROW, IMAGE_COL, IMAGE_CHANNEL, timlCNNInputParamsDefault()); // input layer
   timlCNNAddConvLayer(cnn, 5, 5, 1, 1, 6, timlCNNConvParamsDefault());                         // conv layer
   timlCNNAddNonlinearLayer(cnn, Util_Sigmoid);                                                 // sigmoid layer
   timlCNNAddPoolingLayer(cnn, 4, 4, 4, 4, CNN_MaxPooling, timlCNNPoolingParamsDefault());      // max pooling layer
   timlCNNAddDropoutLayer(cnn, 0.2);                                                            // dropout layer
   timlCNNAddLinearLayer(cnn, 10, timlCNNLinearParamsDefault());                                // linear layer
   timlCNNAddNonlinearLayer(cnn, Util_Softmax);                                                 // softmax layer
   timlCNNInitialize(cnn);
   timlCNNReset(cnn);

   // write model
   printf("2. Write model\n");
   timlCNNWriteToFile(MODEL_PATH, cnn, PARAMS_LEVEL, CNN_NAME, FLOAT_FORMAT, INT_FORMAT);

   timlCNNResize(cnn, IMAGE_RESIZE_ROW, IMAGE_RESIZE_COL, IMAGE_RESIZE_CHANNEL);

   // write resized model
   printf("3. Write resized model\n");
   timlCNNWriteToFile(RESIZED_MODEL_PATH, cnn, PARAMS_LEVEL, CNN_NAME, FLOAT_FORMAT, INT_FORMAT);

   // free CNN
   timlCNNDelete(cnn);

   return 0;
}
