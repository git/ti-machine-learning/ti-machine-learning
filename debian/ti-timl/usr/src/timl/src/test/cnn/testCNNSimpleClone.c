/******************************************************************************/
/*!
 * \file testCNNSimpleClone.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "testCNN.h"


/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define MODEL_PATH       "../../database/test/cnn/databaseTestCNNSimpleClone1.m"
#define CLONE_MODEL_PATH "../../database/test/cnn/databaseTestCNNSimpleClone2.m"
#define IMAGE_ROW        28
#define IMAGE_COL        28
#define IMAGE_CHANNEL    3
#define INT_FORMAT       "%10d"
#define FLOAT_FORMAT     "%12.4f"
#define CNN_NAME         "cnn"
#define PARAMS_LEVEL     Util_ParamsLevel2

/******************************************************************************/
/*!
 * \ingroup testCNN
 * \brief   Simple clone function test
 * \return  Error code
 */
/******************************************************************************/

int testCNNSimpleClone()
{
   FILE                  *fp;
   timlConvNeuralNetwork *cnnCopy;
   timlConvNeuralNetwork *cnn;
   long                  mem;

   printf("[Test] CNN simple clone\n");
   cnn = timlCNNCreateConvNeuralNetwork(timlCNNTrainingParamsDefault(), 0);
   timlCNNAddInputLayer(cnn, IMAGE_ROW, IMAGE_COL, IMAGE_CHANNEL, timlCNNInputParamsDefault()); // input layer
   timlCNNAddConvLayer(cnn, 5, 5, 1, 1, 6, timlCNNConvParamsDefault());                         // conv layer
   timlCNNAddNonlinearLayer(cnn, Util_Sigmoid);                                                 // sigmoid layer
   timlCNNAddPoolingLayer(cnn, 2, 2, 2, 2, CNN_MaxPooling, timlCNNPoolingParamsDefault());      // max pooling layer
   timlCNNAddDropoutLayer(cnn, 0.5);                                                            // drop out layer
   timlCNNAddNormLayer(cnn, timlCNNNormParamsDefault());                                        // norm layer
   timlCNNAddLinearLayer(cnn, 10, timlCNNLinearParamsDefault());                                // linear layer
   timlCNNAddNonlinearLayer(cnn, Util_Softmax);                                                 // softmax layer
   timlCNNInitialize(cnn);
   mem = timlCNNMemory(cnn);
   printf("CNN memory = %.10f MB.\n", (float) mem/1024.0/1024.0);

   cnnCopy = timlCNNClone(cnn, 0);

   // write to m file
   printf("1. Write to model file\n");
   timlCNNWriteToFile(MODEL_PATH, cnn, PARAMS_LEVEL, CNN_NAME, FLOAT_FORMAT, INT_FORMAT);

   // write to m file
   printf("2. Write to cloned model file\n");
   timlCNNWriteToFile(CLONE_MODEL_PATH, cnnCopy, PARAMS_LEVEL, CNN_NAME, FLOAT_FORMAT, INT_FORMAT);

   timlCNNDelete(cnn);
   timlCNNDelete(cnnCopy);

   return 0;

}
