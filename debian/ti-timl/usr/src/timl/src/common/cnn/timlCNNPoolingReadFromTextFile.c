/******************************************************************************/
/*!
 * \file timlCNNPoolingReadFromTextFile.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Read the pooling layer from a text file
 * \param[in]     fp1 FILE ptr to the level 1 text file
 * \param[in,out] cnn CNN
 * \return        Error code
 */
/******************************************************************************/

int timlCNNPoolingReadFromTextFile(FILE *fp1, timlConvNeuralNetwork *cnn)
{
   int                  err;
   int                  intBuffer;
   timlCNNPoolingParams params;
   int                  read;
   int                  row;
   int                  col;
   int                  channel;
   char                 str[TIML_UTIL_MAX_STR];

   err    = 0;
   params = timlCNNPoolingParamsDefault();

   read = fscanf(fp1, "%[^.].layer(%d).row = %d;\n", (char*)&str, &intBuffer, &row);
   if (read != 3) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp1, "%[^.].layer(%d).col = %d;\n", (char*)&str, &intBuffer, &col);
   if (read != 3) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp1, "%[^.].layer(%d).channel = %d;\n", (char*)&str, &intBuffer, &channel);
   if (read != 3) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp1, "%[^.].layer(%d).poolingParams.type = %d;\n", (char*)&str, &intBuffer, (int*)&params.type);
   if (read != 3) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp1, "%[^.].layer(%d).poolingParams.scaleRow = %d;\n", (char*)&str, &intBuffer, &params.scaleRow);
   if (read != 3) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp1, "%[^.].layer(%d).poolingParams.scaleCol = %d;\n", (char*)&str, &intBuffer, &params.scaleCol);
   if (read != 3) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp1, "%[^.].layer(%d).poolingParams.padUp = %d;\n", (char*)&str, &intBuffer, &params.padUp);
   if (read != 3) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp1, "%[^.].layer(%d).poolingParams.padDown = %d;\n", (char*)&str, &intBuffer, &params.padDown);
   if (read != 3) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp1, "%[^.].layer(%d).poolingParams.padLeft = %d;\n", (char*)&str, &intBuffer, &params.padLeft);
   if (read != 3) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp1, "%[^.].layer(%d).poolingParams.padRight = %d;\n", (char*)&str, &intBuffer, &params.padRight);
   if (read != 3) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp1, "%[^.].layer(%d).poolingParams.strideX = %d;\n", (char*)&str, &intBuffer, &params.strideX);
   if (read != 3) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp1, "%[^.].layer(%d).poolingParams.strideY = %d;\n", (char*)&str, &intBuffer, &params.strideY);
   if (read != 3) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }

   err = timlCNNAddPoolingLayer(cnn, params.scaleRow, params.scaleCol, params.strideX, params.strideY, params.type, params);
   if (err) {
      timlCNNDelete(cnn);
      return err;
   }

   return 0;
}
