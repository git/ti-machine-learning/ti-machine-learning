/******************************************************************************/
/*!
 * \file timlCNNResize.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup cnn
 * \brief     Resize the feature map sizes to accommodate new input feature map dimensions
 * \details   Linear layers will be converted to convolutional layer
 * \param[in] cnn     CNN
 * \param[in] row     New input feature map row size
 * \param[in] col     New input feature map col size
 * \param[in] channel New input feature map channel size
 * \return    Error code
 */
/******************************************************************************/

int timlCNNResize(timlConvNeuralNetwork *cnn, int row, int col, int channel)
{
   timlCNNLayer *layer;
   timlCNNLayer *nextLayer;
   timlCNNLayer *prevLayer;
   int deviceId = cnn->deviceId;
   int threadId = cnn->threadId;

   if (cnn == NULL) {
      return ERROR_CNN_NULL_PTR;
   }

   // first pass, convert linear layer to conv layer
   layer = cnn->head;
   while (layer != NULL) {
      if (layer->type == CNN_Linear) {
         nextLayer = layer->next;
         prevLayer = layer->prev;
         cnn->tail = prevLayer;
         timlCNNAddConvLayer(cnn, prevLayer->row, prevLayer->col, 1, 1, layer->channel, timlCNNConvParamsDefault());
         timlCNNConvInitialize(cnn->tail);
         timlUtilBLASscopy(layer->linearParams.dim * layer->linearParams.prevDim, layer->linearParams.weight, cnn->tail->convParams.kernel, deviceId, threadId);
         timlUtilBLASscopy(layer->linearParams.dim, layer->linearParams.bias, cnn->tail->convParams.bias, deviceId, threadId);
         cnn->tail->convParams.type = Util_Corr2D;
         cnn->tail->convParams.kernelDecayFactor = layer->linearParams.weightDecayFactor;
         cnn->tail->convParams.kernelLearningFactor = layer->linearParams.weightLearningFactor;
         cnn->tail->convParams.biasLearningFactor = layer->linearParams.biasLearningFactor;
         cnn->tail->next = nextLayer;
         nextLayer->prev = cnn->tail;
         timlCNNDeleteLinearLayer(layer);
         layer = cnn->tail;
      }
      layer = layer->next;
   }

   // second pass, resize all the layers
   layer = cnn->head;
   while (layer != NULL) {
      switch (layer->type) {
         case CNN_Input:
            layer->inputParams.row = row;
            layer->inputParams.col = col;
            layer->inputParams.channel = channel;
            timlUtilFree(layer->inputParams.mean);
            timlUtilMalloc((void**)&(layer->inputParams.mean), sizeof(float)*row*col*channel);
            if (layer->inputParams.mean == NULL) {
               timlCNNDelete(cnn);
               return ERROR_CNN_LAYER_ALLOCATION;
            }
            timlUtilVectorResetFloat(layer->inputParams.mean, row*col*channel, 0.0, deviceId, threadId);
            break;
         case CNN_Conv:
            row = (layer->prev->row - layer->convParams.kernelRow + layer->convParams.padUp + layer->convParams.padDown)/layer->convParams.strideY + 1;
            col = (layer->prev->col - layer->convParams.kernelCol + layer->convParams.padLeft + layer->convParams.padRight)/layer->convParams.strideX + 1;
            channel = layer->channel;
            // resize biasMultiplier
            timlUtilFree(layer->convParams.biasMultiplier);
            timlUtilMalloc((void**)&(layer->convParams.biasMultiplier), sizeof(float)*row*col);
            if (layer->convParams.biasMultiplier == NULL) {
               timlCNNDelete(cnn);
               return ERROR_CNN_LAYER_ALLOCATION;
            }
            timlUtilVectorResetFloat(layer->convParams.biasMultiplier, row*col, 1.0, deviceId, threadId);

            // resize prevFeatureMapReshape
            timlUtilFree(layer->convParams.prevFeatureMapReshape);
            timlUtilMalloc((void**)&(layer->convParams.prevFeatureMapReshape), sizeof(float)*layer->prev->channel*layer->convParams.kernelRow*layer->convParams.kernelCol*row*col);
            if (layer->convParams.prevFeatureMapReshape == NULL) {
               timlCNNDelete(cnn);
               return ERROR_CNN_LAYER_ALLOCATION;
            }

            // resize prevFeatureMapReshapeIndex
            timlUtilFree(layer->convParams.prevFeatureMapReshapeIndex);
            timlUtilMalloc((void**)&(layer->convParams.prevFeatureMapReshapeIndex), sizeof(int)*layer->convParams.kernelRow*layer->convParams.kernelCol*row*col);
            if (layer->convParams.prevFeatureMapReshapeIndex == NULL) {
               timlCNNDelete(cnn);
               return ERROR_CNN_LAYER_ALLOCATION;
            }
            timlUtilConv2ImageReshapeIndex(layer->convParams.prevFeatureMapReshapeIndex, layer->prev->row, layer->prev->col, layer->convParams.kernelRow, layer->convParams.kernelCol, layer->convParams.padUp, layer->convParams.padDown, layer->convParams.padLeft, layer->convParams.padRight, layer->convParams.strideX, layer->convParams.strideY, layer->convParams.type);
            break;
         case CNN_Norm:
            row = layer->prev->row;
            col = layer->prev->col;
            channel = layer->prev->channel;
            timlUtilFree(layer->normParams.denom);
            timlUtilMalloc((void**)&(layer->normParams.denom), sizeof(float)*row*col*channel);
            if (layer->normParams.denom == NULL) {
               timlCNNDelete(cnn);
               return ERROR_CNN_LAYER_ALLOCATION;
            }
            break;
         case CNN_Pooling:
            row = ceilf((layer->prev->row - layer->poolingParams.scaleRow + layer->poolingParams.padUp + layer->poolingParams.padDown)/(float)layer->poolingParams.strideY) + 1;
            col = ceilf((layer->prev->col - layer->poolingParams.scaleCol + layer->poolingParams.padLeft + layer->poolingParams.padRight)/(float) layer->poolingParams.strideX) + 1;
            channel = layer->prev->channel;
            if (layer->poolingParams.type == CNN_MaxPooling) {
               timlUtilFree(layer->poolingParams.maxIndex);
               timlUtilMalloc((void**)&(layer->poolingParams.maxIndex), sizeof(int)*row*col*channel);
               if (layer->poolingParams.maxIndex == NULL) {
                  timlCNNDelete(cnn);
                  return ERROR_CNN_LAYER_ALLOCATION;
               }
            }
            break;
         case CNN_Dropout:
            row = layer->prev->row;
            col = layer->prev->col;
            channel = layer->prev->channel;
            timlUtilFree(layer->dropoutParams.mask);
            timlUtilMalloc((void**)&(layer->dropoutParams.mask), sizeof(int)*row*col*channel);
            if (layer->dropoutParams.mask == NULL) {
               timlCNNDelete(cnn);
               return ERROR_CNN_LAYER_ALLOCATION;
            }
            timlUtilFree(layer->dropoutParams.randomVector);
            timlUtilMalloc((void**)&(layer->dropoutParams.randomVector), sizeof(unsigned int)*row*col*channel);
            if (layer->dropoutParams.randomVector == NULL) {
               timlCNNDelete(cnn);
               return ERROR_CNN_LAYER_ALLOCATION;
            }
            break;
         case CNN_Nonlinear:
            row = layer->prev->row;
            col = layer->prev->col;
            channel = layer->prev->channel;
            timlUtilFree(layer->nonlinearParams.derivative);
            timlUtilMalloc((void**)&(layer->nonlinearParams.derivative), sizeof(float)*row*col*channel);
            if (layer->nonlinearParams.derivative == NULL) {
               timlCNNDelete(cnn);
               return ERROR_CNN_LAYER_ALLOCATION;
            }
            break;
         case CNN_Linear:
            break;
      }

      // resize featureMap and delta
      layer->row = row;
      layer->col = col;
      layer->channel = channel;
      timlUtilFree(layer->featureMap);
      timlUtilMalloc((void**)&(layer->featureMap), sizeof(float)*row*col*channel);
      if (layer->featureMap == NULL) {
         timlCNNDelete(cnn);
         return ERROR_CNN_LAYER_ALLOCATION;
      }
      timlUtilFree(layer->delta);
      timlUtilMalloc((void**)&(layer->delta), sizeof(float)*row*col*channel);
      if (layer->delta == NULL) {
         timlCNNDelete(cnn);
         return ERROR_CNN_LAYER_ALLOCATION;
      }
      layer = layer->next;
   }

   return 0;

}
