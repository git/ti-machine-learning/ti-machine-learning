/******************************************************************************/
/*!
 * \file timlCNNDelete.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup   cnn
 * \brief     Free a cnn structure
 * \param[in] cnn CNN structure
 * \return    Error code
 */
/******************************************************************************/

int timlCNNDelete(timlConvNeuralNetwork *cnn)
{
   int err;
   timlCNNLayer *layer;
   timlCNNLayer *next;

   err = 0;
   // error checking
   if (cnn == NULL) {
      return ERROR_CNN_NULL_PTR;
   }
   if (cnn->params.allocatorLevel == Util_AllocatorLevel3) {
      free(cnn->memPool);
   }

   layer = cnn->head;
   // delete the rest of the layers
   while (layer != NULL) {
      next = layer->next;
      switch (layer->type) {
         case CNN_Input:
            timlCNNDeleteInputLayer(layer);
            break;
         case CNN_Conv:
            timlCNNDeleteConvLayer(layer);
            break;
         case CNN_Norm:
            timlCNNDeleteNormLayer(layer);
            break;
         case CNN_Pooling:
            timlCNNDeletePoolingLayer(layer);
            break;
         case CNN_Linear:
            timlCNNDeleteLinearLayer(layer);
            break;
         case CNN_Nonlinear:
            timlCNNDeleteNonlinearLayer(layer);
            break;
         case CNN_Dropout:
            timlCNNDeleteDropoutLayer(layer);
            break;
      }
      layer = next;
   }
   free(cnn);

   return err;
}
