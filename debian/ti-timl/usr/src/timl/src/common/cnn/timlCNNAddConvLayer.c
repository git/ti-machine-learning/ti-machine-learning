/******************************************************************************/
/*!
 * \file timlCNNAddConvLayer.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Add conv layer
 * \param[in,out] cnn               CNN
 * \param[in]     kernelRow         Kernel row size
 * \param[in]     kernelCol         Kernel col size
 * \param[in]     strideX           Kernel horizontal stride size
 * \param[in]     strideY           Kernel vertical stride size
 * \param[in]     featureMapChannel Output feature map channel size
 * \param[in]     params            Optional parameters
 * \return        Error code
 */
/******************************************************************************/

int timlCNNAddConvLayer(timlConvNeuralNetwork *cnn, int kernelRow, int kernelCol, int strideX, int strideY, int featureMapChannel, timlCNNConvParams params)
{
   int          err;
   int          prevFeatureMapRow;
   int          prevFeatureMapCol;
   int          prevFeatureMapChannel;
   int          currFeatureMapRow;
   int          currFeatureMapCol;
   int          padUp;
   int          padDown;
   int          padLeft;
   int          padRight;
   timlCNNLayer *prev;
   timlCNNLayer *convLayer;

   // init
   err                   = 0;
   prevFeatureMapRow     = 0;
   prevFeatureMapCol     = 0;
   prevFeatureMapChannel = 0;
   currFeatureMapRow     = 0;
   currFeatureMapCol     = 0;
   prev                  = NULL;
   convLayer             = NULL;
   padUp                 = params.padUp;
   padDown               = params.padDown;
   padLeft               = params.padLeft;
   padRight              = params.padRight;

   // params error checking
   if (cnn == NULL) {
      return ERROR_CNN_NULL_PTR;
   }
   if (cnn->tail == NULL) {
      return ERROR_CNN_EMPTY;
   }
   if (kernelRow <= 0 || kernelCol <= 0) {
      return ERROR_CNN_CONV_LAYER_KERNEL_SIZE;
   }
   if (padLeft >= kernelCol || padLeft < 0 || padRight >= kernelCol || padRight < 0 || padUp < 0 || padUp >= kernelRow || padDown >= kernelRow || padDown < 0) {
      return ERROR_CNN_CONV_LAYER_PAD_SIZE;
   }
   if (strideX <= 0 || strideY <= 0) {
      return ERROR_CNN_CONV_LAYER_STRIDE_SIZE;
   }
   if (featureMapChannel <= 0) {
      return ERROR_CNN_FEATURE_MAP_CHANNEL;
   }

   prev                  = cnn->tail;
   prevFeatureMapRow     = prev->row;
   prevFeatureMapCol     = prev->col;
   prevFeatureMapChannel = prev->channel;
   currFeatureMapRow     = (prevFeatureMapRow - kernelRow + padUp + padDown)/strideY + 1;
   currFeatureMapCol     = (prevFeatureMapCol - kernelCol + padLeft + padRight)/strideX + 1;
   if (currFeatureMapRow < 1 || currFeatureMapCol < 1) {
      return ERROR_CNN_FEATURE_MAP_SIZE;
   }

   // allocate conv layer
   convLayer = (timlCNNLayer*) malloc(sizeof(timlCNNLayer));
   if (convLayer == NULL) {
      return ERROR_CNN_LAYER_ALLOCATION;
   }

   // load params
   convLayer->convParams = params;

   // override params
   convLayer->convParams.prevFeatureMapReshape      = NULL;
   convLayer->convParams.prevFeatureMapReshapeIndex = NULL;
   convLayer->convParams.kernel                     = NULL;
   convLayer->convParams.kernelInc                  = NULL;
   convLayer->convParams.kernelGradAccum            = NULL;
   convLayer->convParams.bias                       = NULL;
   convLayer->convParams.biasInc                    = NULL;
   convLayer->convParams.biasGradAccum              = NULL;
   convLayer->convParams.connectivity               = NULL;
   convLayer->convParams.inputFeatureMapChannel     = prevFeatureMapChannel;
   convLayer->convParams.outputFeatureMapChannel    = featureMapChannel;
   convLayer->convParams.kernelRow                  = kernelRow;
   convLayer->convParams.kernelCol                  = kernelCol;
   convLayer->convParams.strideX                    = strideX;
   convLayer->convParams.strideY                    = strideY;

//   // set inactive params
//   convLayer->inputParams     = timlCNNInputParamsDefault();
//   convLayer->poolingParams   = timlCNNPoolingParamsDefault();
//   convLayer->normParams      = timlCNNNormParamsDefault();
//   convLayer->linearParams    = timlCNNLinearParamsDefault();
//   convLayer->nonlinearParams = timlCNNNonlinearParamsDefault();

   convLayer->type           = CNN_Conv;
   convLayer->featureMap     = NULL;
   convLayer->delta          = NULL;
   convLayer->allocatorLevel = cnn->params.allocatorLevel;
   convLayer->phase          = cnn->params.phase;
   convLayer->row            = currFeatureMapRow;
   convLayer->col            = currFeatureMapCol;
   convLayer->channel        = featureMapChannel;

   // link the convLayer
   convLayer->cnn        = cnn;
   convLayer->id         = prev->id + 1;
   convLayer->prev       = prev;
   convLayer->prev->next = convLayer;
   convLayer->next       = NULL;
   cnn->tail             = convLayer;

   return err;

}
