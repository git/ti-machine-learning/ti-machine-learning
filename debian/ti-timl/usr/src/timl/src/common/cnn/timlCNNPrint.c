/******************************************************************************/
/*!
 * \file timlCNNPrint.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Print out the information of the cnn
 * \param[in,out] cnn CNN
 * \return        Error code
 */
/******************************************************************************/

int timlCNNPrint(timlConvNeuralNetwork *cnn)
{
   timlCNNLayer *layer = cnn->head;

   if (cnn == NULL) {
      return ERROR_CNN_NULL_PTR;
   }

   while (layer != NULL) {
      printf("\t|\n");
      switch (layer->type) {
         case CNN_Input:
            printf("[Input Layer]\n");
            break;
         case CNN_Dropout:
            printf("[Dropout Layer]\n");
            break;
         case CNN_Conv:
            printf("[Convolution Layer]\n");
            break;
         case CNN_Norm:
            printf("[Normalization Layer]\n");
            break;
         case CNN_Pooling:
            if (layer->poolingParams.type == CNN_MaxPooling)
               printf("[MaxPooling Layer]\n");
            else
               printf("[MeanPooling Layer]\n");
            break;
         case CNN_Linear:
            printf("[Linear Layer]\n");
            break;
         case CNN_Nonlinear:
            switch (layer->nonlinearParams.type) {
               case Util_Tanh:
                  printf("[Tanh Layer]\n");
                  break;
               case Util_Sigmoid:
                  printf("[Sigmoid Layer]\n");
                  break;
               case Util_Softmax:
                  printf("[Softmax Layer]\n");
                  break;
               case Util_Relu:
                  printf("[Relu Layer]\n");
                  break;
               default:
                  break;
            }
            break;
         default:
            break;
      }
      printf("\t|\n");
      printf("(%4d, %4d, %4d)\n", layer->row, layer->col, layer->channel);
      layer = layer->next;
   }

   return 0;
}

