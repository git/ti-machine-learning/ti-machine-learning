/******************************************************************************/
/*!
 * \file timlCNNConvUpdateParams.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup cnn
 * \brief         Update the parameters of the conv layer
 * \param[in]     layer Layer
 * \return        Error code
 */
/******************************************************************************/

int timlCNNConvUpdateParams(timlCNNLayer *layer)
{
   int                   err;
   timlConvNeuralNetwork *cnn;
   int                   prevFeatureMapChannel;
   int                   currFeatureMapChannel;
   int                   kernelRow;
   int                   kernelCol;
   int                   count;
   float                 kernelLearningRate;
   float                 biasLearningRate;
   float                 kernelDecay;
   int                   deviceId;
   int                   threadId;

   // init
   err                   = 0;
   cnn                   = layer->cnn;
   prevFeatureMapChannel = layer->convParams.inputFeatureMapChannel;
   currFeatureMapChannel = layer->convParams.outputFeatureMapChannel;
   kernelRow             = layer->convParams.kernelRow;
   kernelCol             = layer->convParams.kernelCol;
   count                 = cnn->params.count;
   kernelLearningRate    = layer->convParams.kernelLearningFactor * cnn->params.learningRate;
   biasLearningRate      = layer->convParams.biasLearningFactor * cnn->params.learningRate;
   kernelDecay           = layer->convParams.kernelDecayFactor * cnn->params.weightDecay;
   deviceId              = cnn->deviceId;
   threadId              = cnn->threadId;

   // kernelGradAccum = kernelGradAccum / count
   timlUtilBLASsscal(kernelRow*kernelCol*prevFeatureMapChannel*currFeatureMapChannel, 1.0/count, layer->convParams.kernelGradAccum, deviceId, threadId);
   // kernelGradAccum = kernelGradAccum + kernelDecay * kernel
   timlUtilBLASsaxpy(kernelRow*kernelCol*prevFeatureMapChannel*currFeatureMapChannel, kernelDecay, layer->convParams.kernel, layer->convParams.kernelGradAccum, deviceId, threadId);
   // kernelGradAccum = kernelGradAccum * learningRate
   timlUtilBLASsscal(kernelRow*kernelCol*prevFeatureMapChannel*currFeatureMapChannel, kernelLearningRate, layer->convParams.kernelGradAccum, deviceId, threadId);
   // kernelGradAccum = kernelGradAccum + momentum * kernelInc
   timlUtilBLASsaxpy(kernelRow*kernelCol*prevFeatureMapChannel*currFeatureMapChannel, cnn->params.momentum, layer->convParams.kernelInc, layer->convParams.kernelGradAccum, deviceId, threadId);
   // kernelInc = kernelGradAccum
   timlUtilBLASscopy(kernelRow*kernelCol*prevFeatureMapChannel*currFeatureMapChannel, layer->convParams.kernelGradAccum, layer->convParams.kernelInc, deviceId, threadId);
   // kernel = kernel + (-1) * kernelInc
   timlUtilBLASsaxpy(kernelRow*kernelCol*prevFeatureMapChannel*currFeatureMapChannel, -1.0, layer->convParams.kernelInc, layer->convParams.kernel, deviceId, threadId);
   // reset kernelGradAccum
   timlUtilVectorResetFloat(layer->convParams.kernelGradAccum, kernelRow*kernelCol*prevFeatureMapChannel*currFeatureMapChannel, 0.0, deviceId, threadId);

   // biasGradAccum = learningRate * biasGradAccum / count
   timlUtilBLASsscal(currFeatureMapChannel, biasLearningRate / count, layer->convParams.biasGradAccum, deviceId, threadId);
   // biasInc = biasInc * momentum
   timlUtilBLASsscal(currFeatureMapChannel, cnn->params.momentum, layer->convParams.biasInc, deviceId, threadId);
   // biasInc = biasInc + biasGradAccum
   timlUtilBLASsaxpy(currFeatureMapChannel, 1.0, layer->convParams.biasGradAccum, layer->convParams.biasInc, deviceId, threadId);
   // bias = bias - biasInc
   timlUtilBLASsaxpy(currFeatureMapChannel, -1.0, layer->convParams.biasInc, layer->convParams.bias, deviceId, threadId);
   // reset biasGradAccum
   timlUtilVectorResetFloat(layer->convParams.biasGradAccum, currFeatureMapChannel, 0.0, deviceId, threadId);

   return err;
}
