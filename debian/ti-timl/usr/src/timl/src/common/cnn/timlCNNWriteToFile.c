/******************************************************************************/
/*!
 * \file timlCNNWriteToFile.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup cnn
 * \brief     Write the cnn to file(s)
 * \param[in] fileName    File name
 * \param[in] cnn         CNN
 * \param[in] level       Parameter write level
 * \param[in] name        Name of the cnn
 * \param[in] floatFormat Format string for float
 * \param[in] intFormat   Format string for int
 * \return    Error code
 */
/******************************************************************************/

int timlCNNWriteToFile(const char *fileName, timlConvNeuralNetwork *cnn, timlUtilParamsLevel level, const char* name, const char *floatFormat, const char *intFormat)
{
   int          err;
   char         str[TIML_UTIL_MAX_STR];
   FILE         *fp1; // ParamsLevel1
   FILE         *fp2; // ParamsLevel2
   FILE         *fp3; // ParamsLevel3
   timlCNNLayer *layer;
   char         *baseName;


   err   = 0;
   fp1   = NULL;
   fp2   = NULL;
   fp3   = NULL;
   layer = cnn->head;
   baseName = basename(strdup(fileName));

   if (cnn == NULL) {
      return ERROR_CNN_NULL_PTR;
   }

   cnn->params.allocatorLevel = level;

   fp1 = fopen(fileName, "wt"); // level 1, 2, 3
   if (level == Util_ParamsLevel2 || level == Util_ParamsLevel3) { // level 2, 3
      sprintf(str, "%s.params", fileName);
      fp2 = fopen(str, "wb");
      sprintf(str, "%s.params", baseName);
      fprintf(fp1, "paramsBinaryFileName = '%s';\n", str);
   }
   else {
      fprintf(fp1, "paramsBinaryFileName = '';\n");
   }

   if (level == Util_ParamsLevel3 && cnn->params.allocatorLevel == Util_AllocatorLevel1) { // paramsLevel 3 + allocatorLevel1
      sprintf(str, "%s.states", fileName);
      fp3 = fopen(str, "wb");
      sprintf(str, "%s.states", baseName);
      fprintf(fp1, "stateBinaryFileName = '%s';\n", str);
   }
   else {
      fprintf(fp1, "stateBinaryFileName = '';\n");
   }
   fprintf(fp1, "\n");

   // write training params
   timlCNNTrainingParamsWriteToFile(fp1, cnn, name, floatFormat, intFormat);
   fprintf(fp1, "\n");
   fprintf(fp1, "layerNum = %d;\n", timlCNNGetLayerNum(cnn));
   while (layer != NULL) {
      // layer type
      fprintf(fp1, "%s.layer(%d).id = %d;\n", name, layer->id + 1, layer->id + 1);
      fprintf(fp1, "%s.layer(%d).type = %d;\n", name, layer->id + 1, layer->type);
      fprintf(fp1, "%s.layer(%d).row = %d;\n", name, layer->id + 1, layer->row);
      fprintf(fp1, "%s.layer(%d).col = %d;\n", name, layer->id + 1, layer->col);
      fprintf(fp1, "%s.layer(%d).channel = %d;\n", name, layer->id + 1, layer->channel);
      switch (layer->type) {
         case CNN_Dropout:
            timlCNNDropoutWriteToFile(fp1, fp2, fp3, layer, level, name, floatFormat, intFormat);
            break;
         case CNN_Input:
            timlCNNInputWriteToFile(fp1, fp2, fp3, layer, level, name, floatFormat, intFormat);
            break;
         case CNN_Conv:
            timlCNNConvWriteToFile(fp1, fp2, fp3, layer, level, name, floatFormat, intFormat);
            break;
         case CNN_Linear:
            timlCNNLinearWriteToFile(fp1, fp2, fp3, layer, level, name, floatFormat, intFormat);
            break;
         case CNN_Nonlinear:
            timlCNNNonlinearWriteToFile(fp1, fp2, fp3, layer, level, name, floatFormat, intFormat);
            break;
         case CNN_Norm:
            timlCNNNormWriteToFile(fp1, fp2, fp3, layer, level, name, floatFormat, intFormat);
            break;
         case CNN_Pooling:
            timlCNNPoolingWriteToFile(fp1, fp2, fp3, layer, level, name, floatFormat, intFormat);
            break;
      }
      fprintf(fp1, "\n");
      layer = layer->next;
   }

   if (fp1 != NULL) {
      fclose(fp1);
   }
   if (fp2 != NULL) {
      fclose(fp2);
   }
   if (fp3 != NULL) {
      fclose(fp3);
   }

   return err;
}
