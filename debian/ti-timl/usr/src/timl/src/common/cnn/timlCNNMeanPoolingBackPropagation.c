/******************************************************************************/
/*!
 * \file timlCNNMeanPoolingBackPropagation.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup cnn
 * \brief         Back propagate the gradient from the mean pooling layer to the previous layer
 * \param[in,out] layer
 * \return        Error code
 */
/******************************************************************************/

int timlCNNMeanPoolingBackPropagation(timlCNNLayer *layer)
{
   timlCNNLayer *prevLayer;
   int scaleRow;
   int scaleCol;
   int padUp;
   int padLeft;
   int strideX;
   int strideY;
   int deviceId;
   int threadId;

   prevLayer = layer->prev;
   scaleRow  = layer->poolingParams.scaleRow;
   scaleCol  = layer->poolingParams.scaleCol;
   padUp     = layer->poolingParams.padUp;
   padLeft   = layer->poolingParams.padLeft;
   strideX   = layer->poolingParams.strideX;
   strideY   = layer->poolingParams.strideY;
   deviceId  = prevLayer->cnn->deviceId;
   threadId  = prevLayer->cnn->threadId;

   timlUtilVectorResetFloat(prevLayer->delta, prevLayer->channel * prevLayer->row * prevLayer->col, 0.0, deviceId, threadId);
   timlUtilUndoMeanPooling(prevLayer->delta, layer->delta, layer->row, layer->col, layer->channel, prevLayer->row, prevLayer->col, scaleRow, scaleCol, padUp, padLeft, strideX, strideY, deviceId, threadId);

   return 0;
}
