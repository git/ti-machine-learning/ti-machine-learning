/******************************************************************************/
/*!
 * \file timlCNNShareParams.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup   cnn
 * \brief     Create a new CNN that shares the parameters with the input CNN
 * \details   Unlike the clone operation, the returned CNN points to the parameters to the input CNN.
 * \param[in] cnn      CNN to be share parameters with
 * \param[in] deviceId Device Id
 * \return    CNN that shares the same parameter with the input CNN
 */

/******************************************************************************/

timlConvNeuralNetwork *timlCNNShareParams(timlConvNeuralNetwork *cnn, int deviceId)
{
   timlConvNeuralNetwork *cnnCopy;
   timlCNNLayer          *layer;
   int                   err;

   err   = 0;
   layer = cnn->head;
   cnnCopy = timlCNNCreateConvNeuralNetwork(cnn->params, deviceId);
   if (cnnCopy == NULL) {
      return NULL;
   }

   while (layer != NULL) {
      switch (layer->type) {
         case CNN_Input:
            err = timlCNNInputShareParams(cnnCopy, layer);
            if (err) {
               timlCNNDelete(cnnCopy);
               return NULL;
            }
            break;
         case CNN_Conv:
            err = timlCNNConvShareParams(cnnCopy, layer);
            if (err) {
               timlCNNDelete(cnnCopy);
               return NULL;
            }
            break;
         case CNN_Linear:
            err = timlCNNLinearShareParams(cnnCopy, layer);
            if (err) {
               timlCNNDelete(cnnCopy);
               return NULL;
            }
            break;
         case CNN_Norm:
            err = timlCNNAddNormLayer(cnnCopy, layer->normParams);
            if (err) {
               timlCNNDelete(cnnCopy);
               return NULL;
            }
            break;
         case CNN_Dropout:
            err = timlCNNAddDropoutLayer(cnnCopy, layer->dropoutParams.prob);
            if (err) {
               timlCNNDelete(cnnCopy);
               return NULL;
            }
            break;
         case CNN_Pooling:
            err = timlCNNAddPoolingLayer(cnnCopy, layer->poolingParams.scaleRow, layer->poolingParams.scaleCol, layer->poolingParams.strideX, layer->poolingParams.strideY, layer->poolingParams.type, layer->poolingParams);
            if (err) {
               timlCNNDelete(cnnCopy);
               return NULL;
            }
            break;
         case CNN_Nonlinear:
            err = timlCNNAddNonlinearLayer(cnnCopy, layer->nonlinearParams.type);
            if (err) {
               timlCNNDelete(cnnCopy);
               return NULL;
            }
            break;
      }
      layer = layer->next;
   }
   cnnCopy->params = cnn->params;

   err = timlCNNInitialize(cnnCopy);
   if (err) {
      timlCNNDelete(cnnCopy);
      return NULL;
   }

   return cnnCopy;
}
