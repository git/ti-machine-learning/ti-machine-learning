/******************************************************************************/
/*!
 * \file timlCNNBackPropagation.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup   cnn
 * \brief     Back propagate the gradient from layer to the first layer of the cnn
 * \param[in] cnn   CNN
 * \param[in] layer Start layer
 * \return    Error code
 */
/******************************************************************************/

int timlCNNBackPropagation(timlConvNeuralNetwork *cnn, timlCNNLayer *layer)
{
   int err = 0;
   // search backward until the second layer
   while (layer->prev != NULL) {
      switch (layer->type) {
         case CNN_Input:
            break;
         case CNN_Conv:
            err = timlCNNConvBackPropagation(layer);
            if (err) return err;
            break;
         case CNN_Pooling:
            err = timlCNNPoolingBackPropagation(layer);
            if (err) return err;
            break;
         case CNN_Norm:
            err = timlCNNNormBackPropagation(layer);
            if (err) return err;
            break;
         case CNN_Nonlinear:
            err = timlCNNNonlinearBackPropagation(layer);
            if (err) return err;
            break;
         case CNN_Linear:
            err = timlCNNLinearBackPropagation(layer);
            if (err) return err;
            break;
         case CNN_Dropout:
            err = timlCNNDropoutBackPropagation(layer);
            if (err) return err;
            break;
      }
      layer = layer->prev;
   }
   return err;
}
