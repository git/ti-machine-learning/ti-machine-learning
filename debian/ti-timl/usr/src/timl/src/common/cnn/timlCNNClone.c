/******************************************************************************/
/*!
 * \file timlCNNClone.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/**
 * \ingroup   cnn
 * \brief     Clone a cnn
 * \param[in] cnn      CNN to be cloned
 * \param[in] deviceId Device Id
 * \return    Cloned cnn
 */
/******************************************************************************/

timlConvNeuralNetwork *timlCNNClone(timlConvNeuralNetwork *cnn, int deviceId)
{
   int                   err;
   int                   dim;
   timlConvNeuralNetwork *cnnCopy;
   timlCNNLayer          *layer;
   timlCNNLayer          *layerCopy;
   int                   threadId;

   // init
   layer        = cnn->head;
   threadId     = cnn->threadId;
   cnnCopy      = timlCNNCreateConvNeuralNetwork(cnn->params, deviceId);
   if (cnnCopy == NULL) {
      return NULL;
   }

   while (layer != NULL) {
      switch (layer->type) {
         case CNN_Input:
            err = timlCNNAddInputLayer(cnnCopy, layer->row, layer->col, layer->channel, layer->inputParams);
            if (err) {
               timlCNNDelete(cnnCopy);
               return NULL;
            }
            break;
         case CNN_Conv:
            err = timlCNNAddConvLayer(cnnCopy, layer->convParams.kernelRow, layer->convParams.kernelCol, layer->convParams.strideX, layer->convParams.strideY, layer->convParams.outputFeatureMapChannel, layer->convParams);
            if (err) {
               timlCNNDelete(cnnCopy);
               return NULL;
            }
            break;
         case CNN_Linear:
            err = timlCNNAddLinearLayer(cnnCopy, layer->linearParams.dim, layer->linearParams);
            if (err) {
               timlCNNDelete(cnnCopy);
               return NULL;
            }
            break;
         case CNN_Norm:
            err = timlCNNAddNormLayer(cnnCopy, layer->normParams);
            if (err) {
               timlCNNDelete(cnnCopy);
               return NULL;
            }
            break;
         case CNN_Pooling:
            err = timlCNNAddPoolingLayer(cnnCopy, layer->poolingParams.scaleRow, layer->poolingParams.scaleCol, layer->poolingParams.strideX, layer->poolingParams.strideY, layer->poolingParams.type, layer->poolingParams);
            if (err) {
               timlCNNDelete(cnnCopy);
               return NULL;
            }
            break;
         case CNN_Nonlinear:
            err = timlCNNAddNonlinearLayer(cnnCopy, layer->nonlinearParams.type);
            if (err) {
               timlCNNDelete(cnnCopy);
               return NULL;
            }
            break;
         case CNN_Dropout:
            err = timlCNNAddDropoutLayer(cnnCopy, layer->dropoutParams.prob);
            if (err) {
               timlCNNDelete(cnnCopy);
               return NULL;
            }
            break;
      }
      layer = layer->next;
   }

   // allocation
   err = timlCNNInitialize(cnnCopy);
   if (err) {
      timlCNNDelete(cnnCopy);
      return NULL;
   }

   // copy parameters
   layerCopy = cnnCopy->head;
   layer     = cnn->head;
   while (layer != NULL) {
      switch (layer->type) {
         case CNN_Input:
            // copy mean
            dim = layer->inputParams.row*layer->inputParams.col*layer->inputParams.channel;
            timlUtilBLASscopy(dim, layer->inputParams.mean, layerCopy->inputParams.mean, deviceId, threadId);
            break;
         case CNN_Conv:
            dim = layer->convParams.inputFeatureMapChannel*layer->convParams.outputFeatureMapChannel*layer->convParams.kernelRow*layer->convParams.kernelCol;
            timlUtilBLASscopy(dim, layer->convParams.kernel, layerCopy->convParams.kernel, deviceId, threadId);
            timlUtilBLASscopy(layer->channel, layer->convParams.bias, layerCopy->convParams.bias, deviceId, threadId);
            if (layer->allocatorLevel == Util_AllocatorLevel1) { // training mode
               timlUtilBLASscopy(dim, layer->convParams.kernelInc, layerCopy->convParams.kernelInc, deviceId, threadId);
               timlUtilBLASscopy(dim, layer->convParams.kernelGradAccum, layerCopy->convParams.kernelGradAccum, deviceId, threadId);
               timlUtilBLASscopy(layer->channel, layer->convParams.biasInc, layerCopy->convParams.biasInc, deviceId, threadId);
               timlUtilBLASscopy(layer->channel, layer->convParams.biasGradAccum, layerCopy->convParams.biasGradAccum, deviceId, threadId);
            }
            break;
         case CNN_Linear:
            dim = layer->linearParams.dim*layer->linearParams.prevDim;
            timlUtilBLASscopy(dim, layer->linearParams.weight, layerCopy->linearParams.weight, deviceId, threadId);
            timlUtilBLASscopy(layer->channel, layer->linearParams.bias, layerCopy->linearParams.bias, deviceId, threadId);
            if (layer->allocatorLevel == Util_AllocatorLevel1) { // training mode
               timlUtilBLASscopy(dim, layer->linearParams.weightInc, layerCopy->linearParams.weightInc, deviceId, threadId);
               timlUtilBLASscopy(dim, layer->linearParams.weightGradAccum, layerCopy->linearParams.weightGradAccum, deviceId, threadId);
               timlUtilBLASscopy(layer->channel, layer->linearParams.biasInc, layerCopy->linearParams.biasInc, deviceId, threadId);
               timlUtilBLASscopy(layer->channel, layer->linearParams.biasGradAccum, layerCopy->linearParams.biasGradAccum, deviceId, threadId);
            }
            break;
         case CNN_Norm:
            break;
         case CNN_Pooling:
            break;
         case CNN_Nonlinear:
            break;
         case CNN_Dropout:
            break;
      }
      layer     = layer->next;
      layerCopy = layerCopy->next;
   }

   return cnnCopy;
}
