/******************************************************************************/
/*!
 * \file timlCNNAddInputLayer.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/*******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Add input layer
 * \param[in,out] cnn               CNN
 * \param[in]     featureMapRow     Output feature map row size
 * \param[in]     featureMapCol     Output feature map col size
 * \param[in]     featureMapChannel Output feature map channel size
 * \param[in]     params            Optional parameters
 * \return        Error code
 */
/*******************************************************************************/

int timlCNNAddInputLayer(timlConvNeuralNetwork *cnn, int featureMapRow, int featureMapCol, int featureMapChannel, timlCNNInputParams params)
{
   timlCNNLayer* newLayer;

   // error checking
   if (cnn == NULL) {
      return ERROR_CNN_NULL_PTR;
   }
   if (featureMapRow <= 0 || featureMapCol <= 0 || featureMapChannel <= 0) {
      return ERROR_CNN_INPUT_LAYER_PARAMS;
   }

   // setup input layer
   newLayer = malloc(sizeof(timlCNNLayer));
   if (newLayer == NULL) {
      return ERROR_CNN_LAYER_ALLOCATION;
   }

//   newLayer->convParams      = timlCNNConvParamsDefault();
//   newLayer->poolingParams   = timlCNNPoolingParamsDefault();
//   newLayer->normParams      = timlCNNNormParamsDefault();
//   newLayer->linearParams    = timlCNNLinearParamsDefault();
//   newLayer->nonlinearParams = timlCNNNonlinearParamsDefault();

   newLayer->id             = 0;
   newLayer->type           = CNN_Input;
   newLayer->phase          = cnn->params.phase;
   newLayer->allocatorLevel = cnn->params.allocatorLevel;
   newLayer->channel        = featureMapChannel;
   newLayer->row            = featureMapRow;
   newLayer->col            = featureMapCol;

   newLayer->inputParams = params;
   if (newLayer->inputParams.row == -1) {
      newLayer->inputParams.row = newLayer->row;
   }
   if (newLayer->inputParams.col == -1) {
      newLayer->inputParams.col = newLayer->col;
   }
   if (newLayer->inputParams.channel == -1) {
      newLayer->inputParams.channel = newLayer->channel;
   }

   newLayer->featureMap       = NULL;
   newLayer->delta            = NULL;
   newLayer->inputParams.mean = NULL;
   newLayer->prev             = NULL;
   newLayer->next             = NULL;

   // link the new layer
   newLayer->cnn = cnn;
   cnn->head     = newLayer;
   cnn->tail     = newLayer;

   return 0;
}
