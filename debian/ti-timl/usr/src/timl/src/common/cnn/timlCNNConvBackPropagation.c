/******************************************************************************/
/*!
 * \file timlCNNConvBackPropagation.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Back propagate the gradient from the conv layer to the previous layer
 * \details       layer->prev->delta[i] = sum_{j}(layer->delta[j] conv2full layer->kernel[i, j])
 * \param[in]     layer Layer ptr
 * \return        Error code
 */
/******************************************************************************/

int timlCNNConvBackPropagation(timlCNNLayer *layer)
{
   int j;
   int M;
   int K;
   int N;
   int err;
   int prevRow;
   int prevCol;
   int prevChannel;
   int row;
   int col;
   int channel;
   int kernelRow;
   int kernelCol;
   int deviceId;
   int threadId;

   // init
   prevRow     = layer->prev->row;
   prevCol     = layer->prev->col;
   prevChannel = layer->prev->channel;
   row         = layer->row;
   col         = layer->col;
   channel     = layer->channel;
   kernelRow   = layer->convParams.kernelRow;
   kernelCol   = layer->convParams.kernelCol;
   deviceId    = layer->cnn->deviceId;
   threadId    = layer->cnn->threadId;
   M           = channel;
   K           = kernelRow*kernelCol*prevChannel;
   N           = row*col;

   // kernelGrad = delta * prevFeatureMapReshape' -- (M*N)*(N*K)
   #pragma omp critical
   {
      timlUtilBLASsgemm(CblasNoTrans, CblasTrans, M, K, N, 1.0, layer->delta, layer->convParams.prevFeatureMapReshape, 1.0, layer->convParams.kernelGradAccum, deviceId, threadId);
      timlUtilBLASsgemv(CblasNoTrans, M, N, 1.0, layer->delta, layer->convParams.biasMultiplier, 1.0, layer->convParams.biasGradAccum, deviceId, threadId);
   }

   // back propagate delta
   if (layer->prev->delta != NULL) {
      // reset prevDelta to 0
      timlUtilVectorResetFloat(layer->prev->delta, prevRow*prevCol*prevChannel, 0.0, deviceId, threadId);
      // prevDeltaTemp = kernel' * delta -- (K*M)(M*N)
      timlUtilBLASsgemm(CblasTrans, CblasNoTrans, K, N, M, 1.0, layer->convParams.kernel, layer->delta, 0.0, layer->convParams.prevFeatureMapReshape, deviceId, threadId);
      // reshape prevDeltaTemp to prevDelta
      timlUtilConv2ImageReshapeBack(layer->prev->delta, layer->convParams.prevFeatureMapReshape, layer->convParams.prevFeatureMapReshapeIndex, prevChannel, prevRow*prevCol, kernelRow*kernelCol*row*col, deviceId, threadId);
   }

//    double *kernelPtr;
//    double *kernelGradAccumPtr;
//    double *prevFeatureMapPtr;
//    double *prevDeltaPtr;
//    double *deltaPtr;
//	for (i = 0; i < prevChannel; i++){
//		// prevDelta[i]
//		prevDeltaPtr = layer->prev->delta + prevRow*prevCol*i;
//		// prevFeatureMap[i]
//		prevFeatureMapPtr = layer->prev->featureMap + prevRow*prevCol*i;
//		for(j = 0; j < channel; j++){
//			if (layer->convParams.connectivity[i*channel + j]){
//				// delta[j]
//				deltaPtr = layer->delta + row*col*j;
//				// kernel[i, j] =====> need to implement connectivity
//				kernelPtr = layer->convParams.kernel + kernelRow*kernelCol*i*channel + j*kernelRow*kernelCol;
//				kernelGradAccumPtr = layer->convParams.kernelGradAccum + kernelRow*kernelCol*i*channel + j*kernelRow*kernelCol;
//				// prevDeltaTemp = conv2(delta[j], rot180(kernel[i, j]), 'full')
//				timlUtilCorr2Full(deltaPtr, kernelPtr, layer->convParams.prevDeltaTemp,
//						row, col, kernelRow, kernelCol);
//				// prevDelta[i] += prevDeltaTemp
//				cblas_daxpy(prevRow*prevCol, 1.0, layer->convParams.prevDeltaTemp, 1, prevDeltaPtr, 1);
//				// kernel gradient[i, j] = conv2(rot180(prevFeatureMap[i]), delta[j], 'valid')
//				timlCNNKernalGradient(prevFeatureMapPtr, deltaPtr, layer->convParams.kernelTemp,
//						prevRow, prevCol, row, col);
//				// add kernel gradient[i, j] to kernel gradient accumulator
//				cblas_daxpy(kernelRow*kernelCol, 1.0, layer->convParams.kernelTemp, 1, kernelGradAccumPtr, 1);
//			}
//		}
//	}
//
//	// bias gradient[j] = sum(delta[j]), and add to the bias gradient accumulator
//	for(j = 0; j < channel; j++){
//		layer->convParams.biasGradAccum[j] += timlUtilDoubleVectorSum(layer->delta + row*col*j, row*col);
//	}

   return 0;
}
