/******************************************************************************/
/*!
 * \file timlCNNSupervisedTrainingWithLabelBatchModeOpenMP.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup cnn
 * \brief supervised training with label using openmp
 * \param[in,out] cnn
 * \param[in] data data batch
 * \param[in] label
 * \param[in] dim data dimension
 * \param[in] num data number
 * \return error code
 */
/******************************************************************************/

int timlCNNSupervisedTrainingWithLabelBatchModeOpenMP(timlConvNeuralNetwork *cnn, float *data, int *label, int dim, int num)
{
   int          i;
   int          t;
   int          thread;
   int          err;
   timlCNNLayer *bpStartLayer;
   float        *cost;
   float        batchCost;

   err    = 0;
   cost   = malloc(sizeof(float)*num);
   thread = omp_get_max_threads();

   // create cnnTeam
   timlConvNeuralNetwork **cnnTeam = malloc(sizeof(timlConvNeuralNetwork*)*thread);
   cnnTeam[0] = cnn;
   for (i = 1; i < thread; i++) {
      cnnTeam[i] = timlCNNShareParams(cnn, 0);
   }

   // parallel for loop
   #pragma omp parallel num_threads(thread) private(t, i, bpStartLayer, err)
   {
      #pragma omp for
      for (i = 0; i < num; i++) {
         t = omp_get_thread_num();
         err = timlCNNForwardPropagation(cnnTeam[t], data + i*dim, dim);
         timlCNNCostWithLabel(cnnTeam[t], label[i], cost + i, &bpStartLayer);
         err = timlCNNBackPropagation(cnnTeam[t], bpStartLayer);
      }
   }

   // update params
   cnn->params.count += num;
   timlCNNUpdateParams(cnn);
   batchCost = timlUtilVectorSumFloat(cost, num)/(float)num;
   printf("batch = %d, cost = %f\n", cnn->params.batchCount, batchCost);
   cnn->params.batchCount += 1;

   // free cnnTeam
   for (i = 1; i < thread; i++) {
      timlCNNDelete(cnnTeam[i]);
   }
   free(cnnTeam);
   free(cost);

   return err;
}
