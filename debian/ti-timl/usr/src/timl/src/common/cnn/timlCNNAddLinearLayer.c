/******************************************************************************/
/*!
 * \file timlCNNAddLinearLayer.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/*******************************************************************************/
/**
 * \ingroup       cnn
 * \brief         Add linear layer
 * \param[in,out] cnn    CNN
 * \param[in]     dim    Output 1D feature map dimension
 * \param[in]     params Optional parameters
 * \return        Error code
 */
/*******************************************************************************/

int timlCNNAddLinearLayer(timlConvNeuralNetwork *cnn, int dim, timlCNNLinearParams params)
{
   int          prevDim;
   timlCNNLayer *prev;
   timlCNNLayer *linearLayer;

   // error checking
   if (cnn == NULL) {
      return ERROR_CNN_NULL_PTR;
   }
   if (cnn->tail == NULL) {
      return ERROR_CNN_EMPTY;
   }
   if (dim <= 0) {
      return ERROR_CNN_LINEAR_LAYER_DIM;
   }

   prev    = cnn->tail;
   prevDim = prev->row * prev->col * prev->channel;

   // allocate linear layer
   linearLayer = (timlCNNLayer*)malloc(sizeof(timlCNNLayer));
   if (linearLayer == NULL) {
      return ERROR_CNN_LAYER_ALLOCATION;
   }

//   linearLayer->inputParams     = timlCNNInputParamsDefault();
//   linearLayer->convParams      = timlCNNConvParamsDefault();
//   linearLayer->poolingParams   = timlCNNPoolingParamsDefault();
//   linearLayer->normParams      = timlCNNNormParamsDefault();
//   linearLayer->nonlinearParams = timlCNNNonlinearParamsDefault();

   // load parameter
   linearLayer->linearParams = params;
   // override parameters
   linearLayer->linearParams.weight          = NULL;
   linearLayer->linearParams.weightInc       = NULL;
   linearLayer->linearParams.weightGradAccum = NULL;
   linearLayer->linearParams.bias            = NULL;
   linearLayer->linearParams.biasInc         = NULL;
   linearLayer->linearParams.biasGradAccum   = NULL;
   linearLayer->linearParams.prevDim         = prevDim;
   linearLayer->linearParams.dim             = dim;

   linearLayer->type           = CNN_Linear;
   linearLayer->featureMap     = NULL;
   linearLayer->delta          = NULL;
   linearLayer->row            = 1;
   linearLayer->col            = 1;
   linearLayer->channel        = dim;
   linearLayer->allocatorLevel = cnn->params.allocatorLevel;
   linearLayer->phase          = cnn->params.phase;

   // link the linearLayer
   linearLayer->cnn        = cnn;
   linearLayer->id         = prev->id + 1;
   linearLayer->prev       = prev;
   linearLayer->prev->next = linearLayer;
   linearLayer->next       = NULL;
   cnn->tail               = linearLayer;

   return 0;

}
