/******************************************************************************/
/*!
 * \file timlCNNLinearUpdateParams.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Update the parameters of the linear layer
 * \param[in,out] layer Layer ptr
 * \return        Error code
 */
/******************************************************************************/

int timlCNNLinearUpdateParams(timlCNNLayer *layer)
{
   timlConvNeuralNetwork *cnn;
   int   count;
   int   dim;
   int   prevDim;
   float weightLearningRate;
   float biasLearningRate;
   float weightDecay;
   int   deviceId;
   int   threadId;

   // init
   cnn                = layer->cnn;
   count              = cnn->params.count;
   dim                = layer->channel;
   prevDim            = layer->prev->row*layer->prev->col*layer->prev->channel;
   weightLearningRate = cnn->params.learningRate*layer->linearParams.weightLearningFactor;
   biasLearningRate   = cnn->params.learningRate*layer->linearParams.biasLearningFactor;
   weightDecay        = cnn->params.weightDecay*layer->linearParams.weightDecayFactor;
   deviceId           = cnn->deviceId;
   threadId           = cnn->threadId;

   // update weight
   // weightGradAccum = weightGradAccum / count
   timlUtilBLASsscal(dim * prevDim, (1.0/count), layer->linearParams.weightGradAccum, deviceId, threadId);
   // weightGradAccum = weightGradAccum + weightDecay * weight
   timlUtilBLASsaxpy(dim * prevDim, weightDecay, layer->linearParams.weight, layer->linearParams.weightGradAccum, deviceId, threadId);
   // weightGradAccum = weightGradAccum * learningRate
   timlUtilBLASsscal(dim * prevDim, weightLearningRate, layer->linearParams.weightGradAccum, deviceId, threadId);
   // weightGradAccum = weightGradAccum + momentum * weightInc
   timlUtilBLASsaxpy(dim * prevDim, cnn->params.momentum, layer->linearParams.weightInc, layer->linearParams.weightGradAccum, deviceId, threadId);
   // weightInc = weightGradAccum
   timlUtilBLASscopy(dim * prevDim, layer->linearParams.weightGradAccum, layer->linearParams.weightInc, deviceId, threadId);
   // weight = weight + (-1) * weightInc
   timlUtilBLASsaxpy(dim * prevDim, -1.0, layer->linearParams.weightInc, layer->linearParams.weight, deviceId, threadId);
   timlUtilVectorResetFloat(layer->linearParams.weightGradAccum, dim * prevDim, 0.0, deviceId, threadId);

   // update bias
   // biasGradAccum = learningRate * biasGradAccum / count
   timlUtilBLASsscal(dim, biasLearningRate / count, layer->linearParams.biasGradAccum, deviceId, threadId);
   // biasInc = biasInc * momentum
   timlUtilBLASsscal(dim, cnn->params.momentum, layer->linearParams.biasInc, deviceId, threadId);
   // biasInc = biasInc + biasGradAccum
   timlUtilBLASsaxpy(dim, 1.0, layer->linearParams.biasGradAccum, layer->linearParams.biasInc, deviceId, threadId);
   // bias = bias - biasInc
   timlUtilBLASsaxpy(dim, -1.0, layer->linearParams.biasInc, layer->linearParams.bias, deviceId, threadId);
   // reset biasGradAccum
   timlUtilVectorResetFloat(layer->linearParams.biasGradAccum, dim, 0.0, deviceId, threadId);

   return 0;
}
