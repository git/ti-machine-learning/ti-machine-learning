/******************************************************************************/
/*!
 * \file timlCNNLinearWriteToFile.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Write the linear layer to file(s)
 * \param[in,out] fp1         FILE ptr to the level 1 text file
 * \param[in,out] fp2         FILE ptr to the level 2 bin file
 * \param[in,out] fp3         FILE ptr to the level 3 bin file
 * \param[in]     layer       Layer ptr
 * \param[in]     level       Write level
 * \param[in]     name        CNN name
 * \param[in]     floatFormat Format string for floats
 * \param[in]     intFormat   Format string for ints
 * \return        Error code
 */
/******************************************************************************/

int timlCNNLinearWriteToFile(FILE *fp1, FILE *fp2, FILE *fp3, timlCNNLayer *layer, timlUtilParamsLevel level, const char* name, const char *floatFormat, const char *intFormat)
{
   int  err;
   err = 0;
   fprintf(fp1, "%s.layer(%d).linearParams.dim = %d;\n", name, layer->id + 1, layer->linearParams.dim);
   fprintf(fp1, "%s.layer(%d).linearParams.prevDim = %d;\n", name, layer->id + 1, layer->linearParams.prevDim);
   fprintf(fp1, "%s.layer(%d).linearParams.weightDecayFactor = ", name, layer->id + 1);
   fprintf(fp1, floatFormat, layer->linearParams.weightDecayFactor);
   fprintf(fp1, ";\n");
   fprintf(fp1, "%s.layer(%d).linearParams.weightInit.type = %d;\n", name, layer->id + 1, layer->linearParams.weightInit.type);
   fprintf(fp1, "%s.layer(%d).linearParams.weightLearningFactor =", name, layer->id + 1);
   fprintf(fp1, floatFormat, layer->linearParams.weightLearningFactor);
   fprintf(fp1, ";\n");
   fprintf(fp1, "%s.layer(%d).linearParams.biasInit.type = %d;\n", name, layer->id + 1, layer->linearParams.biasInit.type);
   fprintf(fp1, "%s.layer(%d).linearParams.biasLearningFactor =", name, layer->id + 1);
   fprintf(fp1, floatFormat, layer->linearParams.biasLearningFactor);
   fprintf(fp1, ";\n");

   if (fp2 != NULL) {
      timlUtilFwrite(layer->linearParams.weight, sizeof(float), layer->linearParams.dim*layer->linearParams.prevDim, fp2);
      timlUtilFwrite(layer->linearParams.bias, sizeof(float), layer->linearParams.dim, fp2);
   }

   if (fp3 != NULL) {
      timlUtilFwrite(layer->linearParams.weightInc, sizeof(float), layer->linearParams.dim*layer->linearParams.prevDim, fp3);
      timlUtilFwrite(layer->linearParams.weightGradAccum, sizeof(float), layer->linearParams.dim*layer->linearParams.prevDim, fp3);
      timlUtilFwrite(layer->linearParams.biasInc, sizeof(float), layer->linearParams.dim, fp3);
      timlUtilFwrite(layer->linearParams.biasGradAccum, sizeof(float), layer->linearParams.dim, fp3);
   }

   return err;
}
