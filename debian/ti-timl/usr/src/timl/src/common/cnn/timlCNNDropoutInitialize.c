/******************************************************************************/
/*!
 * \file timlCNNDropoutInitialize.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup cnn
 * \brief     Initialize the dropout layer
 * \param[in] layer Dropout layer
 * \return    Error code
 */
/******************************************************************************/

int timlCNNDropoutInitialize(timlCNNLayer *layer)
{
   timlConvNeuralNetwork *cnn = layer->cnn;
   if (layer->allocatorLevel == Util_AllocatorLevel1) {
      // allocate feature map
      if (timlUtilMalloc((void**) &(layer->featureMap), sizeof(float) * layer->row * layer->col * layer->channel)) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }

      // allocate delta
      if (timlUtilMalloc((void**) &(layer->delta), sizeof(float) * layer->row * layer->col * layer->channel)) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }

      // allocate mask
      if (timlUtilMalloc((void**) &(layer->dropoutParams.mask), sizeof(int) * layer->row * layer->col * layer->channel)) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }

      // allocate random vector
      if (timlUtilMalloc((void**) &(layer->dropoutParams.randomVector), sizeof(unsigned int) * layer->row * layer->col * layer->channel)) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }
   }

   if (layer->allocatorLevel == Util_AllocatorLevel2) {
      // allocate feature map
      if (timlUtilMalloc((void**) &(layer->featureMap), sizeof(float) * layer->row * layer->col * layer->channel)) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }
   }

   if (layer->allocatorLevel == Util_AllocatorLevel3) {
      if (layer->id%2 == 0) { // layer 2, 4, 6 8, ... allocate at the back end
         layer->featureMap = cnn->memPool + cnn->memPoolSize - layer->channel*layer->row*layer->col;
      }
      else { // layer 1, 3, 5, ... allocate at the front end
         layer->featureMap = cnn->memPool;
      }
   }

   return 0;
}
