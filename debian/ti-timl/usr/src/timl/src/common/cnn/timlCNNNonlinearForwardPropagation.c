/******************************************************************************/
/*!
 * \file timlCNNNonlinearForwardPropagation.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup cnn
 * \brief     Forward propagate form layer to layer->next
 * \param[in] prevLayer Previous layer
 * \return    Error code
 */
/******************************************************************************/

int timlCNNNonlinearForwardPropagation(timlCNNLayer *prevLayer)
{
   timlCNNLayer *layer;
   int row;
   int col;
   int channel;
   int dim;
   int err;
   int deviceId;
   int threadId;

   layer    = prevLayer->next;
   row      = prevLayer->row;
   col      = prevLayer->col;
   channel  = prevLayer->channel;
   dim      = row*col*channel;
   err      = 0;
   deviceId = prevLayer->cnn->deviceId;
   threadId = prevLayer->cnn->threadId;

   // activation &  derivative
   switch (layer->nonlinearParams.type) {
      case Util_Sigmoid:
         if (layer->phase == Util_Train) {
            err = timlUtilSigmoidDerivative(prevLayer->featureMap, layer->nonlinearParams.derivative, dim, deviceId, threadId);
         }
         err = timlUtilSigmoid(prevLayer->featureMap, layer->featureMap, dim, deviceId, threadId);
         break;
      case Util_Relu:
         if (layer->phase == Util_Train) {
            err = timlUtilReluDerivative(prevLayer->featureMap, layer->nonlinearParams.derivative, dim, deviceId, threadId);
         }
         err = timlUtilRelu(prevLayer->featureMap, layer->featureMap, dim, deviceId, threadId);
         break;
      case Util_Nrelu:
         break;
      case Util_Tanh:
         if (layer->phase == Util_Train) {
            err = timlUtilTanhDerivative(prevLayer->featureMap, layer->nonlinearParams.derivative, dim, deviceId, threadId);
         }
         err = timlUtilTanh(prevLayer->featureMap, layer->featureMap, dim, deviceId, threadId);
         break;
      case Util_Softmax:
//		err = timlUtilSoftmaxDerivative(prevLayer->featureMap, layer->nonlinearParams.derivative, dim);
         err = timlUtilSoftmax(prevLayer->featureMap, layer->featureMap, row, col, channel, deviceId, threadId);
         break;
      case Util_Softplus:
         break;
      case Util_Linear:
         break;
   }

   return err;
}
