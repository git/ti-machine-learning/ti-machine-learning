/******************************************************************************/
/*!
 * \file timlCNNConvForwardPropagation.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Forward propagate form layer to layer->next
 * \param[in]     prevLayer Previous layer ptr
 * \return        Error code
 */
/******************************************************************************/

int timlCNNConvForwardPropagation(timlCNNLayer *prevLayer)
{
   int          err;
   int          M;
   int          N;
   int          K;
   timlCNNLayer *layer;
   int          prevFeatureMapRow;
   int          prevFeatureMapCol;
   int          prevFeatureMapChannel;
   int          featureMapRow;
   int          featureMapCol;
   int          featureMapChannel;
   int          kernelRow;
   int          kernelCol;
   int          deviceId;
   int          threadId;

   // init
   err                   = 0;
   layer                 = prevLayer->next;
   prevFeatureMapRow     = prevLayer->row;
   prevFeatureMapCol     = prevLayer->col;
   prevFeatureMapChannel = prevLayer->channel;
   featureMapRow         = layer->row;
   featureMapCol         = layer->col;
   featureMapChannel     = layer->channel;
   kernelRow             = layer->convParams.kernelRow;
   kernelCol             = layer->convParams.kernelCol;
   deviceId              = prevLayer->cnn->deviceId;
   threadId              = prevLayer->cnn->threadId;
   M                     = featureMapChannel;
   K                     = kernelRow*kernelCol*prevFeatureMapChannel;
   N                     = featureMapRow*featureMapCol;

   timlUtilConv2ImageReshape(layer->convParams.prevFeatureMapReshape, prevLayer->featureMap, layer->convParams.prevFeatureMapReshapeIndex, prevFeatureMapChannel, prevFeatureMapRow*prevFeatureMapCol, kernelRow*kernelCol*featureMapRow*featureMapCol, deviceId, threadId);
   // featureMap = kernel * prevFeatureMapReshape
   timlUtilBLASsgemm(CblasNoTrans, CblasNoTrans, M, N, K, 1.0, layer->convParams.kernel, layer->convParams.prevFeatureMapReshape, 0.0, layer->featureMap, deviceId, threadId);
   timlUtilBLASsgemm(CblasNoTrans, CblasNoTrans, M, N, 1, 1.0, layer->convParams.bias, layer->convParams.biasMultiplier, 1.0, layer->featureMap, deviceId, threadId);

//    float *prevFeatureMapPtr;
//    float *featureMapPtr;
//    float *kernelPtr;
//
//    // reshape the prev feature map
//    for (j = 0; j < prevFeatureMapChannel; j++){
//       timlUtilConv2ImageReshape(layer->convParams.prevFeatureMapReshape + j*(kernelRow*kernelCol*featureMapRow*featureMapCol),
//             prevLayer->featureMap + j*prevFeatureMapRow*prevFeatureMapCol, layer->convParams.prevFeatureMapReshapeIndex,
//             kernelRow*kernelCol*featureMapRow*featureMapCol);
//
//	for (j = 0; j < featureMapChannel; j++){
//		// featureMap[j] = bias[j]
//		featureMapPtr = layer->featureMap + featureMapRow*featureMapCol*j;
//		timlUtilFloatVectorReset(featureMapPtr, featureMapCol * featureMapRow, layer->convParams.bias[j]);
//		for (i = 0 ; i < prevFeatureMapChannel; i++){
//			// prevFeatureMap[i]
//			prevFeatureMapPtr = prevLayer->featureMap + prevFeatureMapRow*prevFeatureMapCol*i;
//			// kernel[i, j]
//			kernelPtr = layer->convParams.kernel + kernelRow*kernelCol*i + kernelRow*kernelCol*prevFeatureMapChannel*j;
//			// featureMapTemp = conv2(prevFeatureMap[i], kernel[i, j], 'valid')
//			timlUtilConv2Valid(prevFeatureMapPtr, kernelPtr, featureMapPtr,
//					prevFeatureMapRow, prevFeatureMapCol, kernelRow, kernelCol);
//			// featureMap[j] += featureMapTemp
//			cblas_daxpy(featureMapRow*featureMapCol, 1.0, layer->convParams.featureMapTemp, 1, featureMapPtr, 1);
//		}
//	}
//
//	 // featureMap{i} = featureMap{i} + bias[i]
//	 for (i = 0; i < featureMapChannel; i++){
//	    for(j = 0; j < N; j++){
//	       layer->featureMap[i*N + j] += layer->convParams.bias[i];
//	    }
//	 }

   return err;
}
