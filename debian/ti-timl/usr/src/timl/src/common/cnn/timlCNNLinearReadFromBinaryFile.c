/******************************************************************************/
/*!
 * \file timlCNNLinearReadFromBinaryFile.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Read the linear layer parameters from binary files
 * \param[in]     fp2   FILE ptr to the level 2 parameter bin file
 * \param[in]     fp3   FILE ptr to the level 3 state bin file
 * \param[in,out] layer Layer ptr
 * \return        Error code
 */
/******************************************************************************/

int timlCNNLinearReadFromBinaryFile(FILE *fp2, FILE *fp3, timlCNNLayer *layer)
{
   // copy params
   if (fp2 != NULL) {
      timlUtilFread(layer->linearParams.weight, sizeof(float), layer->linearParams.dim*layer->linearParams.prevDim, fp2);
      timlUtilFread(layer->linearParams.bias, sizeof(float), layer->linearParams.dim, fp2);
   }

   // copy state
   if (fp3 != NULL && layer->allocatorLevel == Util_AllocatorLevel1) {
      timlUtilFread(layer->linearParams.weightInc, sizeof(float), layer->linearParams.dim*layer->linearParams.prevDim, fp3);
      timlUtilFread(layer->linearParams.weightGradAccum, sizeof(float), layer->linearParams.dim*layer->linearParams.prevDim, fp3);
      timlUtilFread(layer->linearParams.biasInc, sizeof(float), layer->linearParams.dim, fp3);
      timlUtilFread(layer->linearParams.biasGradAccum, sizeof(float), layer->linearParams.dim, fp3);
   }

   return 0;
}
