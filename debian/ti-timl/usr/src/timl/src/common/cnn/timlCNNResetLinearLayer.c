/******************************************************************************/
/*!
 * \file timlCNNResetLinearLayer.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup   cnn
 * \brief     Reset linear layer
 * \param[in] layer Layer ptr
 * \return    Error code
 */
/******************************************************************************/

int timlCNNResetLinearLayer(timlCNNLayer *layer)
{
   int dim;
   int prevDim;
   int deviceId;
   int threadId;

   dim      = layer->channel;
   prevDim  = layer->prev->channel*layer->prev->row*layer->prev->col;
   deviceId = layer->cnn->deviceId;
   threadId = layer->cnn->threadId;

   // init weight
   switch (layer->linearParams.weightInit.type) {
      case Util_Xavier:
         timlUtilRandContinuousUniformRNG(layer->linearParams.weight, dim*prevDim, -sqrtf(3.0/dim/prevDim), sqrtf(3.0/dim/prevDim));
         break;
      case Util_Gaussian:
         timlUtilRandNormalRNG(layer->linearParams.weight, dim*prevDim, layer->linearParams.weightInit.mean, layer->linearParams.weightInit.std);
         break;
      case Util_Constant:
         timlUtilVectorResetFloat(layer->linearParams.weight, dim*prevDim, layer->linearParams.weightInit.val, deviceId, threadId);
         break;
      default:
         break;
   }

   // initialize bias
   switch (layer->linearParams.biasInit.type) {
      case Util_Xavier:
         timlUtilRandContinuousUniformRNG(layer->linearParams.bias, dim, -sqrtf(3.0/dim), sqrtf(3.0/dim));
         break;
      case Util_Gaussian:
         timlUtilRandNormalRNG(layer->linearParams.bias, dim, layer->linearParams.biasInit.mean, layer->linearParams.biasInit.std);
         break;
      case Util_Constant:
         timlUtilVectorResetFloat(layer->linearParams.bias, dim, layer->linearParams.biasInit.val, deviceId, threadId);
         break;
      default:
         break;
   }

   // training mode
   if (layer->allocatorLevel == Util_AllocatorLevel1) {
      timlUtilVectorResetFloat(layer->linearParams.weightInc, dim*prevDim, 0.0, deviceId, threadId);
      timlUtilVectorResetFloat(layer->linearParams.weightGradAccum, dim*prevDim, 0.0, deviceId, threadId);
      timlUtilVectorResetFloat(layer->linearParams.biasInc, dim, 0.0, deviceId, threadId);
      timlUtilVectorResetFloat(layer->linearParams.biasGradAccum, dim, 0.0, deviceId, threadId);
   }

   return 0;

}
