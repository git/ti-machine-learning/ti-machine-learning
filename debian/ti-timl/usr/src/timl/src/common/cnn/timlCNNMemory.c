/******************************************************************************/
/*!
 * \file timlCNNMemory.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup   cnn
 * \brief     Return the memory in bytes required by the cnn
 * \param[in] cnn CNN
 * \return    Required memory in byte
 */
/******************************************************************************/

long timlCNNMemory(timlConvNeuralNetwork *cnn)
{
   long         mem;
   timlCNNLayer *layer;
   int          factor;
   int          byte;

   byte  = sizeof(float);
   mem   = sizeof(*cnn);
   layer = cnn->head;
   while (layer != NULL) {
      switch (layer->type) {
         case CNN_Input:
            // mean
            if (layer->inputParams.shared == false) {
               mem += byte*layer->inputParams.row*layer->inputParams.col*layer->inputParams.channel;
            }
            break;
         case CNN_Conv:
            if (layer->allocatorLevel == Util_AllocatorLevel1) {
               factor = 3;
            }
            else {
               factor = 1;
            }
            // prevFeatureMapReshape
            mem += byte*layer->prev->channel*layer->convParams.kernelRow*layer->convParams.kernelCol*layer->row*layer->col;
            if (layer->convParams.shared == false) {
               // kernel
               mem += byte*layer->convParams.inputFeatureMapChannel*layer->convParams.inputFeatureMapChannel*layer->convParams.kernelRow*layer->convParams.kernelCol*factor;
               // bias
               mem += byte*layer->channel*factor;
               // prevFeatureMapReshapeIndex
               mem += byte*layer->convParams.kernelRow*layer->convParams.kernelCol*layer->row*layer->col;
//               // connectivity
//               mem += byte*layer->convParams.inputFeatureMapChannel*layer->convParams.inputFeatureMapChannel;
            }
            break;
         case CNN_Linear:
            if (layer->allocatorLevel == Util_AllocatorLevel1) {
               factor = 3;
            }
            else {
               factor = 1;
            }
            if (layer->linearParams.shared == false) {
               // weight
               mem += byte*layer->channel*(layer->prev->channel*layer->prev->row*layer->prev->col)*factor;
               // bias
               mem += byte*layer->channel*factor;
            }
            break;
         case CNN_Nonlinear:
            // derivative
            mem += byte*layer->col*layer->row*layer->channel;
            break;
         case CNN_Pooling:
            // window
            mem += byte*layer->poolingParams.scaleRow*layer->poolingParams.scaleCol;
            switch (layer->poolingParams.type) {
               case CNN_MaxPooling:
                  // max index
                  mem += byte* layer->col*layer->row*layer->channel;
                  break;
               case CNN_MeanPooling:
                  break;
            }
            break;
         case CNN_Norm:
            // denom
            mem += byte*layer->channel*layer->row*layer->col;
            break;
         case CNN_Dropout:
            mem += byte*layer->channel*layer->row*layer->col;
            break;
      }
      // feature map & delta
      if (layer->allocatorLevel == Util_AllocatorLevel1) {
         factor = 2;
      }
      else if (layer->allocatorLevel == Util_AllocatorLevel2) {
         factor = 1;
      }
      else
         factor = 0;
      mem += byte*layer->channel*layer->row*layer->col*factor;
      mem += sizeof(*layer);
      layer = layer->next;
   }
   if (cnn->params.allocatorLevel == Util_AllocatorLevel3)
      mem += byte*cnn->memPoolSize;

   return mem;
}
