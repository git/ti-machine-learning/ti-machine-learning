/******************************************************************************/
/*!
 * \file timlCNNClassifyTopNTeamModeOpenMP.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup cnn
 * \brief         Batch classification using openmp
 * \details       This is the same function as timlCNNBatchClassifyOpenMP but avoids creating and deleting the cnn team each time the function is called
 * \param[in,out] cnnTeam An array of CNNs that shares the same parameters
 * \param[in]     num     Size of the CNN array as well as the data
 * \param[in]     data    Data batch
 * \param[in]     dim     Data dimension
 * \param[in,out] label   Label array ptr, size = num*topN
 * \param[in,out] percent Percent array ptr, size = num*topN
 * \param[in,out] topN    Output the top N labels and the corresponding percentage
 * \return        Error code
 */
/******************************************************************************/

int timlCNNClassifyTopNTeamModeOpenMP(timlConvNeuralNetwork **cnnTeam, int num, float *data, int dim, int *label, float *percent, int topN)
{
   int err;
   int i;
   int j;
   int outputDim;
   int *index;
   int t;

   outputDim = cnnTeam[0]->tail->row*cnnTeam[0]->tail->col*cnnTeam[0]->tail->channel;
   index     = malloc(sizeof(int)*outputDim*num); // multiple copies for each thread

   #pragma omp parallel num_threads(num) private(i, j, t)
   {
      #pragma omp for
      for (j = 0; j < num; j++) {
         t = omp_get_thread_num();
         timlCNNForwardPropagation(cnnTeam[t], data + j*dim, dim); // fp
         timlUtilVectorSortIndexFloat(cnnTeam[t]->tail->featureMap, index + t*outputDim, outputDim); // sort
         for (i = 0; i < topN; i++) {
            label[j*topN + i] = index[t*outputDim + outputDim - i - 1]; // record label
            if (percent != NULL) {
               percent[j*topN + i] = cnnTeam[t]->tail->featureMap[index[t*outputDim + outputDim - i - 1]]; // record percent
            }
         }
      }
   }
   free(index);

   return err;
}
