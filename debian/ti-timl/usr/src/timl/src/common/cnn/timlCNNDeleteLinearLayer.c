/******************************************************************************/
/*!
 * \file timlCNNDeleteLinearLayer.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Delete linear layer
 * \param[in]     layer Layer ptr
 * \return        Error code
 */
/******************************************************************************/

int timlCNNDeleteLinearLayer(timlCNNLayer *layer)
{
   if (layer->allocatorLevel != Util_AllocatorLevel3) {
      if (layer->featureMap != NULL) {
         timlUtilFree(layer->featureMap);
      }
   }

   if (layer->delta != NULL) {
      timlUtilFree(layer->delta);
   }

   if (layer->linearParams.shared == false) {
      if (layer->linearParams.weight != NULL) {
         timlUtilFree(layer->linearParams.weight);
      }
      if (layer->linearParams.weightInc != NULL) {
         timlUtilFree(layer->linearParams.weightInc);
      }
      if (layer->linearParams.weightGradAccum != NULL) {
         timlUtilFree(layer->linearParams.weightGradAccum);
      }
      if (layer->linearParams.bias != NULL) {
         timlUtilFree(layer->linearParams.bias);
      }
      if (layer->linearParams.biasInc != NULL) {
         timlUtilFree(layer->linearParams.biasInc);
      }
      if (layer->linearParams.biasGradAccum != NULL) {
         timlUtilFree(layer->linearParams.biasGradAccum);
      }
   }
   free(layer);

   return 0;
}
