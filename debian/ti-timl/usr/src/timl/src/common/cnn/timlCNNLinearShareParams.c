/******************************************************************************/
/*!
 * \file timlCNNLinearShareParams.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup cnn
 * \brief         Share the parameters with other linear layer
 * \details       Add a layer to cnnShare that shares the same parameters as the linear layer
 * \param[in,out] cnnShare CNN that shares the same paramters with other cnn
 * \param[in]     layer    Target linear layer to share its parameters
 * \return        Error code
 */
/******************************************************************************/

int timlCNNLinearShareParams(timlConvNeuralNetwork *cnnShare, timlCNNLayer *layer)
{
   timlCNNLayer *layerCopy;
   int          err;

   err     = 0;
   err = timlCNNAddLinearLayer(cnnShare, layer->channel, layer->linearParams);
   if (err) {
      timlCNNDelete(cnnShare);
      return err;
   }
   layerCopy                               = cnnShare->tail;
   layerCopy->linearParams.shared          = true;
   layerCopy->linearParams.bias            = layer->linearParams.bias;
   layerCopy->linearParams.biasInc         = layer->linearParams.biasInc;
   layerCopy->linearParams.biasGradAccum   = layer->linearParams.biasGradAccum;
   layerCopy->linearParams.weight          = layer->linearParams.weight;
   layerCopy->linearParams.weightInc       = layer->linearParams.weightInc;
   layerCopy->linearParams.weightGradAccum = layer->linearParams.weightGradAccum;

   return 0;

}
