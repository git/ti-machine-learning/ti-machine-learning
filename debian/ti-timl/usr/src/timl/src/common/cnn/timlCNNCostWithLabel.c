/******************************************************************************/
/*!
 * \file timlCNNCostWithLabel.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Calculate the cost based on the cnn output and the label
 * \param[in]     cnn          CNN
 * \param[in]     label        Label
 * \param[in,out] cost         Cost
 * \param[in,out] bpStartLayer Back propagation start layer
 * \return        Error code
 */
/******************************************************************************/

int timlCNNCostWithLabel(timlConvNeuralNetwork *cnn, int label, float *cost, timlCNNLayer **bpStartLayer)
{
   int          err;
   timlCNNLayer *layer;
   int          dim;
   int          deviceId;
   int          threadId;

   // init
   err      = 0;
   layer    = cnn->tail;
   dim      = layer->channel;
   deviceId = cnn->deviceId;
   threadId = cnn->threadId;

   switch (cnn->params.costType) {
      case Util_MSE:
         *cost = 0.5*timlUtilMeanSqaureError(layer->featureMap, label, dim);
         break;
      case Util_CrossEntropy:
         *cost = timlUtilMultinomialCrossEntropy(layer->featureMap, label, dim);
         if (layer->type == CNN_Nonlinear && (layer->nonlinearParams.type == Util_Softmax || layer->nonlinearParams.type == Util_Sigmoid)) {
            layer = layer->prev; // move ahead one layer
            *bpStartLayer = layer;
         }
         timlUtilBLASscopy(dim, layer->next->featureMap, layer->delta, deviceId, threadId);
         // delta + label -= 1.0
         timlUtilSubtract(layer->delta + label, 1.0, deviceId, threadId);
         break;
   }

   return err;
}
