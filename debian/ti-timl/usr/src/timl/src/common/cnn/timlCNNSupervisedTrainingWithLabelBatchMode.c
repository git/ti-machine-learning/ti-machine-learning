/******************************************************************************/
 /*!
 * \file timlCNNSupervisedTrainingWithLabelBatchMode.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Supervised training with label
 * \param[in,out] cnn   CNN
 * \param[in]     data  Data batch
 * \param[in]     label Label ptr
 * \param[in]     dim   Data dimension
 * \param[in]     num   Batch size
 * \return        Error code
 */
/******************************************************************************/

int timlCNNSupervisedTrainingWithLabelBatchMode(timlConvNeuralNetwork *cnn, float *data, int *label, int dim, int num)
{
   int          j;
   int          err;
   timlCNNLayer *bpStartLayer;
   float        batchCost;
   float        *cost;

   err  = 0;
   cost = malloc(sizeof(float)*num);

   for (j = 0; j < num; j++) {
      cnn->params.count += 1;
      err = timlCNNForwardPropagation(cnn, data + j*dim, dim);
      timlCNNCostWithLabel(cnn, label[j], cost + j, &bpStartLayer);
      err = timlCNNBackPropagation(cnn, bpStartLayer);
   }

   timlCNNUpdateParams(cnn);
   batchCost = timlUtilVectorSumFloat(cost, num)/(float)num;
   printf("batch = %d, cost = %f\n", cnn->params.batchCount, batchCost);
   cnn->params.batchCount += 1;
   free(cost);

   return err;
}
