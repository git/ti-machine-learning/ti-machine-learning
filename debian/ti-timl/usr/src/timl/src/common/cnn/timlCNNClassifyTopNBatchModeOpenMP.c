/******************************************************************************/
/*!
 * \file timlCNNClassifyTopNBatchModeOpenMP.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Batch classification using openmp
 * \param[in,out] cnn     CNN
 * \param[in]     data    Data batch
 * \param[in]     dim     Data dimension
 * \param[in]     num     Data number
 * \param[out]    label   Label array ptr, size = num*topN
 * \param[out]    percent Percent array ptr, size = num*topN
 * \param[out]    topN    Output the top N labels and the corresponding percentage
 * \return        Error   code
 */
/******************************************************************************/

int timlCNNClassifyTopNBatchModeOpenMP(timlConvNeuralNetwork *cnn, float *data, int dim, int num, int *label, float *percent, int topN)
{
   int                   err;
   int                   i;
   int                   j;
   int                   t;
   int                   k;
   int                   thread;
   struct timespec       startTime;
   struct timespec       endTime;
   long                  testingTime;
   int                   outputDim;
   int                   *index;
   timlConvNeuralNetwork **cnnTeam;

   // init
   err       = 0;
   thread    = omp_get_max_threads();
   outputDim = cnn->tail->row*cnn->tail->col*cnn->tail->channel;
   index     = malloc(sizeof(int)*outputDim*thread); // multiple copies for each thread
   cnnTeam   = malloc(sizeof(timlConvNeuralNetwork*)*thread);

   // create cnnTeam
   cnnTeam[0] = cnn;
   for (i = 1; i < thread; i++) {
      cnnTeam[i] = timlCNNShareParams(cnn, 0);
      if (cnnTeam[i] == NULL) {
         return ERROR_CNN_TEAM_ALLOCATION;
      }
   }

   // batch iterations
   clock_gettime(CLOCK_REALTIME, &startTime);
   #pragma omp parallel num_threads(thread) private(i, j, t)
   {
      #pragma omp for
      for (j = 0; j < num; j++) {
         t = omp_get_thread_num();
         timlCNNForwardPropagation(cnnTeam[t], data + j*dim, dim); // fp
         timlUtilVectorSortIndexFloat(cnnTeam[t]->tail->featureMap, index + t*outputDim, outputDim); // sort
         for (i = 0; i < topN; i++) {
            label[j*topN + i] = index[t*outputDim + outputDim - i - 1]; // record label
            if (percent != NULL) {
               timlUtilBLASscopy(1, cnnTeam[t]->tail->featureMap + index[t*outputDim + outputDim - i - 1], percent + j*topN + i, cnnTeam[t]->deviceId, cnnTeam[t]->threadId);
            }
         }
      }
   }
   clock_gettime(CLOCK_REALTIME, &endTime);
   testingTime = timlUtilDiffTime(startTime, endTime);
//   printf("Loop testing time = %.2fs.\n", testingTime/1000000.0);

   // clean up
   for (i = 1; i < thread; i++) {
      timlCNNDelete(cnnTeam[i]);
   }
   free(cnnTeam);
   free(index);

   return err;
}
