/*****************************************************************************/
/*!
 * \file timlCNNClassifyTopNBatchMode.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Batch classification
 * \param[in,out] cnn     CNN
 * \param[in]     data    Data batch
 * \param[in]     dim     Data dimension
 * \param[in]     num     Data number
 * \param[out]    label  Label array ptr, size = num*topN
 * \param[out]    percent Percent array ptr, size = num*topN
 * \param[out]    topN    Output the top N labels and the corresponding percentage
 * \return        Error code
 */
/******************************************************************************/

int timlCNNClassifyTopNBatchMode(timlConvNeuralNetwork *cnn, float *data, int dim, int num, int *label, float *percent, int topN)
{
   int i;
   int j;
   int t;
   int k;
   int outputDim;
   int *index;
   int err;

   err       = 0;
   outputDim = cnn->tail->row*cnn->tail->col*cnn->tail->channel;
   index     = malloc(sizeof(int)*outputDim); // multiple copies for each thread

   for (j = 0; j < num; j++) {
      err = timlCNNForwardPropagation(cnn, data + j*dim, dim); // fp
      if (err) return err;
      err = timlUtilVectorSortIndexFloat(cnn->tail->featureMap, index, outputDim); // sort
      if (err) return err;
      for (i = 0; i < topN; i++) {
         label[j*topN + i] = index[outputDim - i - 1]; // record label
         if (percent != NULL) {
            timlUtilBLASscopy(1, cnn->tail->featureMap + index[outputDim - i - 1], percent + j*topN + i, cnn->deviceId, cnn->threadId);
         }
      }
   }
   free(index);

   return err;
}
