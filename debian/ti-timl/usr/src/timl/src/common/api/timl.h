#ifndef TIML_H_
#define TIML_H_


/******************************************************************************/
/*!
 * \file timl.h
 * \brief    timl public APIs
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 *  INCLUDES
 *
 ******************************************************************************/

#include "timlUtil.h"
#include "timlCNN.h"


/*******************************************************************************
 *
 *  DEFINITIONS
 *
 ******************************************************************************/

#define TIML_CPU
#ifndef TIML_CPU
#define TIML_ALT
#endif


/*******************************************************************************
 *
 * CNN PUBLIC FUNCTION PROTOTYPES
 *
 ******************************************************************************/

//! \ingroup cnn
//@{


/*******************************************************************************
 *
 * DEFAULT PARAMS FUNCTIONS
 *
 ******************************************************************************/

timlCNNInputParams timlCNNInputParamsDefault();

timlCNNConvParams timlCNNConvParamsDefault();

timlCNNLinearParams timlCNNLinearParamsDefault();

timlCNNPoolingParams timlCNNPoolingParamsDefault();

timlCNNNonlinearParams timlCNNNonlinearParamsDefault();

timlCNNNormParams timlCNNNormParamsDefault();

timlCNNTrainingParams timlCNNTrainingParamsDefault();


/*******************************************************************************
 *
 * CNN SETUP FUNCTIONS
 *
 ******************************************************************************/

timlConvNeuralNetwork *timlCNNCreateConvNeuralNetwork(timlCNNTrainingParams params, int deviceId);

int timlCNNAddInputLayer(timlConvNeuralNetwork *cnn, int featureMapRow, int featureMapCol, int featureMapChannel, timlCNNInputParams params);

int timlCNNAddPoolingLayer(timlConvNeuralNetwork *cnn, int scaleRow, int scaleCol, int strideX, int strideY, timlCNNPoolingType type, timlCNNPoolingParams params);

int timlCNNAddNormLayer(timlConvNeuralNetwork *cnn, timlCNNNormParams params);

int timlCNNAddConvLayer(timlConvNeuralNetwork *cnn, int kernelRow, int kernelCol, int strideX, int strideY, int featureMapChannel, timlCNNConvParams params);

int timlCNNAddNonlinearLayer(timlConvNeuralNetwork *cnn, timlUtilActivationType type);

int timlCNNAddLinearLayer(timlConvNeuralNetwork *cnn, int dim, timlCNNLinearParams params);

int timlCNNAddDropoutLayer(timlConvNeuralNetwork *cnn, float prob);

int timlCNNInitialize(timlConvNeuralNetwork *cnn);

int timlCNNReset(timlConvNeuralNetwork *cnn);

int timlCNNDelete(timlConvNeuralNetwork *cnn);


/*******************************************************************************
 *
 * CNN TRAINING AND TESTING FUNCTIONS
 *
 ******************************************************************************/

int timlCNNSupervisedTrainingWithLabelBatchMode(timlConvNeuralNetwork *cnn, float *data, int *label, int dim, int num);

int timlCNNClassifyTopNBatchMode(timlConvNeuralNetwork *cnn, float *data, int dim, int num, int *label, float *percent, int topN);

int timlCNNClassifyTop1SingleMode(timlConvNeuralNetwork *cnn, float *data, int dim);

int timlCNNSetMode(timlConvNeuralNetwork *cnn, timlUtilPhase phase);


/*******************************************************************************
 *
 * CNN AUX FUNCTIONS
 *
 ******************************************************************************/

timlConvNeuralNetwork* timlCNNClone(timlConvNeuralNetwork *cnn, int deviceId);

timlConvNeuralNetwork* timlCNNShareParams(timlConvNeuralNetwork *cnn, int deviceId);

long timlCNNMemory(timlConvNeuralNetwork *cnn);

long timlCNNGetParamsNum(timlConvNeuralNetwork *cnn);

int timlCNNWriteToFile(const char * fileName, timlConvNeuralNetwork *cnn, timlUtilParamsLevel paramsLevel, const char* name, const char *floatFormat, const char *intFormat);

timlConvNeuralNetwork* timlCNNReadFromFile(const char * fileName, int deviceId);

int timlCNNPrint(timlConvNeuralNetwork *cnn);

int timlCNNProfile(timlConvNeuralNetwork *cnn, float *data, int dim, int num, int *label, int iter);

int timlCNNResize(timlConvNeuralNetwork *cnn, int row, int col, int channel);

int timlCNNGetLayerNum(timlConvNeuralNetwork *cnn);

int timlCNNResize(timlConvNeuralNetwork *cnn, int row, int col, int channel);

//@}
#endif
