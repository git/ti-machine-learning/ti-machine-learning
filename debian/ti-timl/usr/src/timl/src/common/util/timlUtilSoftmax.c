/******************************************************************************/
/*!
 * \file timlUtilSoftmax.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"
#ifdef TIML_ALT
#include "../../alt/timlAlt.h"
#endif


/******************************************************************************/
/*!
 * \ingroup    util
 * \brief      Softmax function
 * \param[in]  x        Input
 * \param[out] y        Outupt
 * \param[in]  row      x row size
 * \param[in]  col      x col size
 * \param[in]  channel  x channel size
 * \param[in]  deviceId Device id
 * \param[in]  threadId Thread id
 * \return     Error code
 */
/******************************************************************************/

int timlUtilSoftmax(float *x, float *y, int row, int col, int channel, int deviceId, int threadId)
{
#ifdef TIML_CPU
   int   i;
   int   r;
   int   c;
   float max;
   float sum;
   int increment;
   increment = row*col;
   for (r = 0; r < row; r++) {
      for (c = 0; c < col; c++) {
         max = timlUtilVectorMaxFloat(x + r*col + c, channel, row*col);
         sum = 0.0;
         for (i = 0; i < channel; i++) {
            sum += expf(x[i*increment + r*col + c] - max);
         }
         for (i = 0; i < channel; i++) {
            y[i*increment + r*col + c] = expf(x[i*increment + r*col + c] - max)/sum;
         }
      }
   }

   return 0;
#elif defined TIML_ALT
   return timlUtilSoftmaxAlt(x, y, row, col, channel, deviceId, threadId);
#endif
}
