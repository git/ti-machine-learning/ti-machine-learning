/******************************************************************************/
/*!
 * \file timlUtilMasking.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"
#ifdef TIML_ALT
#include "../../alt/timlAlt.h"
#endif


/******************************************************************************/
/*!
 * \ingroup    util
 * \brief      Masking feature maps
 * \param[in]  inputMap     Input feature map
 * \param[out] outputMap    Output feature map
 * \param[out] mask         Mask vector of values {0,1}
 * \param[in]  randomVector A uniform random vector in [0,1]
 * \param[in]  dim          Dimension of the feature map (row*col*channel)
 * \param[in]  prob         Dropout probability
 * \param[in]  deviceId     Device id
 * \param[in]  threadId     Thread id
 * \return     Error code
 */
/******************************************************************************/

int timlUtilMasking(float *inputMap, float *outputMap, int *mask, unsigned int *randomVector, int dim, float prob, int deviceId, int threadId)
{
#ifdef TIML_CPU
   int i;
   float randomValue;
   float scale = 1.0/(1.0 - prob);
   for (i = 0; i < dim; i++) {
      timlUtilRandContinuousUniformRNG(&randomValue, 1, 0, 1);
      mask[i] = (randomValue < (1 - prob));
      if (mask[i]) { // not dropout
         outputMap[i] = scale*inputMap[i];
      }
      else {
         outputMap[i] = 0.0;
      }
   }

   return 0;
#elif defined TIML_ALT
   return timlUtilMaskingAlt(inputMap, outputMap, mask, randomVector, dim, prob, deviceId, threadId);
#endif
}
