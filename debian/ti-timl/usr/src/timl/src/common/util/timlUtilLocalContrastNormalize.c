/******************************************************************************/
/*!
 * \file timlUtilLocalContrastNormalize.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"
#ifdef TIML_ALT
#include "../../alt/timlAlt.h"
#endif


/******************************************************************************/
/*!
 * \ingroup    util
 * \brief      Local contrast normalization
 * \param[in]  inputMap  Input feature map
 * \param[out] outputMap Output feature map
 * \param[out] denom     Feature map denom
 * \param[in]  row       Input feature map row
 * \param[in]  col       Input feature map col
 * \param[in]  channel   Input feature map channel
 * \param[in]  N         Channel span
 * \param[in]  alpha     Alpha
 * \param[in]  beta      Beta
 * \param[in]  deviceId  Device id
 * \param[in]  threadId  Thread id
 * \return     Error code
 */
/******************************************************************************/

int timlUtilLocalContrastNormalize(float *inputMap, float *outputMap, float *denom, int row, int col, int channel, int N, float alpha, float beta, int deviceId, int threadId)
{
#ifdef TIML_CPU
   int channelStartIndex;
   int channelEndIndex;
   int i;
   int j;
   int p;
   float sum;

   for (p = 0; p < row*col; p++) {
      // for each channel
      for (i = 0; i < channel; i++) {
         if (i - N/2 < 0) {
            channelStartIndex = 0;
         }
         else {
            channelStartIndex = i - N/2;
         }
         if (i + N/2 > channel - 1) {
            channelEndIndex = channel - 1;
         }
         else {
            channelEndIndex = i + N/2;
         }
         sum = 0.0; // sum(p) = sum(prev feature map{j}(p))

         // inter-channel
         for (j = channelStartIndex; j <= channelEndIndex; j++) {
            sum += inputMap[j*row*col + p]*inputMap[j*row*col + p];
         }
         denom[i*row*col + p] += sum*alpha/N;
         outputMap[i*row*col + p] = inputMap[i*row*col + p]/powf(denom[i*row*col + p], beta);
      }
   }

   return 0;
#elif defined TIML_ALT
   return timlUtilLocalContrastNormalizeAlt(inputMap, outputMap, denom, row, col, channel, N, alpha, beta, deviceId, threadId);
#endif
}
