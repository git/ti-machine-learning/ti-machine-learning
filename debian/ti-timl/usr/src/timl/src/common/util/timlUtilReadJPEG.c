/******************************************************************************/
/*!
 * \file     timlUtilReadJPEG.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"

/******************************************************************************/
/**
 * \ingroup util
 * \brief read a jpg image
 * \param[in] name image name
 * \return timlUtilImage structure
 */
/******************************************************************************/

timlUtilImage timlUtilReadJPEG(const char *name) {
   timlUtilImage image;
   JSAMPARRAY buffer; /* Output row buffer */
   FILE *infile;
   int row_stride;
   int i, j;
   struct jpeg_decompress_struct cinfo;
   struct jpeg_error_mgr jerr;
   cinfo.err = jpeg_std_error(&jerr);
   jpeg_create_decompress(&cinfo);
   infile = fopen(name, "rb");
   jpeg_stdio_src(&cinfo, infile);
   jpeg_read_header(&cinfo, TRUE);
   jpeg_start_decompress(&cinfo);

   image.channel = cinfo.output_components;
   image.row = cinfo.output_height;
   image.col = cinfo.output_width;
   image.data = malloc(sizeof(float) * image.channel * image.row * image.col);

   // buffer for one row (R,G,B), (R,G,B), .....
   row_stride = cinfo.output_width * cinfo.output_components;
   buffer = (*cinfo.mem->alloc_sarray)((j_common_ptr) &cinfo, JPOOL_IMAGE,
      row_stride, 1);

   while (cinfo.output_scanline < cinfo.output_height) {
      jpeg_read_scanlines(&cinfo, buffer, 1);
      i = cinfo.output_scanline - 1;
      if (image.channel == 1) {
         for (j = 0; j < image.col; j++) {
            image.data[i * image.col + j] = (float) buffer[0][j];
         }
      }
      else {
         for (j = 0; j < image.col; j++) {
            image.data[i * image.col + j] = (float) buffer[0][j * 3]; // R
            image.data[i * image.col + j + image.col * image.row] =
               (float) buffer[0][3 * j + 1]; // G
            image.data[i * image.col + j + 2 * image.col * image.row] =
               (float) buffer[0][3 * j + 2]; // B
         }
      }
   }
   jpeg_finish_decompress(&cinfo);
   jpeg_destroy_decompress(&cinfo);
   fclose(infile);
   return image;

}
