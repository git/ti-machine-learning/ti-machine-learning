/******************************************************************************/
/*!
 * \file timlUtilConv2ImageReshapeIndex.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"
#ifdef TIML_ALT
#include "../../alt/timlAlt.h"
#endif


/******************************************************************************/
/*!
 * \ingroup    util
 * \brief      Create a reshaping index matrix
 * \details    Feature maps need to be reshaped so that a 2d convolution can be converted to a GEMM operation.
 * The reshaping index matrix records the index mapping between the original feature maps and the reshaped feature maps
 * \param[out] index    Reshaping index matrix
 * \param[in]  aRow     Feature map row
 * \param[in]  aCol     Feature map col
 * \param[in]  bRow     Kernel row
 * \param[in]  bCol     Kernel col
 * \param[in]  padUp    Padding for the up border of the image
 * \param[in]  padDown  Padding for the down border of the image
 * \param[in]  padLeft  Padding for the left border of the image
 * \param[in]  padRight Padding for the right border of the image
 * \param[in]  strideX  Horizontal stride for the kernel
 * \param[in]  strideY  Vertical stride for the kernel
 * \param[in]  type     Convolution or correlation
 * \return     Error code
 */
/******************************************************************************/

int timlUtilConv2ImageReshapeIndex(int *index, int aRow, int aCol, int bRow, int bCol, int padUp, int padDown, int padLeft, int padRight, int strideX, int strideY, timlUtilConvType type)
{
#ifdef TIML_CPU
   int x;
   int y;
   int i;
   int j;
   int p;
   int q;
   int cCol = (aCol - bCol + padLeft + padRight)/strideX + 1;
   int cRow = (aRow - bRow + padUp + padDown)/strideY + 1;

   for (i = 0; i < cRow; i++) {
      for (j = 0; j < cCol; j++) {
         for (p = 0; p < bRow; p++) {
            for (q = 0; q < bCol; q++) {
               // flip
               if (type == Util_Conv2D) {
                  x = bRow + i*strideY - padUp - p - 1;
                  y = bCol + j*strideX - padLeft - q - 1;
               }
               else {
                  // no flip
                  x = i*strideY - padUp + p;
                  y = j*strideX - padLeft + q;
               }

               if (x >= 0 && x < aRow && y >= 0 && y < aCol) {
                  // index(p*bCol + q, i*cCol + j) = (x, y)
                  index[(p*bCol + q)*(cCol*cRow) + (i*cCol + j)] = x*aCol + y;
               }
               else {
                  index[(p*bCol + q)*(cCol*cRow) + (i*cCol + j)] = -1;
               }
            }
         }
      }
   }

   return 0;
#elif defined TIML_ALT
   return timlUtilConv2ImageReshapeIndexAlt(index, aRow, aCol, bRow, bCol, padUp, padDown, padLeft, padRight, strideX, strideY, type);
#endif
}
