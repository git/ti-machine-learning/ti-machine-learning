/******************************************************************************/
/*!
 * \file timlUtilBLAS.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"
#ifdef TIML_ALT
#include "../../alt/timlAlt.h"
#endif


/******************************************************************************/
/*!
 * \ingroup util
 * \brief   Double general matrix matrix multiplication
 * C = alpha * op(A) * op(B) + beta * C
 * op(A) : M*K
 * op(B) : K*N
 */
/******************************************************************************/

void timlUtilBLASdgemm(const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_TRANSPOSE TransB, const int M, const int N, const int K, const double alpha, const double* A, const double* B, const double beta, double* C, int deviceId, int threadId)
{
   int lda = (TransA == CblasNoTrans) ? K : M;
   int ldb = (TransB == CblasNoTrans) ? N : K;
   cblas_dgemm(CblasRowMajor, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, N);
}

/******************************************************************************/
/*!
 * \ingroup util
 * \brief   Float general matrix matrix multiplication
 * C = alpha * op(A) * op(B) + beta * C
 * op(A) : M*K
 * op(B) : K*N
 */
/******************************************************************************/

void timlUtilBLASsgemm(const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_TRANSPOSE TransB, const int M, const int N, const int K, const float alpha, const float* A, const float* B, const float beta, float* C, int deviceId, int threadId)
{
#ifdef TIML_CPU
   int lda = (TransA == CblasNoTrans) ? K : M;
   int ldb = (TransB == CblasNoTrans) ? N : K;
   cblas_sgemm(CblasRowMajor, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, N);

#elif defined TIML_ALT
   return timlUtilBLASsgemmAlt(TransA,TransB, M, N, K, alpha, A, B, beta, C, deviceId, threadId);
#endif
}

/******************************************************************************/
/*!
 * \ingroup util
 * \brief   Double general matrix vector multiplication
 * y = alpha * op(A) * x + beta * y
 * op(A): M*N
 */
/******************************************************************************/

void timlUtilBLASdgemv(const enum CBLAS_TRANSPOSE TransA, const int M, const int N, const double alpha, const double* A, const double* x, const double beta, double* y, int deviceId, int threadId)
{
   cblas_dgemv(CblasRowMajor, TransA, M, N, alpha, A, N, x, 1, beta, y, 1);
}

/******************************************************************************/
/**
 * \ingroup util
 * \brief   Float general matrix vector multiplication
 * y = alpha * op(A) * x + beta * y
 * op(A): M*N
 */
/******************************************************************************/

void timlUtilBLASsgemv(const enum CBLAS_TRANSPOSE TransA, const int M, const int N, const float alpha, const float* A, const float* x, const float beta, float* y, int deviceId, int threadId)
{
#ifdef TIML_CPU
   cblas_sgemv(CblasRowMajor, TransA, M, N, alpha, A, N, x, 1, beta, y, 1);

#elif defined TIML_ALT
   return timlUtilBLASsgemvAlt(TransA, M, N, alpha, A, x, beta, y, deviceId, threadId);
#endif
}

/******************************************************************************/
/*!
 * \ingroup util
 * \brief   Float vector addition
 * Y = alpha * X + Y
 */
/******************************************************************************/

void timlUtilBLASsaxpy(const int N, const float alpha, const float* X, float* Y, int deviceId, int threadId)
{
#ifdef TIML_CPU
   cblas_saxpy(N, alpha, X, 1, Y, 1);
#elif defined TIML_ALT
   return timlUtilBLASsaxpyAlt(N, alpha, X, Y, deviceId, threadId);
#endif
}

/******************************************************************************/
/*!
 * \ingroup util
 * \brief   Double vector addition
 * Y = alpha * X + Y
 */
/******************************************************************************/

void timlUtilBLASdaxpy(const int N, const double alpha, const double* X, double* Y, int deviceId, int threadId)
{
   cblas_daxpy(N, alpha, X, 1, Y, 1);
}

/******************************************************************************/
/*!
 * \ingroup util
 * \brief   Float vector copy
 * Y = X
 */
/******************************************************************************/

void timlUtilBLASscopy(const int N, const float* X, float* Y, int deviceId, int threadId)
{
#ifdef TIML_CPU
   cblas_scopy(N, X, 1, Y, 1);

#elif defined TIML_ALT
   return timlUtilBLASscopyAlt(N, X, Y, deviceId, threadId);
#endif
}

/******************************************************************************/
/*!
 * \ingroup util
 * \brief   Double vector copy
 * Y = X
 */
/******************************************************************************/

void timlUtilBLASdcopy(const int N, const double* X, double* Y, int deviceId, int threadId)
{
   cblas_dcopy(N, X, 1, Y, 1);
}

/******************************************************************************/
/*!
 * \ingroup util
 * \brief   Double vector outer product
 * A = alpha*x*y' + A;
 * x: M
 * y: N
 */
/******************************************************************************/

void timlUtilBLASdger(const int M, const int N, const double alpha, double *x, double *y, double *A, int deviceId, int threadId)
{
   cblas_dger(CblasRowMajor, M, N, alpha, x, 1, y, 1, A, N);
}

/******************************************************************************/
/*!
 * \ingroup util
 * \brief   Float vector outer product
 * A = alpha*x*y' + A;
 * x: M
 * y: N
 */
/******************************************************************************/

void timlUtilBLASsger(const int M, const int N, const float alpha, float *x, float *y, float *A, int deviceId, int threadId)
{
#ifdef TIML_CPU
   cblas_sger(CblasRowMajor, M, N, alpha, x, 1, y, 1, A, N);
#elif defined TIML_ALT
   return timlUtilBLASsgerAlt(M, N, alpha, x, y, A, deviceId, threadId);
#endif
}

/******************************************************************************/
/*!
 * \ingroup util
 * \brief   Double vector scaling
 * x = alpha * x
 */
/******************************************************************************/

void timlUtilBLASdscal(const int N, const double alpha, double *X, int deviceId, int threadId)
{
   cblas_dscal(N, alpha, X, 1);
}

/******************************************************************************/
/*!
 * \ingroup util
 * \brief   Float vector scaling
 * x = alpha * x
 */
/******************************************************************************/

void timlUtilBLASsscal(const int N, const float alpha, float *X, int deviceId, int threadId)
{
#ifdef TIML_CPU
   cblas_sscal(N, alpha, X, 1);
#elif defined TIML_ALT
   return timlUtilBLASsscalAlt(N, alpha, X, deviceId, threadId);
#endif
}
