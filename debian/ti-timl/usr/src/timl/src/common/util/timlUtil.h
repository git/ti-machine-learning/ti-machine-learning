#ifndef TIMLUTIL_H_
#define TIMLUTIL_H_


/******************************************************************************/
/*!
 * \file timlUtil.h
 * \defgroup util util
 * \brief    utility module
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <dirent.h>
#include <omp.h>
#include <unistd.h>
#include <libgen.h>
#include "jpeglib.h"
#include "cblas.h"

/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

//! \ingroup util
//@{
#define TIML_UTIL_MAX_STR   100
#define TIML_UTIL_PI        3.14159265358979323846
#define ERROR_UTIL_OFFSET   3000


/*******************************************************************************
 *
 * ENUM DEFINITIONS
 *
 ******************************************************************************/

typedef enum {
   ERROR_UTIL_NULL_PTR = ERROR_UTIL_OFFSET,
   ERROR_UTIL_MNIST_TRAINING_DATA_READING,
   ERROR_UTIL_MNIST_TRAINING_DATA_ALLOCATION,
   ERROR_UTIL_MNIST_TRAINING_LABEL_READING,
   ERROR_UTIL_MNIST_TRAINING_LABEL_ALLOCATION,
   ERROR_UTIL_MNIST_TESTING_DATA_READING,
   ERROR_UTIL_MNIST_TESTING_DATA_ALLOCATION,
   ERROR_UTIL_MNIST_TESTING_LABEL_READING,
   ERROR_UTIL_MNIST_TESTING_LABEL_ALLOCATION,
   ERROR_UTIL_CIFAR10_TRAINING_READING,
   ERROR_UTIL_CIFAR10_TRAINING_ALLOCATION,
   ERROR_UTIL_CIFAR10_TESTING_READING,
   ERROR_UTIL_CIFAR10_TESTING_ALLOCATION,
   ERROR_UTIL_CIFAR100_TRAINING_READING,
   ERROR_UTIL_CIFAR100_TRAINING_ALLOCATION,
   ERROR_UTIL_CIFAR100_TESTING_READING,
   ERROR_UTIL_CIFAR100_TESTING_ALLOCATION,
   ERROR_UTIL_READ_FLOAT_MATRIX,
   ERROR_UTIL_READ_INT_MATRIX,
   ERROR_UTIL_READ_FLOAT_VECTOR,
   ERROR_UTIL_READ_INT_VECTOR,
   ERROR_UTIL_WRITE_FLOAT_MATRIX,
   ERROR_UTIL_WRITE_INT_MATRIX,
   ERROR_UTIL_WRITE_FLOAT_VECTOR,
   ERROR_UTIL_WRITE_INT_VECTOR,
   ERROR_UTIL_MALLOC,
   ERROR_UTIL_JPEG_READING
} timlUtilError;

typedef enum {
   Util_Sigmoid,
   Util_Softmax,
   Util_Softplus,
   Util_Relu,
   Util_Nrelu,
   Util_Tanh,
   Util_Linear
} timlUtilActivationType;

typedef enum {
   Util_CrossEntropy, Util_MSE
} timlUtilCostFunctionType;

typedef enum {
   Util_Conv2D, Util_Corr2D
} timlUtilConvType;

typedef enum {
   Util_ParamsLevel1, /**< structure text file only */
   Util_ParamsLevel2, /**< structure text + parameter binary */
   Util_ParamsLevel3  /**< structure text + parameter binary + state binary */
} timlUtilParamsLevel;

typedef enum {
   Util_AllocatorLevel1, /**< training mode */
   Util_AllocatorLevel2, /**< testing mode */
   Util_AllocatorLevel3  /**< testing mode with memory pool */
} timlUtilAllocatorLevel;

typedef enum {
   Util_CenterCrop, /**< crop the picture at the center */
   Util_RandomCrop  /**< randomly crop the picture */
} timlUtilCropType;

typedef enum {
   Util_Mirror, /**< mirror the picture */
   Util_NoMirror, /**< do not mirror the picture */
   Util_RandomMirror /**< randomly  mirror the picture according to Bernoulli(0,1)*/
} timlUtilMirrorType;

typedef enum {
   Util_Constant, Util_Gaussian, Util_Uniform, Util_Xavier
} timlUtilInitializerType;

typedef enum {
   Util_Train, Util_Test, Util_Debug
} timlUtilPhase;


/*******************************************************************************
 *
 * STRUCTURE DEFINITIONS
 *
 ******************************************************************************/

typedef struct {
   float *data;
   int row;
   int col;
   int channel;
} timlUtilImage;

typedef struct {
   timlUtilInitializerType type;
   float val;  /**< constant initializer */
   float min;  /**< uniform initializer */
   float max;  /**< uniform initializer */
   float mean; /**< Gaussian initializer */
   float std;  /**< Gaussian initializer */
} timlUtilInitializer;

typedef struct {
   float *data;
   int *label;
   int row;
   int col;
   int channel;
   int num;     /**< number of images */
   float *mean; /**< mean of all the images */
} timlUtilImageSet;


/*******************************************************************************
 *
 * DATABASE READ FUNCTIONS
 *
 ******************************************************************************/

int timlUtilReadMNIST(const char *path, timlUtilImageSet *training, timlUtilImageSet *testing);

int timlUtilReadCIFAR10(const char *path, timlUtilImageSet *training, timlUtilImageSet *testing);


/*******************************************************************************
 *
 * TIMING FUNCTIONS
 *
 ******************************************************************************/

long timlUtilDiffTime(struct timespec start, struct timespec end);


/*******************************************************************************
 *
 * RANDOM FUNCTIONS
 *
 ******************************************************************************/

int timlUtilRandDiscreteUniformRNG(int a, int b);

int timlUtilRandContinuousUniformRNG(float *x, int dim, float a, float b);

int timlUtilRandNormalRNG(float *x, int dim, float mean, float std);

int timlUtilRandPerm(int *array, int n);


/*******************************************************************************
 *
 * IO FUNCTIONS
 *
 ******************************************************************************/

int timlUtilFread(void* ptr, size_t size, size_t nmemb, FILE *fp);

int timlUtilFwrite(const void *ptr, size_t size, size_t nmemb, FILE *fp);


/*******************************************************************************
 *
 * CONV FUNCTIONS
 *
 ******************************************************************************/

int timlUtilConv2Valid(float *a, float *b, float *c, int aRow, int aCol, int bRow, int bCol);

int timlUtilConv2Full(float *a, float *b, float *c, int aRow, int aCol, int bRow, int bCol);

int timlUtilCorr2Full(float *a, float *b, float *c, int aRow, int aCol, int bRow, int bCol);

int timlUtilConv2ImageReshapeBack(float* x, float* xReshape, int *index, int prevChannel, int xDim, int indexDim, int deviceId, int threadId);

int timlUtilConv2ImageReshapeIndex(int *index, int aRow, int aCol, int bRow, int bCol, int padUp, int padDown, int padLeft, int padRight, int strideX, int strideY, timlUtilConvType type);

int timlUtilConv2ImageReshape(float* xReshape, float* x, int *index, int prevChannel, int xDim, int indexDim, int deviceId, int threadId);


/*******************************************************************************
 *
 * IMAGE READ FUNCTIONS
 *
 ******************************************************************************/

timlUtilImage timlUtilReadJPEG(const char *name);

int timlUtilReadFixedSizeJPEG(const char *name, float *data, int row, int col, int channel);

char** timlUtilScanJPEG(const char *dirName, int *imageNum);


/*******************************************************************************
 *
 * BLAS FUNCTIONS
 *
 ******************************************************************************/

void timlUtilBLASdgemm(const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_TRANSPOSE TransB, const int M, const int N, const int K, const double alpha, const double* A, const double* B, const double beta, double* C, int deviceId, int threadId);

void timlUtilBLASsgemm(const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_TRANSPOSE TransB, const int M, const int N, const int K, const float alpha, const float* A, const float* B, const float beta, float* C, int deviceId, int threadId);

void timlUtilBLASdgemv(const enum CBLAS_TRANSPOSE TransA, const int M, const int N, const double alpha, const double* A, const double* x, const double beta, double* y, int deviceId, int threadId);

void timlUtilBLASsgemv(const enum CBLAS_TRANSPOSE TransA, const int M, const int N, const float alpha, const float* A, const float* x, const float beta, float* y, int deviceId, int threadId);

void timlUtilBLASsaxpy(const int N, const float alpha, const float* X, float* Y, int deviceId, int threadId);

void timlUtilBLASdaxpy(const int N, const double alpha, const double* X, double* Y, int deviceId, int threadId);

void timlUtilBLASscopy(const int N, const float* X, float* Y, int deviceId, int threadId);

void timlUtilBLASdcopy(const int N, const double* X, double* Y, int deviceId, int threadId);

void timlUtilBLASsger(const int M, const int N, const float alpha, float *x, float *y, float *A, int deviceId, int threadId);

void timlUtilBLASdger(const int M, const int N, const double alpha, double *x, double *y, double *A, int deviceId, int threadId);

void timlUtilBLASdscal(const int N, const double alpha, double *X, int deviceId, int threadId);

void timlUtilBLASsscal(const int N, const float alpha, float *X, int deviceId, int threadId);


/*******************************************************************************
 *
 * BLAS AUX FUNCTIONS
 *
 ******************************************************************************/

int timlUtilVectorResetFloat(float *a, int m, float val, int deviceId, int threadId);

int timlUtilVectorResetInt(int *a, int m, int val, int deviceId, int threadId);

float timlUtilVectorSumFloat(float *a, int m);

int timlUtilVectorSortFloat(float *a, int n);

int timlUtilVectorSortIndexFloat(float *a, int *index, int n);

float timlUtilVectorMaxFloat(float *x, int n, int inc);

int timlUtilVectorMaxIndexFloat(float *x, int n, int inc);

int timlUtilElementWiseMultiply(float *a, const float *b, const float *c, int dim, int deviceId, int threadId);

int timlUtilSubtract(float *x, float y, int deviceId, int threadId);


/*******************************************************************************
 *
 * CNN AUX FUNCTIONS
 *
 ******************************************************************************/

int timlUtilSigmoid(float *x, float *y, int n, int deviceId, int threadId);

int timlUtilSigmoidDerivative(float *x, float *y, int n, int deviceId, int threadId);

int timlUtilRelu(float *x, float *y, int n, int deviceId, int threadId);

int timlUtilReluDerivative(float *x, float *y, int n, int deviceId, int threadId);

//int timlUtilSoftmaxDerivative(float *x, float *y, int n);

int timlUtilTanhDerivative(float *x, float *y, int n, int deviceId, int threadId);

float timlUtilMultinomialCrossEntropy(float *x, int label, int n);

float timlUtilMeanSqaureError(float *x, int label, int n);

int timlUtilSoftmax(float *x, float *y, int row, int col, int channel, int deviceId, int threadId);

int timlUtilClassifyAccuracy(int *label, int topN, int num, int *trueLabel);

void timlUtilTransform(float *dataOut, float *dataIn, float *dataHost, int channel, int row, int col, int rowOffset, int colOffset, int rowIn, int colIn, float scale, float *mean, timlUtilMirrorType mirrorType, int deviceId, int threadId);

int timlUtilMaxPooling(float *outputMap, int *maxIndex, float *inputMap, int row, int col, int channel, int prevRow, int prevCol, int scaleRow, int scaleCol, int padUp, int padLeft, int strideX, int strideY, timlUtilPhase phase, int deviceId, int threadId);

int timlUtilUndoMaxPooling(float *prevDelta, int * maxIndex, float *delta, int dim, int deviceId, int threadId);

int timlUtilMeanPooling(float *outputMap, float *inputMap, int row, int col, int channel, int prevRow, int prevCol, int scaleRow, int scaleCol, int padUp, int padLeft, int strideX, int strideY, int deviceId, int threadId);

int timlUtilUndoMeanPooling(float *prevDelta, float *delta, int row, int col, int channel, int prevRow, int prevCol, int scaleRow, int scaleCol, int padUp, int padLeft, int strideX, int strideY, int deviceId, int threadId);

int timlUtilLocalContrastNormalize(float *inputMap, float *outputMap, float *denom, int row, int col, int channel, int N, float alpha, float beta, int deviceId, int threadId);

int timlUtilLocalContrastUnnormalize(float *prevDelta, float *prevFeatureMap, float *delta, float *featureMap, float *denom, int row, int col, int channel, int N, float alpha, float beta, int deviceId, int threadId);

int timlUtilMasking(float *inputMap, float *outputMap, int *mask, unsigned int *randomVector, int dim, float prob, int deviceId, int threadId);

int timlUtilUnmasking(float *inputDelta, float *outputDelta, int *mask, int dim, float prob, int deviceId, int threadId);

int timlUtilTanh(float *x, float *y, int n, int deviceId, int threadId);


/*******************************************************************************
 *
 * MISC FUNCTIONS
 *
 ******************************************************************************/

int timlUtilMalloc(void** devPtr, size_t size);

void timlUtilFree(void* ptr);

uint32_t timlUtilReverseEndian32(register uint32_t i);

int timlUtilElementWiseFunction(float *x, float *y, int n, float (*fun)(float));

//@}
#endif /* TIMLUTIL_H_ */
