/******************************************************************************/
/*!
 * \file timlUtilConv2ImageReshapeBack.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"
#ifdef TIML_ALT
#include "../../alt/timlAlt.h"
#endif


/******************************************************************************/
/*!
 * \ingroup    util
 * \brief      Reshape the convolution matrix back to feature maps
 * \param[in]  x        feature map
 * \param[out] xReshape Reshaped feature map
 * \param[in]  index    Reshaping index matrix
 * \param[in]  channel  The number of channels in the feature map
 * \param[in]  xDim     Dimension of the feature map (row*col)
 * \param[in]  indexDim Dimension of the index matrix
 * \param[in]  deviceId Device id
 * \param[in]  threadId Thread id
 * \return     Error code
 */
/******************************************************************************/

int timlUtilConv2ImageReshapeBack(float* x, float* xReshape, int *index, int channel, int xDim, int indexDim, int deviceId, int threadId)
{
#ifdef TIML_CPU
   int p;
   int j;
   for (j = 0; j < channel; j++) {
      for (p = 0; p < indexDim; p++) {
         if (index[p] != -1) {
            x[j * xDim + index[p]] += xReshape[j * indexDim + p];
         }
      }
   }

   return 0;
#elif defined TIML_ALT
   return timlUtilConv2ImageReshapeBackAlt(x, xReshape, index, channel, xDim, indexDim, deviceId, threadId);
#endif
}
