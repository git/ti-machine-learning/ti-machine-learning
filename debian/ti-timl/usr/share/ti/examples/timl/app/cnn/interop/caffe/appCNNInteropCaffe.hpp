#ifndef APPCNNINTEROPCAFFE_HPP_
#define APPCNNINTEROPCAFFE_HPP_


/******************************************************************************/
/*!
 * \file     appCNNInteropCaffe.hpp
 * \defgroup appCNNInteropCaffe appCNNInteropCaffe
 * \ingroup  appCNN
 * \brief    CNN Caffe interoperation applications
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 *  INCLUDES
 *
 ******************************************************************************/

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/message.h>
#include <google/protobuf/text_format.h>

#include <fcntl.h>
#include "appCNNInteropCaffeProtobuf.hpp"

extern "C" {
#include "timl.h"
}

using namespace std;
using namespace caffe;
using ::google::protobuf::Message;
using ::google::protobuf::io::FileInputStream;
using ::google::protobuf::io::FileOutputStream;
using ::google::protobuf::io::ZeroCopyInputStream;
using ::google::protobuf::io::CodedInputStream;
using ::google::protobuf::io::ZeroCopyOutputStream;
using ::google::protobuf::io::CodedOutputStream;


/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

typedef enum {
   ERROR_APP_CNN_INTEROP_ARG,
   ERROR_APP_CNN_INTEROP_READ_FIEL
} appCNNInteropError;


/*******************************************************************************
 *
 * FUNCTION PROTOTYPES
 *
 ******************************************************************************/

//! \ingroup appCNNInteropCaffe
//@{

bool appCNNInteropCaffeReadProtoFromTextFile(const char* filename, Message* proto);

//void appCNNInteropCaffeWriteProtoToTextFile(const Message& proto, const char* filename);

bool appCNNInteropCaffeReadProtoFromBinaryFile(const char* filename, Message* proto);

//void appCNNInteropCaffeWriteProtoToBinaryFile(const Message& proto, const char* filename);

int appCNNInteropCaffeFlipMatrixFloat(float *a, int m, int n);

int appCNNInteropCaffeFlipKernelMatrix(float *kernel, int kernelRow, int kernelCol, int inputChannel, int outputChannel);

int appCNNInteropCaffeFillBlockDiagonalMatrix(float *a, int M, int N, int group, float *b);

timlUtilActivationType appCNNInteropCaffeNonlinearTypeConvert(LayerParameter_LayerType type);

timlCNNLayerType appCNNInteropCaffeLayerTypeConvert(LayerParameter_LayerType type);

timlCNNPoolingType appCNNInteropCaffePoolingTypeConvert(PoolingParameter_PoolMethod method);

timlConvNeuralNetwork* appCNNInteropCaffeConvert(const char *netStructurePrototxtFileName, const char *netParamPrototxtFileName);

int appCNNInteropCaffeConvLayerConvert(timlConvNeuralNetwork *cnn, LayerParameter layerStructure, LayerParameter layerParam);

int appCNNInteropCaffeConvLayerPermuteKernel(timlCNNLayer *layer);

int appCNNInteropCaffePoolingLayerConvert(timlConvNeuralNetwork *cnn, LayerParameter layerStructure, LayerParameter layerParam);

int appCNNInteropCaffeNormLayerConvert(timlConvNeuralNetwork *cnn, LayerParameter layerStructure, LayerParameter layerParam);

int appCNNInteropCaffeLinearLayerConvert(timlConvNeuralNetwork *cnn, LayerParameter layerStructure, LayerParameter layerParam);

int appCNNInteropCaffeNonlinearLayerConvert(timlConvNeuralNetwork *cnn, LayerParameter layerStructure, LayerParameter layerParam);

int appCNNInteropCaffeDropoutLayerConvert(timlConvNeuralNetwork *cnn, LayerParameter layerStructure, LayerParameter layerParam);

int appCNNInteropCaffeReadMean(timlCNNLayer *layer, const char *fileName);

int appCNNInteropCaffePermuteMean(float *mean, int row, int col, int channel);

//@}

#endif /* APPCNNINTEROPCAFFE_HPP_ */
