/******************************************************************************/
/*!
 * \file appCNNInteropCaffeLayerTypeConvert.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRIC * \param[in]     m Rows
 * \param[in]     n ColsT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 *  INCLUDES
 *
 ******************************************************************************/

#include "appCNNInteropCaffe.hpp"


/******************************************************************************/
/*!
 * \ingroup   appCNNInteropCaffe
 * \brief     Caffe to TIML CNN layer type conversion
 * \param[in] type Caffe layer type
 * \return    TIML CNN layer type
 */
/******************************************************************************/

timlCNNLayerType appCNNInteropCaffeLayerTypeConvert(LayerParameter_LayerType type)
{
   switch (type) {
   case LayerParameter_LayerType_CONVOLUTION:
      return CNN_Conv;
      break;
   case LayerParameter_LayerType_POOLING:
      return CNN_Pooling;
      break;
   case LayerParameter_LayerType_LRN:
      return CNN_Norm;
      break;
   case LayerParameter_LayerType_INNER_PRODUCT:
      return CNN_Linear;
      break;
   case LayerParameter_LayerType_DROPOUT:
      return CNN_Dropout;
      break;
   case LayerParameter_LayerType_RELU:
   case LayerParameter_LayerType_SIGMOID:
   case LayerParameter_LayerType_SOFTMAX:
   case LayerParameter_LayerType_TANH:
      return CNN_Nonlinear;
      break;
   default:
      return CNN_Input;
   }
}
