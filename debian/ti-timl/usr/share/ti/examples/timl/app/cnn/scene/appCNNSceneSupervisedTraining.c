/******************************************************************************/
/*!
 * \file appCNNSceneSupervisedTraining.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "appCNNScene.h"


/******************************************************************************/
/*!
 * \ingroup       appCNNScene
 * \brief         Supervised training on the dataset
 * \param[in,out] cnn     CNN
 * \param[in]     dataSet Data set
 * \return        Error code
 */
/******************************************************************************/

int appCNNSceneSupervisedTraining(timlConvNeuralNetwork *cnn, appCNNSceneDataSet *dataSet)
{

   int          i;
   int          j;
   int          err;
   timlCNNLayer *bpStartLayer;
   int          label;
   int          iter;
   int          patchDim;
   int          *imageIdx;
   int          *rowIdx;
   int          *colIdx;
   float        *patch;
   int          epoch;
   int          batchSize;
   int          batchNum;
   float        *cost;
   float        *batchCost;
   int          batchIndex;

   // init
   err        = 0;
   iter       = dataSet->row*dataSet->col*dataSet->num;
   patchDim   = dataSet->channel*dataSet->patchSize*dataSet->patchSize;
   imageIdx   = malloc(sizeof(int)*iter);
   rowIdx     = malloc(sizeof(int)*iter);
   colIdx     = malloc(sizeof(int)*iter);
   patch      = malloc(sizeof(float)*patchDim);
   epoch      = cnn->params.epoch;
   batchSize  = cnn->params.batchSize;
   batchNum   = iter/batchSize;
   cost       = malloc(sizeof(float)*batchSize);
   batchCost  = malloc(sizeof(float)*batchNum*epoch);
   batchIndex = 0;

   // shuffle the training pixels
   appCNNSceneShuffleIdx(imageIdx, rowIdx, colIdx, dataSet);

   // training loop
   cnn->params.count = 0;
   for (i = 0; i < epoch; i++) {
      cnn->params.count = 0;
      for (j = 0; j < iter; j++) {
         label = appCNNSceneGetLabel(imageIdx[j], rowIdx[j], colIdx[j], dataSet);
         if (label != -1) {
            cnn->params.count += 1;
            appCNNSceneGetPatch(imageIdx[j], rowIdx[j], colIdx[j], dataSet, patch);
            err = timlCNNForwardPropagation(cnn, patch, patchDim);
            timlCNNCostWithLabel(cnn, label, cost + j%batchSize, &bpStartLayer);
            err = timlCNNBackPropagation(cnn, bpStartLayer);
         }
         else {
            cost[j%batchSize] = 0.0;
         }
         if ((j + 1)%batchSize == 0) { // update parameters once each batch
            batchCost[batchIndex + i*batchNum] = timlUtilVectorSumFloat(cost, batchSize)/(double)cnn->params.count;
            timlCNNUpdateParams(cnn);
            printf("epoch = %d, batch = %d, cost = %f\n", i, batchIndex, batchCost[batchIndex + i*batchNum]);
            batchIndex += 1;
         }
      }
   }

   free(imageIdx);
   free(rowIdx);
   free(colIdx);
   free(cost);
   free(batchCost);

   return err;

}

