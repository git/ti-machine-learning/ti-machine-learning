/******************************************************************************/
/*!
 * \file appCNNSceneSBDTesting.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../appCNNScene.h"


/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define SCALE         4
#define IMAGE_NUM     10
#define IMAGE_ROW     240
#define IMAGE_COL     320
#define IMAGE_CHANNEL 3
#define PATCH_SIZE    133
#define MODEL_PATH    "../../../../database/model/sbd/databaseModelSBD.m"
#define IMAGE_PATH    "../../../../database/sbd/test/%03d.jpg"
#define LABEL_PATH    "../../../../database/sbd/test/%03d.txt"


/*******************************************************************************
 *
 * main()
 *
 ******************************************************************************/

int main()
{
   return appCNNSceneSBDTesting();
}


/******************************************************************************/
/*!
 * \ingroup appCNNScene
 * \brief   Standford Backgournd Database Scene labeling testing example
 */
/******************************************************************************/

int appCNNSceneSBDTesting()
{
   int                   i;
   int                   n;
   int                   m;
   int                   read;
   struct timespec       startTime;
   struct timespec       endTime;
   long                  testingTime;
   int                   err;
   long                  mem;
   int                   scale;
   int                   *labelMatrix;
   float                 *image;
   int                   *trueLabelMatrix;
   float                 labelAccuracy;
   int                   thread;
   timlConvNeuralNetwork **cnnTeam;
   char                  str[TIML_UTIL_MAX_STR];
   FILE                  *fp;
   appCNNSceneDataSet    slTesting;

   // init
   err    = 0;
   scale  = SCALE;
   thread = omp_get_max_threads();
   slTesting.num              = IMAGE_NUM;
   slTesting.row              = IMAGE_ROW;
   slTesting.col              = IMAGE_COL;
   slTesting.channel          = IMAGE_CHANNEL;
   slTesting.patchSize        = PATCH_SIZE;
   slTesting.imageFileNameStr = IMAGE_PATH;
   slTesting.labelFileNameStr = LABEL_PATH;
   labelMatrix     = malloc(sizeof(int)*slTesting.row*slTesting.col);
   trueLabelMatrix = malloc(sizeof(int)*slTesting.row*slTesting.col);
   image           = malloc(sizeof(float)*slTesting.row*slTesting.col*slTesting.channel);
   cnnTeam         = malloc(sizeof(timlConvNeuralNetwork*)*thread);
   setbuf(stdout, NULL); // do not buffer the console output

   // read CNN config
   printf("1. Read cnn config\n");
   timlConvNeuralNetwork *cnn = timlCNNReadFromFile(MODEL_PATH, 0);
   mem = timlCNNMemory(cnn);
   printf("CNN memory = %.10f MB.\n", (float)mem/1024.0/1024.0);

   // create cnnTeam
   cnnTeam[0] = cnn;
   for (i = 1; i < thread; i++) {
      cnnTeam[i] = timlCNNShareParams(cnn, 0);
   }

   // testing
   printf("2. Start testing\n");
   for (i = 0; i < slTesting.num; i++) {
      printf("Read image %03d.jpg\n", i);
      sprintf(str, slTesting.imageFileNameStr, i);
      timlUtilReadFixedSizeJPEG(str, image, slTesting.row, slTesting.col, slTesting.channel);
      clock_gettime(CLOCK_REALTIME, &startTime);
      appCNNSceneClassifyOpenMP(cnnTeam, thread, image, slTesting.row, slTesting.col, slTesting.channel, labelMatrix, scale);
      clock_gettime(CLOCK_REALTIME, &endTime);
      testingTime = timlUtilDiffTime(startTime, endTime);

      // read true label
      sprintf(str, slTesting.labelFileNameStr, i);
      fp = fopen(str, "rt");
      for (n = 0; n < slTesting.row; n++) {
         for (m = 0; m < slTesting.col; m++) {
            read = fscanf(fp, "%d", trueLabelMatrix + n*slTesting.col + m);
         }
         read = fscanf(fp, "\n");
      }
      fclose(fp);

      // calculate accuracy
      labelAccuracy = appCNNSceneAccuracy(labelMatrix, trueLabelMatrix, slTesting.row*slTesting.col);
      printf("Test image %03d label accuracy = %.2f %%\n", i, 100.0*labelAccuracy);
      printf("Test image %03d time           = %.3f s\n", i, testingTime/1000000.0);
   }

   // clean up
   printf("4. Clean up");
   for (i = 0; i < thread; i++) {
      timlCNNDelete(cnnTeam[i]);
   }
   free(cnnTeam);
   free(labelMatrix);
   free(trueLabelMatrix);
   free(image);

   return err;

}

