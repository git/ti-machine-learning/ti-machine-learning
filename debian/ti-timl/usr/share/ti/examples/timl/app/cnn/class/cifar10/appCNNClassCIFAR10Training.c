/******************************************************************************/
/*!
 * \file appCNNClassCIFAR10Training.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../appCNNClass.h"


/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define DATABASE_PATH    "../../../../database/cifar10"
#define IMAGE_ROW        32
#define IMAGE_COL        32
#define IMAGE_CHANNEL    3
#define BATCH_SIZE       100

/*******************************************************************************
 *
 * main()
 *
 ******************************************************************************/

int main()
{
   return appCNNClassCIFAR10Training();
}


/******************************************************************************/
/*!
 * \ingroup appCNNClass
 * \brief   CIFAR10 training example
 */
/******************************************************************************/

int appCNNClassCIFAR10Training()
{
   int              i;
   int              dim;
   int              batchSize;
   int              batchNum;
   long             mem;
   struct timespec  startTime;
   struct timespec  endTime;
   long             trainingTime;
   int              err;
   timlUtilImageSet training;
   timlUtilImageSet testing;

   // init
   err       = 0;
   dim       = IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL;
   batchSize = BATCH_SIZE;

   setbuf(stdout, NULL); // do not buffer the console output

   // build up the CNN
   printf("1. Build up CNN\n");
   timlConvNeuralNetwork *cnn = timlCNNCreateConvNeuralNetwork(timlCNNTrainingParamsDefault(), 0);
   cnn->params.learningRate = 0.01;
   timlCNNAddInputLayer(cnn, IMAGE_ROW, IMAGE_COL, IMAGE_CHANNEL, timlCNNInputParamsDefault());  // input layer
   timlCNNAddConvLayer(cnn, 5, 5, 1, 1, 32, timlCNNConvParamsDefault());                         // conv layer
   timlCNNAddPoolingLayer(cnn, 3, 3, 2, 2, CNN_MaxPooling, timlCNNPoolingParamsDefault());       // max pooling layer
   timlCNNAddNonlinearLayer(cnn, Util_Relu);                                                     // relu layer
   timlCNNAddConvLayer(cnn, 5, 5, 1, 1, 32, timlCNNConvParamsDefault());                         // conv layer
   timlCNNAddNonlinearLayer(cnn, Util_Relu);                                                     // relu layer
   timlCNNAddPoolingLayer(cnn, 3, 3, 2, 2, CNN_MeanPooling, timlCNNPoolingParamsDefault());      // max pooling layer
   timlCNNAddConvLayer(cnn, 5, 5, 1, 1, 64, timlCNNConvParamsDefault());                         // conv layer
   timlCNNAddNonlinearLayer(cnn, Util_Relu);                                                     // relu layer
   timlCNNAddPoolingLayer(cnn, 3, 3, 2, 2, CNN_MeanPooling, timlCNNPoolingParamsDefault());      // max pooling layer
   timlCNNAddLinearLayer(cnn, 64, timlCNNLinearParamsDefault());                                 // linear layer
   timlCNNAddLinearLayer(cnn, 10, timlCNNLinearParamsDefault());                                 // linear layer
   timlCNNAddNonlinearLayer(cnn, Util_Softmax);                                                  // softmax layer
   timlCNNInitialize(cnn);
   timlCNNReset(cnn);
   mem = timlCNNMemory(cnn);
   timlCNNPrint(cnn);
   printf("CNN memory allocation = %.10f MB.\n", (float)mem/1024.0/1024.0);
   printf("CNN parameter #       = %lu.\n", timlCNNGetParamsNum(cnn));

   // read the CIFAR10 database
   printf("2. Read CIFAR10 database\n");
   timlUtilReadCIFAR10(DATABASE_PATH, &training, &testing);

   // training
   printf("3. Start training\n");
   batchNum = training.num/batchSize;
   clock_gettime(CLOCK_REALTIME, &startTime);
   for (i = 0; i < batchNum; i++) {
      timlCNNSupervisedTrainingWithLabelBatchMode(cnn, training.data + i * batchSize * dim, training.label + i * batchSize, dim, batchSize);
   }
   clock_gettime(CLOCK_REALTIME, &endTime);
   trainingTime = timlUtilDiffTime(startTime, endTime);
   printf("Training time = %.2f sec.\n", trainingTime/1000000.0);

   // cleaning
   printf("4. Clean up\n");
   free(training.data);
   free(training.label);
   free(testing.data);
   free(testing.label);
   timlCNNDelete(cnn);

   return err;
}
