/******************************************************************************/
/*!
 * \file appCNNSceneGetLabel.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "appCNNScene.h"


/******************************************************************************/
/*!
 * \ingroup appCNNScene
 * \brief     Return the pixel label for (image, row, col) index combination
 * \param[in] imageIdx Image index
 * \param[in] rowIdx   Row index
 * \param[in] colIdx   Col index
 * \param[in] dataSet  Data set
 * \return    Pixel label
 */
/******************************************************************************/

int appCNNSceneGetLabel(int imageIdx, int rowIdx, int colIdx, appCNNSceneDataSet *dataSet)
{
   FILE *fp;
   int  n;
   int  m;
   int  row;
   int  col;
   int  read;
   int  *label;
   int  pixelLabel;
   char str[TIML_UTIL_MAX_STR];

   row   = dataSet->row;
   col   = dataSet->col;
   label = malloc(sizeof(int) * row * col);

   sprintf(str, dataSet->labelFileNameStr, imageIdx);
   fp = fopen(str, "rt");
   for (n = 0; n < row; n++) {
      for (m = 0; m < col; m++) {
         read = fscanf(fp, "%d", label + n*col + m);
      }
      read = fscanf(fp, "\n");
   }
   fclose(fp);

   pixelLabel = label[rowIdx*dataSet->col + colIdx];
   free(label);

   return pixelLabel;
}

