/******************************************************************************/
/*!
 * \file benchmarkCNNClassVGGNetTesting.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../benchmarkCNNClass.h"

/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define MODEL_PATH       "../../../../database/model/vggnet/databaseModelVGGNet.m"
#define LABEL_PATH       "../../../../database/imagenet/test/label.txt"
#define IMAGE_PATH       "../../../../database/imagenet/test/%010d.jpg"
#define TOP_N            5
#define IMAGE_NUM        100
#define IMAGE_ROW        256
#define IMAGE_COL        256
#define IMAGE_CHANNEL    3
#define ITER             10


/*******************************************************************************
 *
 * main()
 *
 ******************************************************************************/

//int main()
//{
//
//   return benchmarkCNNClassVGGNetTesting();
//}


/******************************************************************************/
/*!
 * \ingroup benchmarkCNNClass
 * \brief   CNN VGGNet classification benchmark
 */
/******************************************************************************/

int benchmarkCNNClassVGGNetTesting()
{
   int           i;
   int           j;
   int           read;
   int           err;
   FILE          *fp;
   int           dim;
   timlUtilImage image;
   int           *testLabel;
   float         *testImage;
   char          str[TIML_UTIL_MAX_STR];
   int           iter;
   int           testNum;

   // init
   err       = 0;
   dim       = IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL;
   iter      = ITER;
   testNum   = IMAGE_NUM;
   testLabel = malloc(sizeof(int)*testNum);
   testImage = malloc(sizeof(float)*dim*testNum);
   setbuf(stdout, NULL); // do not buffer the console output

   // read model
   timlConvNeuralNetwork *cnn = timlCNNReadFromFile(MODEL_PATH);
   timlCNNReset(cnn);
   timlCNNSetMode(cnn, Util_Test);

   // read labels
   fp = fopen(LABEL_PATH, "rt");
   for (i = 0; i < testNum; i++) {
      read = fscanf(fp, "%d", testLabel + i);
   }
   fclose(fp);

   // read images
   for (i = 0; i < testNum; i++) {
      sprintf(str, IMAGE_PATH, i);
      image = timlUtilReadJPEG(str);
      if (image.channel == 1) { // duplicate channels
         for (j = 0; j < IMAGE_CHANNEL; j++) {
            cblas_scopy(IMAGE_ROW*IMAGE_COL, image.data, 1, testImage + i*IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL + j*IMAGE_ROW*IMAGE_COL, 1);
         }
      }
      else {
         cblas_scopy(IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL, image.data, 1, testImage + i * IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL, 1);
      }
      free(image.data);
   }

   // profiling
   timlCNNProfile(cnn, testImage, IMAGE_ROW, IMAGE_COL, IMAGE_CHANNEL, testLabel, 1, 1, testNum, iter);

   // clean up
   timlCNNDelete(cnn);
   free(testLabel);
   free(testImage);

   return err;
}

