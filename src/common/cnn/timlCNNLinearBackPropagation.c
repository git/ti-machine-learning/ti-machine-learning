/******************************************************************************/
/*!
 * \file timlCNNLinearBackPropagation.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Back propagate the gradient from the linear layer to the previous layer
 * \param[in]     layer Layer ptr
 * \return        Error code
 */
/******************************************************************************/

int timlCNNLinearBackPropagation(timlCNNLayer *layer)
{
   int prevDim;
   int dim;
   int deviceId;
   int threadId;
   int M;
   int K;
   int N;

   prevDim  = layer->prev->row*layer->prev->col*layer->prev->channel;
   dim      = layer->channel;
   deviceId = layer->cnn->deviceId;
   threadId = layer->cnn->threadId;
   M        = layer->batchSize;
   K        = layer->linearParams.dim;
   N        = layer->linearParams.prevDim;

//   // calculate layer->prev->delta
//   timlUtilBLASsgemv(CblasTrans, dim, prevDim, 1.0, layer->linearParams.weight, layer->delta, 0.0, layer->prev->delta, deviceId, threadId);
//   #pragma omp critical
//   {
//      // calculate layer->weightGradAccum
//      timlUtilBLASsger(dim, prevDim, 1.0, layer->delta, layer->prev->featureMap, layer->linearParams.weightGradAccum, deviceId, threadId);
//      // calculate layer->biasGradAccum
//      timlUtilBLASsaxpy(dim, 1.0, layer->delta, layer->linearParams.biasGradAccum, deviceId, threadId);
//   }

   // calculate layer->prev->delta
   timlUtilBLASsgemm(CblasNoTrans, CblasNoTrans, M, N, K, 1.0, layer->delta, layer->linearParams.weight, 0.0, layer->prev->delta, deviceId, threadId);
   #pragma omp critical
   {
      // calculate layer->weightGradAccum
      timlUtilBLASsgemm(CblasTrans, CblasNoTrans, K, N, M, 1.0, layer->delta, layer->prev->featureMap, 1.0, layer->linearParams.weightGradAccum, deviceId, threadId);
      // calculate layer->biasGradAccum
      timlUtilBLASsgemm(CblasNoTrans, CblasNoTrans, 1, K, M, 1.0, layer->linearParams.biasMultiplier, layer->delta, 1.0, layer->linearParams.biasGradAccum, deviceId, threadId);
   }
   return 0;
}
