/*****************************************************************************/
/*!
 * \file timlCNNClassifyAccuracyTeamModeOpenMP.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Batch classification
 * \param[in,out] cnnTeam   CNN team
 * \param[in]     teamNum   CNN team number
 * \param[in]     data      Data batch
 * \param[in]     dataDim   Data dimension
 * \param[in]     num       Data number
 * \param[out]    label     Label array ptr
 * \param[out]    labelDim  Label dimension
 * \param[out]    success   Number of successful classification
 * \return        Error code
 */
/******************************************************************************/

int timlCNNClassifyAccuracyTeamModeOpenMP(timlConvNeuralNetwork **cnnTeam, int teamNum, float *image, int row, int col, int channel, int *label, int labelRow, int labelCol, int num, int *success)
{
   int i;
   int batchSize;
   int batchNum;
   int err;
   int successLocal;
   int thread;
   int t;
   int dataDim;
   int labelDim;
   timlConvNeuralNetwork *cnn;

   cnn = cnnTeam[0];
   err = 0;
   successLocal = 0;
   batchSize = cnn->params.batchSize;
   batchNum = num/batchSize;
   dataDim = row*col*channel;
   labelDim = labelRow*labelCol;


   if (cnn->tail->type != CNN_Accuracy) {
      return ERROR_CNN_CLASS;
   }

   thread = omp_get_max_threads();
   if (thread > teamNum) { // more thread than cnn copies
      thread = teamNum;
   }

   #pragma omp parallel num_threads(thread) private(t, i, err)
   {
      #pragma omp for reduction(+:successLocal)
      for (i = 0; i < batchNum; i++) {
         t = omp_get_thread_num(); // get thread id
         err = timlCNNLoadImage(cnnTeam[t], image + i*dataDim*batchSize, row, col, channel, batchSize);
         err = timlCNNLoadLabel(cnnTeam[t], label + i*labelDim*batchSize, labelRow, labelCol, batchSize);
         err = timlCNNForwardPropagation(cnnTeam[t]);
         successLocal += cnnTeam[t]->tail->accuracyParams.success;
      }
   }

   *success = successLocal;
   return err;
}
