/******************************************************************************/
/*!
 * \file timlCNNDeleteLayer.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup   cnn
 * \brief     Delete a layer
 * \param[in] layer Layer ptr
 * \return    Error code
 */
/******************************************************************************/

int timlCNNDeleteLayer(timlCNNLayer *layer)
{
   int err;

   if (layer->prev != NULL && layer->next != NULL) {
      layer->prev->next = layer->next;
      layer->next->prev = layer->prev;
   }

   // layer at the head
   if (layer->prev == NULL && layer->next != NULL) {
      layer->next->prev = NULL;
      layer->cnn->head = layer->next;
   }

   // layer at the tail
   if (layer->next == NULL && layer->prev != NULL) {
      layer->prev->next = NULL;
      layer->cnn->tail = layer->prev;
   }

   // delete the layer
   switch(layer->type) {
   case CNN_Input:
      timlCNNFreeInputLayer(layer);
      break;
   case CNN_Conv:
      timlCNNFreeConvLayer(layer);
      break;
   case CNN_Norm:
      timlCNNFreeNormLayer(layer);
      break;
   case CNN_Pooling:
      timlCNNFreePoolingLayer(layer);
      break;
   case CNN_Linear:
      timlCNNFreeLinearLayer(layer);
      break;
   case CNN_Nonlinear:
      timlCNNFreeNonlinearLayer(layer);
      break;
   case CNN_Dropout:
      timlCNNFreeDropoutLayer(layer);
      break;
   case CNN_Softmax:
      timlCNNFreeSoftmaxLayer(layer);
      break;
   case CNN_SoftmaxCost:
      timlCNNFreeSoftmaxCostLayer(layer);
      break;
   case CNN_Accuracy:
      timlCNNFreeAccuracyLayer(layer);
      break;
   default:
      break;
   }
   timlUtilFreeHost(layer);

   return err;
}
