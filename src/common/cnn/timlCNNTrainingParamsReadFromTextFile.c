/******************************************************************************/
/*!
 * \file timlCNNTrainingParamsReadFromTextFile.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Read the training params from a text file
 * \param[in]     fp  FILE ptr to the level 1 text file
 * \param[in,out] cnn CNN
 * \return        Error code
 */
/******************************************************************************/

int timlCNNTrainingParamsReadFromTextFile(FILE *fp, timlConvNeuralNetwork *cnn)
{
   int  read;
   char str[TIML_UTIL_MAX_STR];

   read = fscanf(fp, "%[^.].params.batchUpdate = %d;\n", (char*)&str, &cnn->params.batchUpdate);
   if (read != 2) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp, "%[^.].params.batchSize = %d;\n", (char*)&str, &cnn->params.batchSize);
   if (read != 2) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp, "%[^.].params.maxBatchSize = %d;\n", (char*)&str, &cnn->params.maxBatchSize);
   if (read != 2) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp, "%[^.].params.epoch = %d;\n", (char*)&str, &cnn->params.epoch);
   if (read != 2) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp, "%[^.].params.learningRate = %f;\n", (char*)&str, &cnn->params.learningRate);
   if (read != 2) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp, "%[^.].params.momentum = %f;\n", (char*)&str, &cnn->params.momentum);
   if (read != 2) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp, "%[^.].params.phase = %d;\n", (char*)&str, (int*)&cnn->params.phase);
   if (read != 2) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp, "%[^.].params.allocatorLevel = %d;\n", (char*)&str, (int*)&cnn->params.allocatorLevel);
   if (read != 2) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }
   read = fscanf(fp, "%[^.].params.costType = %d;\n", (char*)&str, (int*)&cnn->params.costType);
   if (read != 2) {
      timlCNNDelete(cnn);
      return ERROR_CNN_READ_FILE;
   }

   return 0;
}
