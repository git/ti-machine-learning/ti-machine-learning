/******************************************************************************/
/*!
 * \file timlCNNConvBackPropagation.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Back propagate the gradient from the conv layer to the previous layer
 * \details       layer->prev->delta[i] = sum_{j}(layer->delta[j] conv2full layer->kernel[i, j])
 * \param[in]     layer Layer ptr
 * \return        Error code
 */
/******************************************************************************/

int timlCNNConvBackPropagation(timlCNNLayer *layer)
{
   int M;
   int K;
   int N;
   int b;
   int err;
   int prevRow;
   int prevCol;
   int prevChannel;
   int row;
   int col;
   int channel;
   int kernelRow;
   int kernelCol;
   int deviceId;
   int threadId;

   // init
   prevRow     = layer->prev->row;
   prevCol     = layer->prev->col;
   prevChannel = layer->prev->channel;
   row         = layer->row;
   col         = layer->col;
   channel     = layer->channel;
   kernelRow   = layer->convParams.kernelRow;
   kernelCol   = layer->convParams.kernelCol;
   deviceId    = layer->cnn->deviceId;
   threadId    = layer->cnn->threadId;
   M           = channel;
   K           = kernelRow*kernelCol*prevChannel;
   N           = row*col;
   err         = 0;

   // kernelGrad = delta * prevFeatureMapReshape' -- (M*N)*(N*K)

   for (b = 0; b < layer->batchSize; b++) {
      timlUtilConv2ImageReshape(layer->convParams.prevFeatureMapReshape, layer->prev->featureMap + b*prevRow*prevCol*prevChannel, layer->convParams.prevFeatureMapReshapeIndex, prevChannel, prevRow*prevCol, kernelRow*kernelCol*row*col, deviceId, threadId);
      #pragma omp critical
      {
         timlUtilBLASsgemm(CblasNoTrans, CblasTrans, M, K, N, 1.0, layer->delta + b*M*N, layer->convParams.prevFeatureMapReshape, 1.0, layer->convParams.kernelGradAccum, deviceId, threadId);
         timlUtilBLASsgemv(CblasNoTrans, M, N, 1.0, layer->delta + b*M*N, layer->convParams.biasMultiplier, 1.0, layer->convParams.biasGradAccum, deviceId, threadId);
      }
   }

   // back propagate delta
   if (layer->prev->delta != NULL) {
      for (b = 0; b < layer->batchSize; b++) {
         // reset prevDelta to 0
         timlUtilVectorResetFloat(layer->prev->delta + b*prevRow*prevCol*prevChannel, prevRow*prevCol*prevChannel, 0.0, deviceId, threadId);
         // prevDeltaTemp = kernel' * delta -- (K*M)(M*N)
         timlUtilBLASsgemm(CblasTrans, CblasNoTrans, K, N, M, 1.0, layer->convParams.kernel, layer->delta + b*M*N, 0.0, layer->convParams.prevFeatureMapReshape, deviceId, threadId);
         // reshape prevDeltaTemp to prevDelta
         timlUtilConv2ImageReshapeBack(layer->prev->delta + b*prevRow*prevCol*prevChannel, layer->convParams.prevFeatureMapReshape, layer->convParams.prevFeatureMapReshapeIndex, prevChannel, prevRow*prevCol, kernelRow*kernelCol*row*col, deviceId, threadId);
      }
   }

   return err;
}
