/******************************************************************************/
/*!
 * \file timlCNNForwardPropagation.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup   cnn
 * \brief     Forward propagate data to the the input layer
 * \param[in] layer     Layer ptr
 * \param[in] data      Data ptr
 * \param[in] dim       Data dimension
 * \param[in] batchSize Data batch size
 * \return    Error code
 */
/******************************************************************************/

int timlCNNInputForwardPropagation(timlCNNLayer *layer)
{

   int rowOffset;
   int colOffset;
   int deviceId;
   int threadId;

   deviceId = layer->cnn->deviceId;
   threadId = layer->cnn->threadId;

   // testing mode
   if (layer->phase == Util_Test) {
      if (layer->inputParams.testingCropType == Util_CenterCrop) {
         rowOffset = (layer->inputParams.row - layer->row)/2;
         colOffset = (layer->inputParams.col - layer->col)/2;
      }
      else { // randomCrop
         rowOffset = timlUtilRandDiscreteUniformRNG(0, layer->inputParams.row - layer->row);
         colOffset = timlUtilRandDiscreteUniformRNG(0, layer->inputParams.col - layer->col);
      }
      timlUtilTransform(layer->featureMap, layer->inputParams.inputData, layer->row, layer->col, layer->channel, layer->batchSize, rowOffset, colOffset, layer->inputParams.row, layer->inputParams.col, layer->inputParams.scale, layer->inputParams.mean, layer->inputParams.testingMirrorType, deviceId, threadId);
   }
   else { // training mode
      if (layer->inputParams.trainingCropType == Util_CenterCrop) {
         rowOffset = (layer->inputParams.row - layer->row)/2;
         colOffset = (layer->inputParams.col - layer->col)/2;
      }
      else { // randomCrop
         rowOffset = timlUtilRandDiscreteUniformRNG(0, layer->inputParams.row - layer->row);
         colOffset = timlUtilRandDiscreteUniformRNG(0, layer->inputParams.col - layer->col);
      }
      timlUtilTransform(layer->featureMap, layer->inputParams.inputData, layer->row, layer->col, layer->channel, layer->batchSize, rowOffset, colOffset, layer->inputParams.row, layer->inputParams.col, layer->inputParams.scale, layer->inputParams.mean, layer->inputParams.testingMirrorType, deviceId, threadId);
   }

   return 0;
}
