#ifndef TIMLCNN_H_
#define TIMLCNN_H_


/******************************************************************************/
/*!
 * \file timlCNN.h
 * \defgroup cnn cnn
 * \brief    Convolutional neural network
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "timlUtil.h"

//! \ingroup cnn
//@{

/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define ERROR_CNN_OFFSET   4000


/*******************************************************************************
 *
 * ENUM DEFINITIONS
 *
 ******************************************************************************/

typedef enum {
   ERROR_CNN_FEATURE_MAP_SIZE = ERROR_CNN_OFFSET,
   ERROR_CNN_FEATURE_MAP_CHANNEL,
   ERROR_CNN_ALLOCATION,
   ERROR_CNN_LAYER_ALLOCATION,
   ERROR_CNN_TEAM_ALLOCATION,
   ERROR_CNN_CONV_LAYER_KERNEL_SIZE,
   ERROR_CNN_CONV_LAYER_PAD_SIZE,
   ERROR_CNN_CONV_LAYER_STRIDE_SIZE,
   ERROR_CNN_POOLING_LAYER_SCALE_SIZE,
   ERROR_CNN_POOLING_LAYER_PAD_SIZE,
   ERROR_CNN_POOLING_LAYER_STRIDE_SIZE,
   ERROR_CNN_INPUT_LAYER_PARAMS,
   ERROR_CNN_LINEAR_LAYER_DIM,
   ERROR_CNN_NORM_LAYER_PARAMS,
   ERROR_CNN_DROPOUT_LAYER_PARAMS,
   ERROR_CNN_NULL_PTR,
   ERROR_CNN_EMPTY,
   ERROR_CNN_READ_FILE,
   ERROR_CNN_CLASS,
   ERROR_CNN_LOAD_IMAGE,
   ERROR_CNN_LOAD_LABEL,
   ERROR_CNN_IMAGE_DIM_MISMATCH,
   ERROR_CNN_LABEL_DIM_MISMATCH
} timlCNNError;

typedef enum {
   CNN_InterChannel,
   CNN_IntraChannel
} timlCNNNormType;

typedef enum {
   CNN_Input,
   CNN_Conv,
   CNN_Pooling,
   CNN_Nonlinear,
   CNN_Linear,
   CNN_Norm,
   CNN_Dropout,
   CNN_Softmax,
   CNN_SoftmaxCost,
   CNN_Accuracy
} timlCNNLayerType;

typedef enum {
   CNN_MaxPooling,
   CNN_MeanPooling
} timlCNNPoolingType;


/*******************************************************************************
 *
 * STRUCTURE DEFINITIONS
 *
 ******************************************************************************/

typedef struct {
  float *max; /**< size = row*col*maxBatchSize */
  float *sum; /**< size = row*col*maxBatchSize */
  float *jacob; /**< Jacobian matrix */
} timlCNNSoftmaxParams;

typedef struct {
   float *cost;  /**< size = maxBatchSize*/
   int   *label;
} timlCNNSoftmaxCostParams;

typedef struct {
   int *label;
   int *trueLabel;
   int top;
   int success;
} timlCNNAccuracyParams;

typedef struct {
   timlCNNPoolingType type;
   int                scaleRow; /**< pooling kernel row size */
   int                scaleCol; /**< pooling kernel col size */
   int                padUp;
   int                padDown;
   int                padLeft;
   int                padRight;
   int                strideX; /**< pooling kernel stride (horizontal) */
   int                strideY; /**< pooling kernel stride (vertical) */
   int                *maxIndex; /**< recode the indices of the max pooling value so that delta can be back propagated to the pooled position*/
} timlCNNPoolingParams;

typedef struct {
   int                 dim; /**< 1d dimension of the layer **/
   int                 prevDim; /**< 1d dimension of the previous layer */
   float               *weight;
   float               *weightInc;
   float               *weightGradAccum;
   float               *bias;
   float               *biasInc;
   float               *biasGradAccum;
   float               *biasMultiplier; /**< size = maxBatchSize */
   float               weightDecayFactor;
   timlUtilInitializer weightInit;
   timlUtilInitializer biasInit;
   float               weightLearningFactor;
   float               biasLearningFactor;
   bool                shared; /**< if this layer shares the parameters from other layer */
} timlCNNLinearParams;

typedef struct {
   int   num;
   int   channel;
   int   row;
   int   col;
   float *data; /**< size = (rowSize * colSize * channel) * num */
   int   *label; /**< size = num */
} timlCNNDataSet;

typedef struct {
   int                 inputFeatureMapChannel;
   int                 outputFeatureMapChannel;
   int                 kernelRow;
   int                 kernelCol;
   int                 padUp;
   int                 padDown;
   int                 padLeft;
   int                 padRight;
   int                 strideX;
   int                 strideY;
   timlUtilConvType    type;
   float               *prevFeatureMapReshape; /**< reshape the feature map of the previous layer to size (prev->channel*kernelRow*kernelCol) * (row*col) */
   int                 *prevFeatureMapReshapeIndex; /**< the reshape matrix of size (kernelRow*kernelCol) * (row*col) */
   float               *kernelGradAccum;
   float               *kernel; /**< size =  (channel) * (kernelRow*kernelCol*prev->channel) */
   float               *kernelInc;
   float               kernelDecayFactor;
   float               kernelLearningFactor;
   timlUtilInitializer kernelInit;
   int                 *connectivity; /**< connectivity matrix (if prev->featureMap(i) is connected to layer->featureMap(j) by a kernel) */
   float               *bias;
   float               *biasGradAccum;
   float               *biasInc;
   float               *biasMultiplier; /**< size = row*col */
   float               biasLearningFactor;
   timlUtilInitializer biasInit;
   bool                shared; /** if this layer shares parameters from another layer */
} timlCNNConvParams;

typedef struct {
   timlUtilActivationType type;
   float                  *derivative;
} timlCNNNonlinearParams;

typedef struct {
   timlCNNNormType type;
   int             N;
   float           alpha;
   float           beta;
   float           *denom; /**< denominator */
} timlCNNNormParams;

typedef struct {
   int                row; /**< raw data row size */
   int                col; /**< raw data col size */
   int                channel; /**< raw data channel size */
   int                *channelPermute; /**<channel permutation order*/
   float              *mean; /**< mean of the raw data */
   float              scale;
   float              *inputData; /**< raw data */
   timlUtilCropType   trainingCropType;
   timlUtilMirrorType trainingMirrorType;
   timlUtilCropType   testingCropType;
   timlUtilMirrorType testingMirrorType;
   bool               shared; /**< if this layer shares the same mean with some other layer */
} timlCNNInputParams;

typedef struct {
   unsigned int *mask; /**< a mask matrix of values (0,1) */
   float        prob; /**< dropout probability */
} timlCNNDropoutParams;

typedef struct _timlCNNLayer_ {
   int                            id;
   timlCNNLayerType               type;
   int                            row;
   int                            col;
   int                            channel;
   int                            batchSize; /*< batch size*/
   int                            maxBatchSize; /**< max batch size */
   float                          *featureMap;
   float                          *delta; /**< partial derivative of the cost function with regard to each kernel */
   timlUtilPhase                  phase;
   timlUtilAllocatorLevel         allocatorLevel;
   timlCNNDropoutParams           dropoutParams; /**< only one of the layer-specific params structure is valid */
   timlCNNInputParams             inputParams;
   timlCNNConvParams              convParams;
   timlCNNNormParams              normParams;
   timlCNNPoolingParams           poolingParams;
   timlCNNNonlinearParams         nonlinearParams;
   timlCNNLinearParams            linearParams;
   timlCNNSoftmaxParams           softmaxParams;
   timlCNNSoftmaxCostParams       softmaxCostParams;
   timlCNNAccuracyParams          accuracyParams;
   size_t                         forwardCompute;
   size_t                         backwardCompute;
   size_t                         forwardMemory;
   size_t                         backwardMemory;
   size_t                         forwardMemMov;
   size_t                         backwardMemMov;
   size_t                         fixedMemory;
   size_t                         paramsMemory;
   timlUtilMemPoolPos             memPoolPos;
   struct _timlCNNLayer_          *prev; /**< layers are connected with double linked list */
   struct _timlCNNLayer_          *next;
   struct _timlConvNeuralNetwork_ *cnn;
} timlCNNLayer;

typedef struct {
   int                      batchCount; /**< batch count */
   int                      epoch;      /**< how many iterations we need to run through the whole database */
   timlUtilPhase            phase;
   timlUtilAllocatorLevel   allocatorLevel;
   int                      batchSize;    /**< the batch size of the cnn */
   int                      batchUpdate;  /**< how many samples do we process until we update the parameters */
   int                      maxBatchSize; /**< max batch size*/
   float                    momentum;
   float                    learningRate;
   float                    weightDecay;
   timlUtilCostFunctionType costType; /**< how to evaluate the cost with output of the cnn */
} timlCNNTrainingParams;

typedef struct _timlConvNeuralNetwork_ {
   char                 *memPool; /**< used by allocatorLevel3 mode to store the feature maps */
   size_t                memPoolSize; /**< size of the memory pool */
   size_t                fixedMemory;
   size_t                forwardMemory;
   size_t                backwardMemory;
   size_t                forwardMemMov;
   size_t                backwardMemMov;
   size_t                paramsMemory;
   size_t                totalMemory;
   int                   deviceId;
   int                   threadId;
   timlCNNLayer          *head;
   timlCNNLayer          *tail;
   timlCNNTrainingParams params;
   char                  configFileName[TIML_UTIL_MAX_STR];
   char                  paramsFileName[TIML_UTIL_MAX_STR];
   char                  statesFileName[TIML_UTIL_MAX_STR];
} timlConvNeuralNetwork;


/*******************************************************************************
 *
 * PARAMS SHARING FUNCTIONS
 *
 ******************************************************************************/

int timlCNNInputShareParams(timlConvNeuralNetwork *cnn, timlCNNLayer *layer);

int timlCNNConvShareParams(timlConvNeuralNetwork *cnn, timlCNNLayer *layer);

int timlCNNLinearShareParams(timlConvNeuralNetwork *cnn, timlCNNLayer *layer);


/*******************************************************************************
 *
 * INITIALIZATION FUNCTIONS
 *
 ******************************************************************************/

int timlCNNConvInitialize(timlCNNLayer *layer);

int timlCNNLinearInitialize(timlCNNLayer *layer);

int timlCNNNonlinearInitialize(timlCNNLayer *layer);

int timlCNNInputInitialize(timlCNNLayer *layer);

int timlCNNPoolingInitialize(timlCNNLayer *layer);

int timlCNNNormInitialize(timlCNNLayer *layer);

int timlCNNDropoutInitialize(timlCNNLayer *layer);

int timlCNNSoftmaxInitialize(timlCNNLayer *layer);

int timlCNNSoftmaxCostInitialize(timlCNNLayer *layer);

int timlCNNAccuracyInitialize(timlCNNLayer *layer);


/*******************************************************************************
 *
 * BACKWARD PROPAGATION FUNCTIONS
 *
 ******************************************************************************/

int timlCNNBackPropagation(timlConvNeuralNetwork *cnn);

int timlCNNConvBackPropagation(timlCNNLayer *layer);

int timlCNNNormBackPropagation(timlCNNLayer *layer);

int timlCNNPoolingBackPropagation(timlCNNLayer *layer);

int timlCNNMaxPoolingBackPropagation(timlCNNLayer *layer);

int timlCNNMeanPoolingBackPropagation(timlCNNLayer *layer);

int timlCNNNonlinearBackPropagation(timlCNNLayer *layer);

int timlCNNLinearBackPropagation(timlCNNLayer *layer);

int timlCNNDropoutBackPropagation(timlCNNLayer *layer);

int timlCNNSoftmaxCostBackPropagation(timlCNNLayer *layer);

int timlCNNSoftmaxBackPropagation(timlCNNLayer *layer);


/*******************************************************************************
 *
 * FORWARD PROPAGATION FUNCTIONS
 *
 ******************************************************************************/

int timlCNNLoadImage(timlConvNeuralNetwork *cnn, float *image, int row, int col, int channel, int batchSize);

int timlCNNLoadLabel(timlConvNeuralNetwork *cnn, int *label, int row, int col, int batchSize);

int timlCNNLoadParamsFromMemory(timlConvNeuralNetwork *cnnCopy, timlConvNeuralNetwork *cnn);

int timlCNNLoadStatesFromMemory(timlConvNeuralNetwork *cnnCopy, timlConvNeuralNetwork *cnn);

int timlCNNLoadParamsFromBinaryFile(timlConvNeuralNetwork *cnn, const char *fileName);

int timlCNNLoadStatesFromBinaryFile(timlConvNeuralNetwork *cnn, const char *fileName);



int timlCNNForwardPropagation(timlConvNeuralNetwork *cnn);

int timlCNNInputForwardPropagation(timlCNNLayer *layer);

int timlCNNLinearForwardPropagation(timlCNNLayer *layer);

int timlCNNDropoutForwardPropagation(timlCNNLayer *layer);

int timlCNNNonlinearForwardPropagation(timlCNNLayer *layer);

int timlCNNNormForwardPropagation(timlCNNLayer *layer);

int timlCNNPoolingForwardPropagation(timlCNNLayer *layer);

int timlCNNMaxPoolingForwardPropagation(timlCNNLayer *layer);

int timlCNNMeanPoolingForwardPropagation(timlCNNLayer *layer);

int timlCNNConvForwardPropagation(timlCNNLayer *layer);

int timlCNNSoftmaxCostForwardPropagation(timlCNNLayer *layer);

int timlCNNSoftmaxForwardPropagation(timlCNNLayer *layer);

int timlCNNAccuracyForwardPropagation(timlCNNLayer *layer);

/*******************************************************************************
 *
 * DELETE FUNCTIONS
 *
 ******************************************************************************/

int timlCNNFreeConvLayer(timlCNNLayer *layer);

int timlCNNFreeInputLayer(timlCNNLayer * layer);

int timlCNNFreeNonlinearLayer(timlCNNLayer * layer);

int timlCNNFreeNormLayer(timlCNNLayer *layer);

int timlCNNFreePoolingLayer(timlCNNLayer *layer);

int timlCNNFreeLinearLayer(timlCNNLayer * layer);

int timlCNNFreeDropoutLayer(timlCNNLayer *layer);

int timlCNNFreeSoftmaxCostLayer(timlCNNLayer *layer);

int timlCNNFreeSoftmaxLayer(timlCNNLayer *layer);

int timlCNNFreeAccuracyLayer(timlCNNLayer *layer);


/*******************************************************************************
 *
 * RESET FUNCTIONS
 *
 ******************************************************************************/

int timlCNNResetConvLayer(timlCNNLayer *layer);

int timlCNNResetInputLayer(timlCNNLayer * layer);

int timlCNNResetLinearLayer(timlCNNLayer * layer);

int timlCNNResetNonlinearLayer(timlCNNLayer * layer);

int timlCNNResetNormLayer(timlCNNLayer *layer);

int timlCNNResetPoolingLayer(timlCNNLayer *layer);

int timlCNNResetSoftmaxCostLayer(timlCNNLayer *layer);

int timlCNNResetSoftmaxLayer(timlCNNLayer *layer);

int timlCNNResetAccuracyLayer(timlCNNLayer *layer);

/*******************************************************************************
 *
 * UPDATE PARAMS FUNCTIONS
 *
 ******************************************************************************/

int timlCNNUpdateParams(timlConvNeuralNetwork *cnn);

int timlCNNLinearUpdateParams(timlCNNLayer *layer);

int timlCNNConvUpdateParams(timlCNNLayer *layer);


/*******************************************************************************
 *
 * IO FUNCTIONS
 *
 ******************************************************************************/

int timlCNNConvWriteToFile(FILE *fp1, FILE *fp2, FILE *fp3, timlCNNLayer *layer, timlUtilParamsLevel level, const char* name, const char *floatFormat, const char *intFormat);

int timlCNNNonlinearWriteToFile(FILE *fp1, FILE *fp2, FILE *fp3, timlCNNLayer *layer, timlUtilParamsLevel level, const char* name, const char *floatFormat, const char *intFormat);

int timlCNNSoftmaxWriteToFile(FILE *fp1, FILE *fp2, FILE *fp3, timlCNNLayer *layer, timlUtilParamsLevel level, const char* name, const char *floatFormat, const char *intFormat);

int timlCNNSoftmaxCostWriteToFile(FILE *fp1, FILE *fp2, FILE *fp3, timlCNNLayer *layer, timlUtilParamsLevel level, const char* name, const char *floatFormat, const char *intFormat);

int timlCNNNormWriteToFile(FILE *fp1, FILE *fp2, FILE *fp3, timlCNNLayer *layer, timlUtilParamsLevel level, const char* name, const char *floatFormat, const char *intFormat);

int timlCNNPoolingWriteToFile(FILE *fp1, FILE *fp2, FILE *fp3, timlCNNLayer *layer, timlUtilParamsLevel level, const char* name, const char *floatFormat, const char *intFormat);

int timlCNNLinearWriteToFile(FILE *fp1, FILE *fp2, FILE *fp3, timlCNNLayer *layer, timlUtilParamsLevel level, const char* name, const char *floatFormat, const char *intFormat);

int timlCNNInputWriteToFile(FILE *fp1, FILE *fp2, FILE *fp3, timlCNNLayer *layer, timlUtilParamsLevel level, const char* name, const char *floatFormat, const char *intFormat);

int timlCNNTrainingParamsWriteToFile(FILE *fp, timlConvNeuralNetwork *cnn, const char* name, const char *floatFormat, const char *intFormat);

int timlCNNDropoutWriteToFile(FILE *fp1, FILE *fp2, FILE *fp3, timlCNNLayer *layer, timlUtilParamsLevel level, const char* name, const char *floatFormat, const char *intFormat);

int timlCNNConvReadFromTextFile(FILE *fp, timlConvNeuralNetwork *cnn);

int timlCNNTrainingParamsReadFromTextFile(FILE *fp, timlConvNeuralNetwork *cnn);

int timlCNNNormReadFromTextFile(FILE *fp, timlConvNeuralNetwork *cnn);

int timlCNNPoolingReadFromTextFile(FILE *fp, timlConvNeuralNetwork *cnn);

int timlCNNNonlinearReadFromTextFile(FILE *fp, timlConvNeuralNetwork *cnn);

int timlCNNLinearReadFromTextFile(FILE *fp, timlConvNeuralNetwork *cnn);

int timlCNNDropoutReadFromTextFile(FILE *fp, timlConvNeuralNetwork *cnn);

int timlCNNInputReadFromTextFile(FILE *fp, timlConvNeuralNetwork *cnn);

int timlCNNSoftmaxReadFromTextFile(FILE *fp, timlConvNeuralNetwork *cnn);

int timlCNNSoftmaxCostReadFromTextFile(FILE *fp, timlConvNeuralNetwork *cnn);


int timlCNNConvReadFromParamsBinaryFile(FILE *fp2, timlCNNLayer *layer);

int timlCNNConvReadFromParamsMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer);

int timlCNNLinearReadFromParamsBinaryFile(FILE *fp2, timlCNNLayer *layer);

int timlCNNLinearReadFromParamsMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer);

int timlCNNInputReadFromParamsBinaryFile(FILE *fp2, timlCNNLayer *layer);

int timlCNNInputReadFromParamsMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer);



int timlCNNConvReadFromStatesBinaryFile(FILE *fp3, timlCNNLayer *layer);

int timlCNNLinearReadFromStatesMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer);

int timlCNNInputReadFromStatesBinaryFile(FILE *fp3, timlCNNLayer *layer);

int timlCNNInputReadFromStatesMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer);

int timlCNNLinearReadFromStatesBinaryFile(FILE *fp3, timlCNNLayer *layer);

int timlCNNDroputReadFromStatesBinaryFile(FILE *fp3, timlCNNLayer *layer);

int timlCNNDroputReadFromStatesMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer);

int timlCNNPoolingReadFromStatesBinaryFile(FILE *fp3, timlCNNLayer *layer);

int timlCNNPoolingReadFromStatesMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer);

int timlCNNSoftmaxReadFromStatesBinaryFile(FILE *fp3, timlCNNLayer *layer);

int timlCNNSoftmaxReadFromStatesMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer);

int timlCNNSoftmaxCostReadFromStatesBinaryFile(FILE *fp3, timlCNNLayer *layer);

int timlCNNSoftmaxCostReadFromStatesMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer);

int timlCNNAccuracyReadFromStatesBinaryFile(FILE *fp3, timlCNNLayer *layer);

int timlCNNAccuracyReadFromStatesMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer);

int timlCNNNormReadFromStatesBinaryFile(FILE *fp3, timlCNNLayer *layer);

int timlCNNNormReadFromStatesMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer);

/*******************************************************************************
 *
 * MEMORY AND COMPUTE ESTIMATE FUNCTIONS
 *
 ******************************************************************************/

int timlCNNMemory(timlConvNeuralNetwork *cnn);

int timlCNNAccurayMemory(timlCNNLayer *layer);

int timlCNNAccurayCompute(timlCNNLayer *layer);

int timlCNNInputMemory(timlCNNLayer *layer);

int timlCNNInputCompute(timlCNNLayer *layer);

int timlCNNConvMemory(timlCNNLayer *layer);

int timlCNNConvCompute(timlCNNLayer *layer);

int timlCNNNonlinearMemory(timlCNNLayer *layer);

int timlCNNNonlinearCompute(timlCNNLayer *layer);

int timlCNNLinearMemory(timlCNNLayer *layer);

int timlCNNLinearCompute(timlCNNLayer *layer);

int timlCNNPoolingMemory(timlCNNLayer *layer);

int timlCNNPoolingCompute(timlCNNLayer *layer);

int timlCNNMaxPoolingMemory(timlCNNLayer *layer);

int timlCNNMaxPoolingCompute(timlCNNLayer *layer);

int timlCNNMeanPoolingMemory(timlCNNLayer *layer);

int timlCNNMeanPoolingCompute(timlCNNLayer *layer);

int timlCNNNormMemory(timlCNNLayer *layer);

int timlCNNNormCompute(timlCNNLayer *layer);

int timlCNNSoftmaxMemory(timlCNNLayer *layer);

int timlCNNSoftmaxCompute(timlCNNLayer *layer);

int timlCNNSoftmaxCostMemory(timlCNNLayer *layer);

int timlCNNSoftmaxCostCompute(timlCNNLayer *layer);

/*******************************************************************************
 *
 * MISC FUNCTIONS
 *
 ******************************************************************************/

const char* timlCNNLayerTypeStr(timlCNNLayer* layer);

int timlCNNMemPoolSize(timlConvNeuralNetwork *cnn);

int timlCNNSetBatchSize(timlConvNeuralNetwork *cnn, int batchSize);

/*******************************************************************************
 *
 * FUNCTIONS WITH OPENMP ACCELERATION
 *
 ******************************************************************************/

int timlCNNSupervisedTrainingWithLabelTeamModeOpenMP(timlConvNeuralNetwork **cnnTream, int teamNum, float *image, int row, int col, int channel, int *label, int labelRow, int labelCol, int batchUpdate);

int timlCNNClassifyAccuracyTeamModeOpenMP(timlConvNeuralNetwork **cnnTeam, int teamNum, float *image, int row, int col, int channel, int *label, int labelRow, int labelCol, int num, int *success);
//@}

#endif /* TIMLCNN_H_ */

