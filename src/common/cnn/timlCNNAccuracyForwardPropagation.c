/******************************************************************************/
/*!
 * \file timlCNNAccuracyForwardPropagation.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Forward propagate form prevLayer to prevLayer->next
 * \param[in]     prevLayer Previous layer ptr
 * \return        Error code
 */
/******************************************************************************/

int timlCNNAccuracyForwardPropagation(timlCNNLayer *prevLayer)
{
   timlCNNLayer *layer;
   int          dim;
   int          deviceId;
   int          threadId;
   int          r;
   int          c;
   int          b;
   int          offset;
   int          trueLabel;

   // init
   layer    = prevLayer->next;
   dim      = prevLayer->channel*prevLayer->row*prevLayer->col*prevLayer->batchSize;
   deviceId = prevLayer->cnn->deviceId;
   threadId = prevLayer->cnn->threadId;

   layer->accuracyParams.success = 0;
   timlUtilMemcpy(layer->featureMap, prevLayer->featureMap, dim*sizeof(float), deviceId, threadId);


   for(b = 0 ; b < layer->batchSize; b++) {
      for (r = 0; r < layer->row; r++) {
         for (c = 0; c < layer->col; c++) {
            offset = b*layer->row*layer->col*layer->channel + r*layer->col + c;
            trueLabel = layer->accuracyParams.trueLabel[b*layer->row*layer->col + r*layer->col + c];
            timlUtilVectorSortIndexFloat(layer->featureMap + offset, layer->accuracyParams.label + offset, layer->channel, layer->row*layer->col, Util_Descend);
            if (timlUtilHitLabel(layer->accuracyParams.label + offset, layer->accuracyParams.top, layer->row*layer->col, trueLabel)) {
               layer->accuracyParams.success++;
            }
         }
      }
   }

   return 0;


}
