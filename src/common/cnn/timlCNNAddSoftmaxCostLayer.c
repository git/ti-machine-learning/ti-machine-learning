/******************************************************************************/
/*!
 * \file timlCNNAddSoftmaxCostLayer.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"

/******************************************************************************/


/******************************************************************************/
/*!
 * \ingroup   cnn
 * \brief     Add softmax cost layer
 * \param[in] cnn CNN
 * \return    Error code
 */
/******************************************************************************/

int timlCNNAddSoftmaxCostLayer(timlConvNeuralNetwork *cnn)
{
   timlCNNLayer *prev;
   timlCNNLayer *softmaxCostLayer;

   // error checking
   if (cnn == NULL) {
      return ERROR_CNN_NULL_PTR;
   }
   if (cnn->tail == NULL) {
      return ERROR_CNN_EMPTY;
   }

   if (timlUtilMallocHost((void**)&softmaxCostLayer, sizeof(timlCNNLayer))) {
      return ERROR_CNN_LAYER_ALLOCATION;
   }

   prev = cnn->tail;
   softmaxCostLayer->type                       = CNN_SoftmaxCost;
   softmaxCostLayer->row                        = prev->row;
   softmaxCostLayer->col                        = prev->col;
   softmaxCostLayer->channel                    = prev->channel;
   softmaxCostLayer->batchSize                  = prev->batchSize;
   softmaxCostLayer->maxBatchSize               = prev->maxBatchSize;
   softmaxCostLayer->allocatorLevel             = cnn->params.allocatorLevel;
   softmaxCostLayer->phase                      = cnn->params.phase;
   softmaxCostLayer->featureMap                 = NULL;
   softmaxCostLayer->delta                      = NULL;
   softmaxCostLayer->softmaxParams.max          = NULL;
   softmaxCostLayer->softmaxParams.sum          = NULL;

   // link the list
   softmaxCostLayer->cnn        = cnn;
   softmaxCostLayer->id         = prev->id + 1;
   softmaxCostLayer->prev       = prev;
   softmaxCostLayer->prev->next = softmaxCostLayer;
   softmaxCostLayer->next       = NULL;
   cnn->tail                    = softmaxCostLayer;

   return 0;

}
