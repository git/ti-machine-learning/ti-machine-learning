/******************************************************************************/
/*!
 * \file timlCNNTrainingParamsWriteToFile.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Write the training params to file(s)
 * \param[in,out] fp          FILE ptr to the level 1 text file
 * \param[in]     cnn         CNN
 * \param[in]     name        CNN name
 * \param[in]     floatFormat Format string for floats
 * \param[in]     intFormat   Format string for ints
 * \return        Error code
 */
/******************************************************************************/

int timlCNNTrainingParamsWriteToFile(FILE *fp, timlConvNeuralNetwork *cnn, const char* name, const char *floatFormat, const char *intFormat)
{
   fprintf(fp, "%s.params.batchUpdate = %d;\n", name, cnn->params.batchUpdate);
   fprintf(fp, "%s.params.batchSize = %d;\n", name, cnn->params.batchSize);
   fprintf(fp, "%s.params.maxBatchSize = %d;\n", name, cnn->params.maxBatchSize);
   fprintf(fp, "%s.params.epoch = %d;\n", name, cnn->params.epoch);
   fprintf(fp, "%s.params.learningRate = ", name);
   fprintf(fp, floatFormat, cnn->params.learningRate);
   fprintf(fp, ";\n");
   fprintf(fp, "%s.params.momentum = ", name);
   fprintf(fp, floatFormat, cnn->params.momentum);
   fprintf(fp, ";\n");
   fprintf(fp, "%s.params.phase = %d;\n", name, cnn->params.phase);
   fprintf(fp, "%s.params.allocatorLevel = %d;\n", name, cnn->params.allocatorLevel);
   fprintf(fp, "%s.params.costType = %d;\n", name, cnn->params.costType);

   return 0;
}
