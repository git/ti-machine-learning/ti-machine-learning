/******************************************************************************/
/*!
 * \file timlCNNLinearReadFromStatesMemory.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Read the linear layer states from a binary file
 * \param[out]    layerCopy Copied layer
 * \param[in]     layer     Target layer
 * \return        Error code
 */
/******************************************************************************/

int timlCNNLinearReadFromStatesMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer)
{
   int dim;
   int deviceId = layerCopy->cnn->deviceId;
   int threadId = layerCopy->cnn->threadId;


   dim = layer->maxBatchSize*layer->row*layer->col*layer->channel;
   // feature map
   if (layerCopy->featureMap != NULL && layer->featureMap != NULL) {
      timlUtilMemcpy(layerCopy->featureMap, layer->featureMap, sizeof(float)*dim, deviceId, threadId);
   }

   if (layer->allocatorLevel == Util_AllocatorLevel1) {
      // delta
      if (layerCopy->delta != NULL && layer->delta != NULL) {
         timlUtilMemcpy(layerCopy->delta, layer->delta, sizeof(float)*dim, deviceId, threadId);
      }
      if (layerCopy->linearParams.weightInc != NULL && layer->linearParams.weightInc != NULL) {
         timlUtilMemcpy(layerCopy->linearParams.weightInc, layer->linearParams.weightInc, sizeof(float)*layer->linearParams.dim*layer->linearParams.prevDim, deviceId, threadId);
      }
      if (layerCopy->linearParams.weightGradAccum != NULL && layer->linearParams.weightGradAccum != NULL) {
         timlUtilMemcpy(layerCopy->linearParams.weightGradAccum, layer->linearParams.weightGradAccum, sizeof(float)*layer->linearParams.dim*layer->linearParams.prevDim, deviceId, threadId);
      }
      if (layerCopy->linearParams.biasInc != NULL && layer->linearParams.biasInc != NULL) {
         timlUtilMemcpy(layerCopy->linearParams.biasInc, layer->linearParams.biasInc, sizeof(float)*layer->linearParams.dim, deviceId, threadId);
      }
      if (layerCopy->linearParams.biasGradAccum != NULL && layer->linearParams.biasGradAccum != NULL) {
         timlUtilMemcpy(layerCopy->linearParams.biasGradAccum, layer->linearParams.biasGradAccum, sizeof(float)*layer->linearParams.dim, deviceId, threadId);
      }
   }
   return 0;
}
