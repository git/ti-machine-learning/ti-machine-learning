/******************************************************************************/
/*!
 * \file timlCNNResetConvLayer.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup   cnn
 * \brief     Reset conv layer
 * \param[in] layer Layer ptr
 * \return    Error code
 */
/******************************************************************************/

int timlCNNResetConvLayer(timlCNNLayer *layer)
{
   int channel;
   int inputFeatureMapChannel;
   int outputFeatureMapChannel;
   int kernelRow;
   int kernelCol;
   int dim;
   int deviceId;
   int threadId;

   // init
   channel                 = layer->channel;
   inputFeatureMapChannel  = layer->convParams.inputFeatureMapChannel;
   outputFeatureMapChannel = layer->convParams.outputFeatureMapChannel;
   kernelRow               = layer->convParams.kernelRow;
   kernelCol               = layer->convParams.kernelCol;
   deviceId                = layer->cnn->deviceId;
   threadId                = layer->cnn->threadId;
   dim                     = kernelRow*kernelCol*inputFeatureMapChannel*outputFeatureMapChannel;

   // level 1
   if (layer->allocatorLevel == Util_AllocatorLevel1) {
      timlUtilVectorResetFloat(layer->convParams.kernelInc, dim, 0.0, deviceId, threadId);
      timlUtilVectorResetFloat(layer->convParams.kernelGradAccum, dim, 0.0, deviceId, threadId);
      timlUtilVectorResetFloat(layer->convParams.biasInc, channel, 0.0, deviceId, threadId);
      timlUtilVectorResetFloat(layer->convParams.biasGradAccum, channel, 0.0, deviceId, threadId);
   }

   // initialize kenel
   switch (layer->convParams.kernelInit.type) {
      case Util_Xavier:
         timlUtilRandContinuousUniformRNG(layer->convParams.kernel, dim, -sqrtf(3.0/dim), sqrtf(3.0/dim));
         break;
      case Util_Gaussian:
         timlUtilRandNormalRNG(layer->convParams.kernel, dim, layer->convParams.kernelInit.mean, layer->convParams.kernelInit.std);
         break;
      case Util_Constant:
         break;
      default:
         break;
   }
   // initialize bias
   switch (layer->convParams.biasInit.type) {
      case Util_Xavier:
         timlUtilRandContinuousUniformRNG(layer->convParams.bias, channel, -sqrtf(3.0/channel), sqrtf(3.0/channel));
         break;
      case Util_Gaussian:
         timlUtilRandNormalRNG(layer->convParams.bias, channel, layer->convParams.biasInit.mean, layer->convParams.biasInit.std);
         break;
      case Util_Constant:
         timlUtilVectorResetFloat(layer->convParams.bias, channel, layer->convParams.biasInit.val, deviceId, threadId);
         break;
      default:
         break;
   }

   return 0;
}
