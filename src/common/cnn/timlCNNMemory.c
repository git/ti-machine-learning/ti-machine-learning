/******************************************************************************/
/*!
 * \file timlCNNMemory.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup   cnn
 * \brief     Calculate the memory in bytes required by the cnn
 * \param[in,out] cnn CNN
 * \return        Error code
 */
/******************************************************************************/

int timlCNNMemory(timlConvNeuralNetwork *cnn)
{
   timlCNNLayer *layer;
   int dataSize = sizeof(float);

   cnn->forwardMemory = 0;
   cnn->backwardMemory = 0;
   cnn->paramsMemory = 0;
   cnn->fixedMemory = sizeof(cnn);

   layer = cnn->head;
   while (layer != NULL) {
      switch (layer->type) {
         case CNN_Input:
            timlCNNInputMemory(layer);
            cnn->memPoolSize = layer->forwardMemory + dataSize*layer->maxBatchSize*layer->inputParams.row*layer->inputParams.col*layer->inputParams.channel;
            break;
         case CNN_Conv:
            timlCNNConvMemory(layer);
            break;
         case CNN_Linear:
            timlCNNLinearMemory(layer);
            break;
         case CNN_Nonlinear:
            timlCNNNonlinearMemory(layer);
            break;
         case CNN_Pooling:
            timlCNNPoolingMemory(layer);
            break;
         case CNN_Norm:
            timlCNNNormMemory(layer);
            break;
         case CNN_Dropout:
            timlCNNDropoutMemory(layer);
            break;
         case CNN_Softmax:
            timlCNNSoftmaxMemory(layer);
            break;
         case CNN_SoftmaxCost:
            timlCNNSoftmaxCostMemory(layer);
            break;
         case CNN_Accuracy:
            timlCNNAccuracyMemory(layer);
            break;
         default:
            break;
      }
      if (layer->prev != NULL && (layer->prev->forwardMemory + layer->forwardMemory > cnn->memPoolSize)) {
         cnn->memPoolSize = layer->prev->forwardMemory + layer->forwardMemory;
      }
      cnn->forwardMemory += layer->forwardMemory;
      cnn->backwardMemory += layer->backwardMemory;
      cnn->paramsMemory += layer->paramsMemory;
      cnn->fixedMemory += sizeof(timlCNNLayer);
      layer = layer->next;
   }
      return 0;
}
