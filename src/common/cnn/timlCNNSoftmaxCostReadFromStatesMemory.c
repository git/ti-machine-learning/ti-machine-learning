/******************************************************************************/
/*!
 * \file timlCNNSoftmaxCostReadFromStatesMemory.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Read the softmax cost layer states from memory
 * \param[out]    layerCopy Copied layer
 * \param[in]     layer     Target layer
 * \return        Error code
 */
/******************************************************************************/

int timlCNNSoftmaxCostReadFromStatesMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer)
{
   int deviceId = layerCopy->cnn->deviceId;
   int threadId = layerCopy->cnn->threadId;

   if (layer->featureMap != NULL && layerCopy->featureMap != NULL) {
      timlUtilMemcpy(layerCopy->featureMap, layer->featureMap , sizeof(float)*layer->maxBatchSize*layer->row*layer->col*layer->channel, deviceId, threadId);
   }
   if (layer->softmaxParams.max != NULL && layerCopy->softmaxParams.max != NULL) {
      timlUtilMemcpy(layerCopy->softmaxParams.max, layer->softmaxParams.max, sizeof(float)*layer->maxBatchSize*layer->row*layer->col, deviceId, threadId);
   }
   if (layer->softmaxParams.sum != NULL && layerCopy->softmaxParams.sum != NULL) {
      timlUtilMemcpy(layerCopy->softmaxParams.sum, layer->softmaxParams.sum, sizeof(float)*layer->maxBatchSize*layer->row*layer->col, deviceId, threadId);
   }
   if (layer->softmaxCostParams.cost != NULL && layerCopy->softmaxCostParams.cost != NULL) {
      timlUtilMemcpy(layerCopy->softmaxCostParams.cost, layer->softmaxCostParams.cost, sizeof(float)*layer->maxBatchSize, deviceId, threadId);
   }
   if (layer->softmaxCostParams.label != NULL && layerCopy->softmaxCostParams.label != NULL) {
      timlUtilMemcpy(layerCopy->softmaxCostParams.label, layer->softmaxCostParams.label, sizeof(int)*layer->maxBatchSize*layer->row*layer->col, deviceId, threadId);
   }
   return 0;
}
