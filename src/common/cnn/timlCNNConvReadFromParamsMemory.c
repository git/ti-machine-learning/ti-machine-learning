/******************************************************************************/
/*!
 * \file timlCNNConvReadFromParamsMemory.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Read the conv layer parameters from memory
 * \param[out]    layerCopy Copied layer
 * \param[in]     layer     Target layer
 * \return        Error code
 */
/******************************************************************************/

int timlCNNConvReadFromParamsMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer)
{
   int dim;
   int deviceId;
   int threadId;
   deviceId = layerCopy->cnn->deviceId;
   threadId = layerCopy->cnn->threadId;
   // read params
   dim = layer->convParams.kernelRow*layer->convParams.kernelCol*layer->convParams.inputFeatureMapChannel*layer->convParams.outputFeatureMapChannel;
   if (layer->convParams.kernel != NULL && layerCopy->convParams.kernel != NULL) {
      timlUtilMemcpy(layerCopy->convParams.kernel, layer->convParams.kernel, sizeof(float)*dim, deviceId, threadId);
   }
   if(layer->convParams.bias != NULL && layerCopy->convParams.bias != NULL) {
      timlUtilMemcpy(layerCopy->convParams.bias, layer->convParams.bias, sizeof(float)*layer->channel, deviceId, threadId);
   }

   return 0;
}
