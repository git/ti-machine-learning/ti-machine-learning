/******************************************************************************/
/*!
 * \file timlCNNSoftmaxInitialize.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup   cnn
 * \brief     Initialize the softmax layer
 * \param[in] layer Layer ptr
 * \return    Error code
 */
/******************************************************************************/

int timlCNNSoftmaxInitialize(timlCNNLayer *layer)
{
   timlConvNeuralNetwork *cnn = layer->cnn;
   char                  *offset;

   // level 1
   if (layer->allocatorLevel == Util_AllocatorLevel1) {
      // feature map
      if (timlUtilMallocAcc((void**) &(layer->featureMap), sizeof(float)*layer->maxBatchSize*layer->row*layer->col*layer->channel) != 0) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }
      // feature map delta
      if (timlUtilMallocAcc((void**) &(layer->delta), sizeof(float)*layer->maxBatchSize*layer->row*layer->col*layer->channel) != 0) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }
      // jacobian matrix
      if (timlUtilMallocAcc((void**) &(layer->softmaxParams.jacob), sizeof(float)*layer->maxBatchSize*layer->row*layer->col*layer->channel*layer->channel) != 0) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }
      // max
      if (timlUtilMallocAcc((void**) &(layer->softmaxParams.max), sizeof(float)*layer->maxBatchSize*layer->row*layer->col) != 0) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }
      // sum
      if (timlUtilMallocAcc((void**) &(layer->softmaxParams.sum), sizeof(float)*layer->maxBatchSize*layer->row*layer->col) != 0) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }
   }

   // level 2
   if (layer->allocatorLevel == Util_AllocatorLevel2) {
      // feature map
      if (timlUtilMallocAcc((void**) &(layer->featureMap), sizeof(float)*layer->maxBatchSize*layer->row*layer->col*layer->channel) != 0) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }
      // max
      if (timlUtilMallocAcc((void**) &(layer->softmaxParams.max), sizeof(float)*layer->maxBatchSize*layer->row*layer->col) != 0) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }
      // sum
      if (timlUtilMallocAcc((void**) &(layer->softmaxParams.sum), sizeof(float)*layer->maxBatchSize*layer->row*layer->col) != 0) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }
   }

   // allocatorLevel 3 specific
   if (layer->allocatorLevel == Util_AllocatorLevel3) {
      if (layer->prev->memPoolPos == Util_MemPoolTop) {
         layer->memPoolPos = Util_MemPoolTop;
         layer->featureMap = layer->prev->featureMap;
         offset = cnn->memPool + sizeof(float)*layer->maxBatchSize*layer->row*layer->col*layer->channel;
         layer->softmaxParams.max = offset;
         offset += sizeof(float)*layer->maxBatchSize*layer->row*layer->col;
         layer->softmaxParams.sum = offset;
      }
      else {
         layer->memPoolPos = Util_MemPoolBottom;
         offset = cnn->memPool + cnn->memPoolSize - layer->forwardMemory;
         layer->softmaxParams.max = offset;
         offset += sizeof(float)*layer->maxBatchSize*layer->row*layer->col;
         layer->softmaxParams.sum = offset;
         layer->featureMap = layer->prev->featureMap;
      }
   }
   return 0;
}
