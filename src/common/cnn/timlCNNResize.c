/******************************************************************************/
/*!
 * \file timlCNNResize.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup cnn
 * \brief     Resize the feature map sizes to accommodate new input feature map dimensions
 * \details   Linear layers will be converted to convolutional layer
 * \param[in] cnn     CNN
 * \param[in] row     New input feature map row size
 * \param[in] col     New input feature map col size
 * \param[in] channel New input feature map channel size
 * \return    Error code
 */
/******************************************************************************/

int timlCNNResize(timlConvNeuralNetwork *cnn, int row, int col, int channel)
{
   timlCNNLayer *layer;
   timlCNNLayer *nextLayer;
   timlCNNLayer *prevLayer;
   timlCNNLayer *tail;
   int deviceId = cnn->deviceId;
   int threadId = cnn->threadId;

   if (cnn == NULL) {
      return ERROR_CNN_NULL_PTR;
   }
   timlCNNFree(cnn);

   // first pass, convert all linear layer to conv layer
   layer = cnn->head;
   while (layer != NULL) {
      if (layer->type == CNN_Linear) {
         nextLayer = layer->next;
         prevLayer = layer->prev;
         timlCNNDeleteLayer(layer);
         tail = cnn->tail;
         cnn->tail = prevLayer;
         timlCNNAddConvLayer(cnn, prevLayer->row, prevLayer->col, 1, 1, layer->channel, timlCNNConvParamsDefault());
         cnn->tail->convParams.type = Util_Corr2D;
         cnn->tail->convParams.kernelDecayFactor = layer->linearParams.weightDecayFactor;
         cnn->tail->convParams.kernelLearningFactor = layer->linearParams.weightLearningFactor;
         cnn->tail->convParams.biasLearningFactor = layer->linearParams.biasLearningFactor;
         cnn->tail->next = nextLayer;
         nextLayer->prev = cnn->tail;
         cnn->tail = tail;
      }
      layer = layer->next;
   }

   // second pass, resize all the layers
   layer = cnn->head;
   while (layer != NULL) {
      switch (layer->type) {
         case CNN_Input:
            layer->inputParams.row = row;
            layer->inputParams.col = col;
            layer->inputParams.channel = channel;
            layer->row = row;
            layer->col = col;
            layer->channel = channel;
            break;
         case CNN_Conv:
            layer->row = (layer->prev->row - layer->convParams.kernelRow + layer->convParams.padUp + layer->convParams.padDown)/layer->convParams.strideY + 1;
            layer->col = (layer->prev->col - layer->convParams.kernelCol + layer->convParams.padLeft + layer->convParams.padRight)/layer->convParams.strideX + 1;
            break;
         case CNN_Norm:
            layer->row = layer->prev->row;
            layer->col = layer->prev->col;
            layer->channel = layer->prev->channel;
            break;
         case CNN_Pooling:
            layer->row = ceilf((layer->prev->row - layer->poolingParams.scaleRow + layer->poolingParams.padUp + layer->poolingParams.padDown)/(float)layer->poolingParams.strideY) + 1;
            layer->col = ceilf((layer->prev->col - layer->poolingParams.scaleCol + layer->poolingParams.padLeft + layer->poolingParams.padRight)/(float) layer->poolingParams.strideX) + 1;
            layer->channel = layer->prev->channel;
            break;
         case CNN_Dropout:
            layer->row = layer->prev->row;
            layer->col = layer->prev->col;
            layer->channel = layer->prev->channel;
            break;
         case CNN_Nonlinear:
            layer->row = layer->prev->row;
            layer->col = layer->prev->col;
            layer->channel = layer->prev->channel;
            break;
         case CNN_Softmax:
            layer->row = layer->prev->row;
            layer->col = layer->prev->col;
            layer->channel = layer->prev->channel;
            break;
         case CNN_SoftmaxCost:
            layer->row = layer->prev->row;
            layer->col = layer->prev->col;
            layer->channel = layer->prev->channel;
            break;
         case CNN_Accuracy:
            layer->row = layer->prev->row;
            layer->col = layer->prev->col;
            layer->channel = layer->prev->channel;
            break;
         default:
            break;
      }
      layer = layer->next;
   }

   timlCNNInitialize(cnn);
   timlCNNLoadParamsFromFile(cnn, cnn->paramsFileName);
   return 0;

}
