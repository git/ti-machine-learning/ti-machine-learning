/******************************************************************************/
/*!
 * \file timlCNNAccuracyReadFromStatesMemory.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Read the accuracy layer states from memory
 * \param[in]     layer      Target layer ptr
 * \param[out]    layerCopy  Copied layer ptr
 * \return        Error code
 */
/******************************************************************************/

int timlCNNAccuracyReadFromStatesMemory(timlCNNLayer *layerCopy, timlCNNLayer *layer)
{
   int dim;
   int deviceId;
   int threadId;
   deviceId = layerCopy->cnn->deviceId;
   threadId = layerCopy->cnn->threadId;
      // feature map
      dim = layer->maxBatchSize*layer->row*layer->col*layer->channel;
      if (layerCopy->featureMap != NULL && layer->featureMap != NULL) {
         timlUtilMemcpy(layerCopy->featureMap, layer->featureMap, sizeof(float)*dim, deviceId, threadId);
      }
      // label
      if (layerCopy->accuracyParams.label != NULL && layer->accuracyParams.label != NULL) {
         timlUtilMemcpy(layerCopy->accuracyParams.label, layer->accuracyParams.label, sizeof(int)*dim, deviceId, threadId);
      }
      // true label
      if (layerCopy->accuracyParams.trueLabel != NULL && layer->accuracyParams.trueLabel != NULL) {
         timlUtilMemcpy(layerCopy->accuracyParams.trueLabel, layer->accuracyParams.trueLabel, sizeof(int)*layer->maxBatchSize*layer->row*layer->col, threadId, deviceId);
      }
   return 0;
}
