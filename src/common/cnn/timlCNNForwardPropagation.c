/******************************************************************************/
/*!
 * \file timlCNNForwardPropagation.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"

/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Forward propagate data to the CNN
 * \param[in,out] cnn
 * \param[in]     data Data ptr
 * \param[in]     dim  Data dimension
 * \return        Error code
 */
/******************************************************************************/

int timlCNNForwardPropagation(timlConvNeuralNetwork *cnn)
{
   int          err;
   timlCNNLayer *layer;
   char str[100];
   FILE *fp;

   err = 0;
   if (cnn == NULL) {
      return ERROR_CNN_NULL_PTR;
   }
   layer = cnn->head;
   err = timlCNNInputForwardPropagation(layer);



   while (layer->next != NULL) {
//      sprintf(str, "gpu_level_%d_layer_%d_fm", layer->allocatorLevel, layer->id);
//      fp = fopen(str, "w");
//      timlUtilFwrite(layer->featureMap, sizeof(float), layer->row*layer->col*layer->channel*layer->maxBatchSize, fp);
//      fclose(fp);
      switch (layer->next->type) {
         case CNN_Accuracy:
            err = timlCNNAccuracyForwardPropagation(layer);
            if (err) return err;
            break;
         case CNN_Conv:
            err = timlCNNConvForwardPropagation(layer);
            if (err) return err;
            break;
         case CNN_Norm:
            err = timlCNNNormForwardPropagation(layer);
            if (err) return err;
            break;
         case CNN_Pooling:
            err = timlCNNPoolingForwardPropagation(layer);
            if (err) return err;
            break;
         case CNN_Linear:
            err = timlCNNLinearForwardPropagation(layer);
            if (err) return err;
            break;
         case CNN_Nonlinear:
            err = timlCNNNonlinearForwardPropagation(layer);
            break;
         case CNN_Dropout:
            err = timlCNNDropoutForwardPropagation(layer);
            if (err) return err;
            break;
         case CNN_SoftmaxCost:
            err = timlCNNSoftmaxCostForwardPropagation(layer);
            if (err) return err;
            break;
         case CNN_Softmax:
            err = timlCNNSoftmaxForwardPropagation(layer);
            if (err) return err;
            break;
         default:
            break;
      }
      layer = layer->next;

   }

//   sprintf(str, "gpu_level_%d_layer_%d_fm", layer->allocatorLevel, layer->id);
//   fp = fopen(str, "w");
//   timlUtilFwrite(layer->featureMap, sizeof(float), layer->row*layer->col*layer->channel*layer->maxBatchSize, fp);
//   fclose(fp);

   return err;
}
