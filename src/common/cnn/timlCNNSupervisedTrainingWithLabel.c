/******************************************************************************/
 /*!
 * \file timlCNNSupervisedTrainingWithLabel.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Supervised training with label
 * \param[in,out] cnn       CNN
 * \param[in]     data      Data batch
 * \param[in]     label     Label ptr
 * \param[in]     dataDim   Data dimension
 * \param[in]     labelDim  Label dimension
 * \param[in]     batchSize Batch update size
 * \return        Error code
 */
/******************************************************************************/

int timlCNNSupervisedTrainingWithLabel(timlConvNeuralNetwork *cnn, float *image, int row, int col, int channel, int *label, int labelRow, int labelCol, int batchUpdate)
{
   int          i;
   int          err;
   float        batchCost;
   int          cnnBatchSize;
   int          dataDim;
   int          labelDim;
   int          iter;

   // error checking
   if (cnn->head->inputParams.row != row || cnn->head->inputParams.col != col || cnn->head->inputParams.channel != channel) {
      return ERROR_CNN_IMAGE_DIM_MISMATCH;
   }
   if (cnn->tail->row != labelRow || cnn->tail->col != labelCol ) {
      return ERROR_CNN_LABEL_DIM_MISMATCH;
   }
   err          = 0;
   dataDim      = row*col*channel;
   labelDim     = labelRow*labelCol;
   cnnBatchSize = cnn->params.batchSize;
   iter         = batchUpdate/cnnBatchSize;
   batchCost    = 0;
   float cost;

   for (i = 0; i < iter; i++) {
	   err = timlCNNLoadImage(cnn, image + i*cnnBatchSize*dataDim, row, col, channel, cnnBatchSize);
	   err = timlCNNLoadLabel(cnn, label + i*cnnBatchSize*labelDim, labelRow, labelCol, cnnBatchSize);
	   err = timlCNNForwardPropagation(cnn);
	   err = timlCNNBackPropagation(cnn);
      timlUtilBLASsasum(cnnBatchSize*labelDim, cnn->tail->softmaxCostParams.cost, &cost, cnn->deviceId, cnn->threadId);
	   batchCost += cost;
   }
   printf("batch cost = %f\n", batchCost/(float)(cnnBatchSize*labelDim*iter));
   timlCNNUpdateParams(cnn);

   return err;
}
