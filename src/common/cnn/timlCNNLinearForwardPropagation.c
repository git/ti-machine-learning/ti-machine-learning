/******************************************************************************/
/*!
 * \file timlCNNLinearForwardPropagation.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Forward propagate form layer to layer->next
 * \param[in]     prevLayer Previous layer ptr
 * \return        Error code
 */
/******************************************************************************/

int timlCNNLinearForwardPropagation(timlCNNLayer *prevLayer)
{
   int          err;
   timlCNNLayer *layer;
   int          prevDim;
   int          dim;
   int          deviceId;
   int          threadId;
   int          M;
   int          N;
   int          K;

   err      = 0;
   layer    = prevLayer->next;
   M        = layer->batchSize;
   K        = layer->linearParams.prevDim;
   N        = layer->linearParams.dim;
   prevDim  = prevLayer->row*prevLayer->col*prevLayer->channel;
   dim      = layer->channel;
   deviceId = prevLayer->cnn->deviceId;
   threadId = prevLayer->cnn->threadId;

   // pre-activtion input: featureMap = weight * prevLayer->featureMap + bias
//   timlUtilBLASsgemv(CblasNoTrans, dim, prevDim, 1.0, layer->linearParams.weight, prevLayer->featureMap, 0.0, layer->featureMap, deviceId, threadId);
//   timlUtilBLASsaxpy(dim, 1.0, layer->linearParams.bias, layer->featureMap, deviceId, threadId);
   timlUtilBLASsgemm(CblasNoTrans, CblasTrans, M, N, K, 1.0, prevLayer->featureMap, layer->linearParams.weight, 0.0, layer->featureMap, deviceId, threadId);
   timlUtilBLASsgemm(CblasNoTrans, CblasNoTrans, M, N, 1, 1.0, layer->linearParams.biasMultiplier, layer->linearParams.bias, 1.0, layer->featureMap, deviceId, threadId);

   return err;
}
