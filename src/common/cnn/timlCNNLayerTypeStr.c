/******************************************************************************/
/*!
 * \file timlCNNLayerTypeStr.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup   cnn
 * \brief     Return a string that represents the layer type
 * \param[in] layer Layer pointer
 * \return    Layer type string
 */
/******************************************************************************/

const char* timlCNNLayerTypeStr(timlCNNLayer* layer)
{
   switch (layer->type) {
      case CNN_Input:
         return "Input";
         break;
      case CNN_Conv:
         return "Conv";
         break;
      case CNN_Pooling:
         if (layer->poolingParams.type == CNN_MaxPooling)
            return "MaxPooling";
         else
            return "MeanPooling";
         break;
      case CNN_Accuracy:
    	 return "Accuracy";
    	 break;
      case CNN_Softmax:
       return "Softmax";
       break;
      case CNN_SoftmaxCost:
    	 return "SoftmaxCost";
    	 break;
      case CNN_Nonlinear:
         switch (layer->nonlinearParams.type) {
            case Util_Relu:
               return "Relu";
               break;
            case Util_Sigmoid:
               return "Sigmoid";
               break;
            case Util_Tanh:
               return "Tanh";
               break;
            default:
               break;
         }
         break;
      case CNN_Linear:
         return "Linear";
         break;
      case CNN_Norm:
         return "Normalization";
         break;
      case CNN_Dropout:
         return "Dropout";
         break;
      default:
         return "Unknown"; /**< return unknown type */
   }
   return "Unknown";
}
