/******************************************************************************/
/*!
 * \file timlCNNAddDropoutLayer.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Add dropout layer
 * \param[in,out] cnn  CNN
 * \param[in]     prob Dropout probability
 * \return        Error code
 */
/******************************************************************************/

int timlCNNAddDropoutLayer(timlConvNeuralNetwork *cnn, float prob)
{
   int          err = 0;
   timlCNNLayer *prev;
   timlCNNLayer *newLayer;

   // error checking
   if (cnn == NULL) {
      return ERROR_CNN_NULL_PTR;
   }
   if (cnn->tail == NULL) {
      return ERROR_CNN_EMPTY;
   }
   if (prob <= 0 || prob >= 1.0) {
      return ERROR_CNN_DROPOUT_LAYER_PARAMS;
   }

   prev     = cnn->tail;
   if (timlUtilMallocHost((void**)&newLayer, sizeof(timlCNNLayer))) {
      return ERROR_CNN_LAYER_ALLOCATION;
   }

   newLayer->row                        = prev->row;
   newLayer->col                        = prev->col;
   newLayer->channel                    = prev->channel;
   newLayer->batchSize                  = prev->batchSize;
   newLayer->maxBatchSize               = prev->maxBatchSize;
   newLayer->dropoutParams.prob         = prob;
   newLayer->dropoutParams.mask         = NULL;
   newLayer->featureMap                 = NULL;
   newLayer->delta                      = NULL;
   newLayer->phase                      = cnn->params.phase;
   newLayer->allocatorLevel             = cnn->params.allocatorLevel;

   // link the list
   newLayer->cnn        = cnn;
   newLayer->id         = prev->id + 1;
   newLayer->prev       = prev;
   newLayer->prev->next = newLayer;
   newLayer->next       = NULL;
   cnn->tail            = newLayer;
   cnn->tail->type      = CNN_Dropout;

   return err;

}
