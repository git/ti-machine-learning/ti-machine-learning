/******************************************************************************/
/*!
 * \file timlCNNReadFromFile.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/*******************************************************************************/
/*!
 * \ingroup   cnn
 * \brief     Read CNN from file(s)
 * \param[in] fileName File name
 * \param[in] deviceId Device Id
 * \return    CNN
 */
/*******************************************************************************/

timlConvNeuralNetwork* timlCNNReadFromFile(const char *fileName)
{
   int                   l;
   int                   layerNum;
   int                   intBuffer;
   int                   err;
   int                   read;
   timlCNNLayer          *layer;
   timlCNNLayerType      layerType;
   timlConvNeuralNetwork *cnn;
   char                  str[TIML_UTIL_MAX_STR];
   FILE                  *fp1;
   char                  *dirName;

   err = 0;
   // create cnn
   cnn = timlCNNCreateConvNeuralNetwork(timlCNNTrainingParamsDefault());
   if (cnn == NULL) {
      return NULL;
   }

   // read config file
   strcpy(cnn->configFileName, fileName);
   fp1 = fopen(fileName, "rt");
   if (fp1 == NULL) {
      return NULL;
   }
   dirName = dirname(strdup(fileName));

   // read params file
   read = fscanf(fp1, "paramsBinaryFileName = '%[^']';\n", str);
   sprintf(cnn->paramsFileName, "%s/%s", dirName, str);

   // read states file
   read = fscanf(fp1, "stateBinaryFileName = '%[^']';\n", str);
   sprintf(cnn->statesFileName, "%s/%s", dirName, str);

   // read training parameters
   err = timlCNNTrainingParamsReadFromTextFile(fp1, cnn);
   if (err) {
      timlCNNDelete(cnn);
      return NULL;
   }

   // read layer number
   read = fscanf(fp1, "layerNum = %d;\n", &layerNum);
   if (read != 1) {
      timlCNNDelete(cnn);
      return NULL;
   }

   // read each layer
   for (l = 0; l < layerNum; l++) {
      read = fscanf(fp1, "%[^.].layer(%d).id = %d;\n", (char*)&str, &intBuffer, &intBuffer);
      read = fscanf(fp1, "%[^.].layer(%d).type = %d;\n", (char*)&str, &intBuffer, (int*)&layerType);
      switch (layerType) {
         case CNN_Accuracy:
            err = timlCNNAccuracyReadFromTextFile(fp1, cnn);
            if (err) {
               timlCNNDelete(cnn);
               return NULL;
            }
            break;
         case CNN_SoftmaxCost:
            err = timlCNNSoftmaxCostReadFromTextFile(fp1, cnn);
            if (err) {
               timlCNNDelete(cnn);
               return NULL;
            }
            break;
         case CNN_Softmax:
            err = timlCNNSoftmaxReadFromTextFile(fp1, cnn);
            if (err) {
               timlCNNDelete(cnn);
               return NULL;
            }
            break;
         case CNN_Dropout:
            err = timlCNNDropoutReadFromTextFile(fp1, cnn);
            if (err) {
               timlCNNDelete(cnn);
               return NULL;
            }
            break;
         case CNN_Input:
            err = timlCNNInputReadFromTextFile(fp1, cnn);
            if (err) {
               timlCNNDelete(cnn);
               return NULL;
            }
            break;
         case CNN_Conv:
            err = timlCNNConvReadFromTextFile(fp1, cnn);
            if (err) {
               timlCNNDelete(cnn);
               return NULL;
            }
            break;
         case CNN_Linear:
            err = timlCNNLinearReadFromTextFile(fp1, cnn);
            if (err) {
               timlCNNDelete(cnn);
               return NULL;
            }
            break;
         case CNN_Nonlinear:
            err = timlCNNNonlinearReadFromTextFile(fp1, cnn);
            if (err) {
               timlCNNDelete(cnn);
               return NULL;
            }
            break;
         case CNN_Norm:
            err = timlCNNNormReadFromTextFile(fp1, cnn);
            if (err) {
               timlCNNDelete(cnn);
               return NULL;
            }
            break;
         case CNN_Pooling:
            err = timlCNNPoolingReadFromTextFile(fp1, cnn);
            if (err) {
               timlCNNDelete(cnn);
               return NULL;
            }
            break;
         default:
            break;
      }
   }
   return cnn;
}
