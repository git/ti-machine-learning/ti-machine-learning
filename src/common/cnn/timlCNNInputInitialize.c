/******************************************************************************/
/*!
 * \file timlCNNInputInitialize.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup   cnn
 * \brief     Initialize the input layer
 * \param[in] layer Input layer
 * \return    Error code
 */
/******************************************************************************/

int timlCNNInputInitialize(timlCNNLayer *layer)
{
   timlConvNeuralNetwork *cnn = layer->cnn;
   int deviceId = layer->cnn->deviceId;
   int threadId = layer->cnn->threadId;
   int dim;
   int dataSize = sizeof(float);
   char *offset;

   // params memory
   // mean
   if (layer->inputParams.shared == false) {
      if (timlUtilMallocAcc((void**) &(layer->inputParams.mean), sizeof(float)*layer->inputParams.row*layer->inputParams.col*layer->inputParams.channel) != 0) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }
      timlUtilVectorResetFloat(layer->inputParams.mean, layer->inputParams.row*layer->inputParams.col*layer->inputParams.channel, 0.0, deviceId, threadId);
   }

   // level 1 and 2
   if (layer->allocatorLevel == Util_AllocatorLevel1 || layer->allocatorLevel == Util_AllocatorLevel2) {
      // feature map
      if (timlUtilMallocAcc((void**) &(layer->featureMap), sizeof(float)*layer->maxBatchSize*layer->row*layer->col*layer->channel) != 0) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }
      // input data
      if (timlUtilMallocAcc((void**) &(layer->inputParams.inputData), sizeof(float)*layer->maxBatchSize*layer->inputParams.row*layer->inputParams.col*layer->inputParams.channel) != 0) {
         return ERROR_CNN_LAYER_ALLOCATION;
      }
      timlUtilVectorResetFloat(layer->inputParams.inputData, layer->maxBatchSize*layer->inputParams.row*layer->inputParams.col*layer->inputParams.channel, 0.0, deviceId, threadId);
   }

   // level 3
   if (layer->allocatorLevel == Util_AllocatorLevel3) {
      if (layer->allocatorLevel == Util_AllocatorLevel3) {
         layer->featureMap = cnn->memPool; // allocate at the top
         layer->memPoolPos = Util_MemPoolTop;
         offset = cnn->memPool + cnn->memPoolSize - dataSize*layer->maxBatchSize*layer->inputParams.row*layer->inputParams.col*layer->inputParams.channel;
         layer->inputParams.inputData = offset;
      }
   }

   return 0;
}

