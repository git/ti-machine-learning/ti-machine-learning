/******************************************************************************/
/*!
 * \file timlCNNFreeConvLayer.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Free conv layer
 * \param[in]     layer Layer ptr
 * \return        Error code
 */
/******************************************************************************/

int timlCNNFreeConvLayer(timlCNNLayer *layer)
{

   // free params memory
   if (layer->convParams.shared == false) {
      if (layer->convParams.prevFeatureMapReshapeIndex != NULL) {
         timlUtilFreeAcc(layer->convParams.prevFeatureMapReshapeIndex);
         layer->convParams.prevFeatureMapReshapeIndex = NULL;
      }

      if (layer->convParams.biasMultiplier != NULL) {
         timlUtilFreeAcc(layer->convParams.biasMultiplier);
         layer->convParams.biasMultiplier = NULL;
      }
      if (layer->convParams.kernel != NULL) {
         timlUtilFreeAcc(layer->convParams.kernel);
         layer->convParams.kernel = NULL;
      }
      if (layer->convParams.kernelInc != NULL) {
         timlUtilFreeAcc(layer->convParams.kernelInc);
         layer->convParams.kernelInc = NULL;
      }
      if (layer->convParams.kernelGradAccum != NULL) {
         timlUtilFreeAcc(layer->convParams.kernelGradAccum);
         layer->convParams.kernelGradAccum = NULL;
      }
      if (layer->convParams.bias != NULL) {
         timlUtilFreeAcc(layer->convParams.bias);
         layer->convParams.bias = NULL;
      }
      if (layer->convParams.biasInc != NULL) {
         timlUtilFreeAcc(layer->convParams.biasInc);
         layer->convParams.biasInc = NULL;
      }
      if (layer->convParams.biasGradAccum != NULL) {
         timlUtilFreeAcc(layer->convParams.biasGradAccum);
         layer->convParams.biasGradAccum = NULL;
      }
   }

   // free level 1 or level 2
   if (layer->allocatorLevel != Util_AllocatorLevel3) {

      if (layer->featureMap != NULL) {
         timlUtilFreeAcc(layer->featureMap);
         layer->featureMap = NULL;
      }

      if (layer->delta != NULL) {
         timlUtilFreeAcc(layer->delta);
         layer->delta = NULL;
      }

      if (layer->convParams.prevFeatureMapReshape != NULL) {
         timlUtilFreeAcc(layer->convParams.prevFeatureMapReshape);
         layer->convParams.prevFeatureMapReshape = NULL;
      }
   }


   return 0;

}
