/******************************************************************************/
/*!
 * \file timlCNNAddPoolingLayer.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/*******************************************************************************/
/**
 * \ingroup       cnn
 * \brief         Add pooling layer
 * \param[in,out] cnn      CNN
 * \param[in]     scaleRow Scale kernel row size
 * \param[in]     scaleCol Scale kernel col size
 * \param[in]     strideX  Scale kernel horizontal stride size
 * \param[in]     strideY  Scale kernel vertical stride size
 * \param[in]     type     Pooling type (max/mean)
 * \param[in]     params   Optional parameters
 * \return        Error code
 */
/*******************************************************************************/

int timlCNNAddPoolingLayer(timlConvNeuralNetwork *cnn, int scaleRow, int scaleCol, int strideX, int strideY, timlCNNPoolingType type, timlCNNPoolingParams params)
{
   timlCNNLayer *prev;
   timlCNNLayer *newLayer;
   int          padUp;
   int          padDown;
   int          padLeft;
   int          padRight;

   // init
   padUp    = params.padUp;
   padDown  = params.padDown;
   padLeft  = params.padLeft;
   padRight = params.padRight;

   // error checking
   if (cnn == NULL) {
      return ERROR_CNN_NULL_PTR;
   }
   if (cnn->tail == NULL) {
      return ERROR_CNN_EMPTY;
   }
   if (scaleRow <= 0 || scaleCol <= 0) {
      return ERROR_CNN_POOLING_LAYER_SCALE_SIZE;
   }
   if (padLeft >= scaleCol || padLeft < 0 || padRight >= scaleCol || padRight < 0 || padUp < 0 || padUp >= scaleRow || padDown >= scaleRow || padDown < 0) {
      return ERROR_CNN_POOLING_LAYER_PAD_SIZE;
   }
   if (strideX <= 0 || strideY <= 0) {
      return ERROR_CNN_POOLING_LAYER_STRIDE_SIZE;
   }

   if (timlUtilMallocHost((void**)&newLayer, sizeof(timlCNNLayer))) {
      return ERROR_CNN_LAYER_ALLOCATION;
   }

//   newLayer->inputParams     = timlCNNInputParamsDefault();
//   newLayer->convParams      = timlCNNConvParamsDefault();
//   newLayer->linearParams    = timlCNNLinearParamsDefault();
//   newLayer->normParams      = timlCNNNormParamsDefault();
//   newLayer->nonlinearParams = timlCNNNonlinearParamsDefault();

   prev                             = cnn->tail;
   newLayer->poolingParams          = params;
   // override
   newLayer->poolingParams.maxIndex = NULL;
   newLayer->poolingParams.scaleRow = scaleRow;
   newLayer->poolingParams.scaleCol = scaleCol;
   newLayer->poolingParams.strideX  = strideX;
   newLayer->poolingParams.strideY  = strideY;
   newLayer->poolingParams.type     = type;

   newLayer->channel = prev->channel;
   newLayer->row     = (int) ceilf((float)(prev->row - scaleRow + padUp + padDown)/(float)strideY) + 1;
   newLayer->col     = (int) ceilf((prev->col - scaleCol + padLeft + padRight)/(float) strideX) + 1;
   if (newLayer->row < 1 || newLayer->col < 1) {
      return ERROR_CNN_FEATURE_MAP_SIZE;
   }
   newLayer->batchSize      = prev->batchSize;
   newLayer->maxBatchSize   = prev->maxBatchSize;
   newLayer->phase          = cnn->params.phase;
   newLayer->allocatorLevel = cnn->params.allocatorLevel;
   newLayer->featureMap     = NULL;
   newLayer->delta          = NULL;

   // link the list
   newLayer->cnn        = cnn;
   newLayer->id         = prev->id + 1;
   newLayer->prev       = prev;
   newLayer->prev->next = newLayer;
   newLayer->next       = NULL;
   cnn->tail            = newLayer;
   cnn->tail->type      = CNN_Pooling;

   return 0;
}
