/******************************************************************************/
/*!
 * \file timlCNNSupervisedTrainingWithLabelTeamModeOpenMP.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup cnn
 * \brief supervised training with label using openmp
 * \param[in] cnnTeam     CNN team
 * \param[in] teamNum     CNN team number
 * \param[in] data        data batch
 * \param[in] label       label batch
 * \param[in] dataDim     Data dimension
 * \param[in] labelDim    Label dimension
 * \param[in] batchUpdate data batch size
 * \return error code
 */
/******************************************************************************/

int timlCNNSupervisedTrainingWithLabelTeamModeOpenMP(timlConvNeuralNetwork **cnnTeam, int teamNum, float *image, int row, int col, int channel, int *label, int labelRow, int labelCol, int batchUpdate)
{
   int          i;
   int          t;
   int          thread;
   int          err;
   int          iter;
   float        cost;
   int          cnnBatchSize;
   float        batchCost;
   int          dataDim;
   int          labelDim;
   timlConvNeuralNetwork *cnn;

   err    = 0;
   cnn = cnnTeam[0];
   cnnBatchSize = cnn->params.batchSize;
   thread = omp_get_max_threads();
   iter = batchUpdate/cnnBatchSize;
   batchCost = 0;
   dataDim      = row*col*channel;
   labelDim     = labelRow*labelCol;

   if (thread > teamNum) { // more thread than cnn copies
      thread = teamNum;
   }

   #pragma omp parallel num_threads(thread) private(t, i, err, cost)
   {
      #pragma omp for ordered reduction(+:batchCost)
      for (i = 0; i < iter; i++) {
         t = omp_get_thread_num(); // get thread id
         err = timlCNNLoadImage(cnnTeam[t], image + i*cnnBatchSize*dataDim, row, col, channel, cnnBatchSize);
         err = timlCNNLoadLabel(cnnTeam[t], label + i*cnnBatchSize*labelDim, labelRow, labelCol, cnnBatchSize);
         err = timlCNNForwardPropagation(cnnTeam[t]);
         err = timlCNNBackPropagation(cnnTeam[t]);
         timlUtilBLASsasum(cnnBatchSize*labelDim, cnnTeam[t]->tail->softmaxCostParams.cost, &cost, cnnTeam[t]->deviceId, cnnTeam[t]->threadId);
         batchCost += cost;
      }
   }

   // update params
   timlCNNUpdateParams(cnn);
   batchCost = batchCost/(float)batchUpdate;
   printf("batch cost = %f\n", batchCost);
   return err;
}
