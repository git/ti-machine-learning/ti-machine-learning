/******************************************************************************/
/*!
 * \file timlCNNConvForwardPropagation.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Forward propagate form layer to layer->next
 * \param[in]     prevLayer Previous layer ptr
 * \return        Error code
 */
/******************************************************************************/

int timlCNNConvForwardPropagation(timlCNNLayer *prevLayer)
{
   int          err;
   int          M;
   int          N;
   int          K;
   timlCNNLayer *layer;
   int          prevFeatureMapRow;
   int          prevFeatureMapCol;
   int          prevFeatureMapChannel;
   int          featureMapRow;
   int          featureMapCol;
   int          featureMapChannel;
   int          kernelRow;
   int          kernelCol;
   int          deviceId;
   int          threadId;
   int          b;

   // init
   err                   = 0;
   layer                 = prevLayer->next;
   prevFeatureMapRow     = prevLayer->row;
   prevFeatureMapCol     = prevLayer->col;
   prevFeatureMapChannel = prevLayer->channel;
   featureMapRow         = layer->row;
   featureMapCol         = layer->col;
   featureMapChannel     = layer->channel;
   kernelRow             = layer->convParams.kernelRow;
   kernelCol             = layer->convParams.kernelCol;
   deviceId              = prevLayer->cnn->deviceId;
   threadId              = prevLayer->cnn->threadId;

   M                     = featureMapChannel;
   K                     = kernelRow*kernelCol*prevFeatureMapChannel;
   N                     = featureMapRow*featureMapCol;

   char str[100];
   FILE *fp;

   // featureMap = kernel * prevFeatureMapReshape
   for (b = 0; b < layer->batchSize; b++) {
      timlUtilConv2ImageReshape(layer->convParams.prevFeatureMapReshape, prevLayer->featureMap + b*prevFeatureMapRow*prevFeatureMapCol*prevFeatureMapChannel, layer->convParams.prevFeatureMapReshapeIndex, prevFeatureMapChannel, prevFeatureMapRow*prevFeatureMapCol, kernelRow*kernelCol*featureMapRow*featureMapCol, deviceId, threadId);
      timlUtilBLASsgemm(CblasNoTrans, CblasNoTrans, M, N, K, 1.0, layer->convParams.kernel, layer->convParams.prevFeatureMapReshape, 0.0, layer->featureMap + b*M*N, deviceId, threadId);
      timlUtilBLASsgemm(CblasNoTrans, CblasNoTrans, M, N, 1, 1.0, layer->convParams.bias, layer->convParams.biasMultiplier, 1.0, layer->featureMap + b*M*N, deviceId, threadId);
   }

   return err;
}
