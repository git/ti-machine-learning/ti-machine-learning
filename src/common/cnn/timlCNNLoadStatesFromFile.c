/******************************************************************************/
/*!
 * \file timlCNNLoadStatesFromFile.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Load the params to the CNN
 * \param[in]     cnn   CNN
 * \return        Error code
 */
/******************************************************************************/

#include "timlCNN.h"

int timlCNNLoadStatesFromFile(timlConvNeuralNetwork *cnn, const char *fileName)
{
   timlCNNLayer *layer;
   int err;
   // read binary file
   FILE *fp3 = fopen(fileName, "rb");
   if (fp3 == NULL) {
      return 0;
   }

   // allocator level 3
   if (cnn->params.allocatorLevel == Util_AllocatorLevel3) {
      timlUtilFread(cnn->memPool, sizeof(char), cnn->memPoolSize, fp3);
      fclose(fp3);
      return 0;
   }

   layer = cnn->head;
   while (layer != NULL) {
      switch (layer->type) {
      case CNN_Accuracy:
         err = timlCNNAccuracyReadFromStatesBinaryFile(fp3, layer);
         break;
      case CNN_SoftmaxCost:
         err = timlCNNSoftmaxCostReadFromStatesBinaryFile(fp3, layer);
         break;
      case CNN_Softmax:
         err = timlCNNSoftmaxReadFromStatesBinaryFile(fp3, layer);
         break;
      case CNN_Dropout:
         err = timlCNNDropoutReadFromStatesBinaryFile(fp3, layer);
         break;
      case CNN_Input:
         err = timlCNNInputReadFromStatesBinaryFile(fp3, layer);
         break;
      case CNN_Conv:
         err = timlCNNConvReadFromStatesBinaryFile(fp3, layer);
         break;
      case CNN_Linear:
         err = timlCNNLinearReadFromStatesBinaryFile(fp3, layer);
         break;
      case CNN_Nonlinear:
         err = timlCNNNonlinearReadFromStatesBinaryFile(fp3, layer);
         break;
      case CNN_Norm:
         err = timlCNNNormReadFromStatesBinaryFile(fp3, layer);
         break;
      case CNN_Pooling:
         err = timlCNNPoolingReadFromStatesBinaryFile(fp3, layer);
         break;
      default:
         break;
      }
      layer = layer->next;
   }
   fclose(fp3);
   return 0;
}


