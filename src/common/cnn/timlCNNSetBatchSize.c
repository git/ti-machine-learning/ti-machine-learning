/*
 * timlCNNSetBatchSize.c
 *
 *  Created on: Apr 9, 2015
 *      Author: a0282871
 */


#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Reset the parameters of the CNN
 * \param[in,out] cnn CNN
 * \return        Error code
 */
/******************************************************************************/

int timlCNNSetBatchSize(timlConvNeuralNetwork *cnn, int batchSize)
{
   if (cnn == NULL) {
      return ERROR_CNN_NULL_PTR;
   }
   cnn->params.batchSize = batchSize;
   timlCNNLayer *layer = cnn->head;

   while (layer != NULL) {
      switch (layer->type) {
         case CNN_Input:
            layer->batchSize = batchSize;
            break;
         case CNN_Dropout:
            layer->batchSize = layer->prev->batchSize;
            break;
         case CNN_Conv:
            layer->batchSize = layer->prev->batchSize;
            break;
         case CNN_Norm:
            layer->batchSize = layer->prev->batchSize;
            break;
         case CNN_Pooling:
            layer->batchSize = layer->prev->batchSize;
            break;
         case CNN_Linear:
            layer->batchSize = layer->prev->batchSize;
            break;
         case CNN_Nonlinear:
            layer->batchSize = layer->prev->batchSize;
            break;
         case CNN_Accuracy:
            layer->batchSize = layer->prev->batchSize;
            break;
         case CNN_SoftmaxCost:
            layer->batchSize = layer->prev->batchSize;
            break;
         case CNN_Softmax:
            layer->batchSize = layer->prev->batchSize;
            break;
         default:
            break;
      }
      layer = layer->next;
   }

   return 0;
}
