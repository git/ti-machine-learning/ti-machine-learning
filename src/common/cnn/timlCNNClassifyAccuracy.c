/*****************************************************************************/
/*!
 * \file timlCNNClassifyAccuracy.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup       cnn
 * \brief         Batch classification
 * \param[in,out] cnn       CNN
 * \param[in]     data      Data batch
 * \param[in]     dataDim   Data dimension
 * \param[in]     num       Data number
 * \param[out]    label     Label array ptr
 * \param[out]    labelDim  Label dimension
 * \param[out]    success   Number of successful classification
 * \return        Error code
 */
/******************************************************************************/

int timlCNNClassifyAccuracy(timlConvNeuralNetwork *cnn, float *image, int row, int col, int channel, int *label, int labelRow, int labelCol, int num, int *success)
{
   int i;
   int batchSize;
   int batchNum;
   int dataDim;
   int labelDim;
   int err;

   // error checking
   if (cnn->head->inputParams.row != row || cnn->head->inputParams.col != col || cnn->head->inputParams.channel != channel) {
      return ERROR_CNN_IMAGE_DIM_MISMATCH;
   }
   if (cnn->tail->row != labelRow || cnn->tail->col != labelCol ) {
      return ERROR_CNN_LABEL_DIM_MISMATCH;
   }
   if (cnn->tail->type != CNN_Accuracy) {
      return ERROR_CNN_CLASS;
   }

   err = 0;
   *success = 0;
   batchSize = cnn->params.batchSize;
   batchNum = num/batchSize;
   dataDim = row*col*channel;
   labelDim = labelRow*labelCol;

   for (i = 0; i < batchNum; i++) {
      err = timlCNNLoadImage(cnn, image + i*dataDim*batchSize, row, col, channel, batchSize);
      if (label != NULL) {
         err = timlCNNLoadLabel(cnn, label + i*labelDim*batchSize, labelRow, labelCol, batchSize);
      }
      err = timlCNNForwardPropagation(cnn);
      *success += cnn->tail->accuracyParams.success;
   }

   return err;
}
