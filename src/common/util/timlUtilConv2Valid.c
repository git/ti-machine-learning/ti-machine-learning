/******************************************************************************/
/*!
 * \file timlUtilConv2Valid.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup    util
 * \brief      conv2(a, b, 'valid')
 * \param[in]  a    a matrix
 * \param[in]  b    b matrix
 * \param[out] c    c = conv2(a, b, 'valid')
 * \param[in]  aRow a row size
 * \param[in]  aCol a col size
 * \param[in]  bRow b row size
 * \param[in]  bCol b col size
 * \return     Error code
 */
/******************************************************************************/

int timlUtilConv2Valid(float *a, float *b, float *c, int aRow, int aCol, int bRow, int bCol)
{
   int i;
   int j;
   int m;
   int n;
   float sum;
   int cRow = aRow - bRow + 1;
   int cCol = aCol - bCol + 1;
   for (m = 0; m < cRow; m++) {
      for (n = 0; n < cCol; n++) {
         sum = 0.0;
         for (i = 0; i < bRow; i++) {
            for (j = 0; j < bCol; j++) {
               sum += a[(bCol - 1 + n - j) + aCol*(bRow - 1 + m - i)]*b[bCol*i + j];
            }
         }
         c[n + cCol*m] += sum;
      }
   }
   return 0;
}
