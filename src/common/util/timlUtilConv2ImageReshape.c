/******************************************************************************/
/*!
 * \file timlUtilConv2ImageReshape.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"
#ifdef TIML_CPU_ALT
#include "../../alt/timlAlt.h"
#elif defined TIML_ARM_DSP
#include "../../dsp/timlDSP.h"
#endif


/******************************************************************************/
/*!
 * \ingroup    util
 * \brief      Reshape feature maps to a format that turns 2d convolution to GEMM operation
 * \param[out] xReshape Reshaped feature map
 * \param[in]  x        Feature map
 * \param[in]  index    Reshaping index matrix
 * \param[in]  xNum     The number of feature maps
 * \param[in]  xDim     Dimension of the feature map (row*col)
 * \param[in]  indexDim Dimension of the index matrix
 * \param[in]  deviceId Device id
 * \param[in]  threadId Thread id
 * \return     Error code
 */
/******************************************************************************/

int timlUtilConv2ImageReshape(float* xReshape, float* x, int *index, int xNum, int xDim, int indexDim, int deviceId, int threadId)
{
#ifdef TIML_CPU_ALT
   return timlUtilConv2ImageReshapeAlt(xReshape, x, index, xNum, xDim, indexDim, deviceId, threadId);
#else
   int j;
   int p;
   for (j = 0; j < xNum; j++) {
      for (p = 0; p < indexDim; p++) {
         if (index[p] != -1) {
            xReshape[p + j*indexDim] = x[index[p] + j*xDim];
         }
         else {
            xReshape[p + j*indexDim] = 0.0;
         }
      }
   }
   return 0;

#endif
}
