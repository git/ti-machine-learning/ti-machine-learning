/*
 * timlUtilWriteMatrix.c
 *
 *  Created on: Apr 17, 2015
 *      Author: eric
 */



#include "../api/timl.h"
#ifdef TIML_CPU_ALT
#include "../../alt/timlAlt.h"
#elif defined TIML_ARM_DSP
#include "../../dsp/timlDSP.h"
#endif

/******************************************************************************/
/*!
 * \ingroup    util
 * \brief      Write to a binar file
 * \param[out] ptr   Memory pointer
 * \param[in]  size  Array size
 * \param[in]  nmemb Array element size
 * \param[in]  fp    File pointer
 * \return     Number of successfully written elements
 */
/******************************************************************************/

int timlUtilWriteMatrixFloat(const float *ptr, int m, int n, FILE *fp, const char *format)
{
#ifdef TIML_CPU_ALT
   return timlUtilWriteMatrixFloatAlt(ptr, m, n, fp, format);
#else
   int i;
   int j;
   for (i = 0; i < m; i++) {
      for (j = 0; j < n; j++) {
         fprintf(fp, format, ptr[i*n + j]);
         fprintf(fp, " ");
      }
      fprintf(fp, "\n");
   }
   return 0;

#endif
}
