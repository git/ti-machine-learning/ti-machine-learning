/******************************************************************************/
/*!
 * \file timlUtilMeanPooling.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"
#ifdef TIML_CPU_ALT
#include "../../alt/timlAlt.h"
#elif defined TIML_ARM_DSP
#include "../../dsp/timlDSP.h"
#endif


/*******************************************************************************/
/*!
 * \ingroup    util
 * \brief      Mean pooling
 * \param[out] outputMap Output feature map
 * \param[in]  inputMap  Input feature map
 * \param[in]  row       Output feature map row
 * \param[in]  col       Output feature map col
 * \param[in]  channel   Output feature map channel
 * \param[in]  batchSize Output feature map batch size
 * \param[in]  prevRow   Previous feature map row
 * \param[in]  prevCol   Previous feature map col
 * \param[in]  scaleRow  Scaling window row size
 * \param[in]  scaleCol  Scaling window col size
 * \param[in]  padUp     Upper border padding for the input feature map
 * \param[in]  padLeft   Left border padding for the input feature map
 * \param[in]  strideX   Window stride in x direction
 * \param[in]  strideY   Window stride in y direction
 * \param[in]  deviceId  Device id
 * \param[in]  threadId  Thread id
 * \return     Error code
 */
/*******************************************************************************/

int timlUtilMeanPooling(float *outputMap, float *inputMap, int row, int col, int channel, int batchSize, int prevRow, int prevCol, int scaleRow, int scaleCol, int padUp, int padLeft, int strideX, int strideY, int deviceId, int threadId)
{
#ifdef TIML_CPU_ALT
   return timlUtilMeanPoolingAlt(outputMap, inputMap,
      row, col, channel, batchSize, prevRow, prevCol,
      scaleRow, scaleCol, padUp, padLeft, strideX, strideY, deviceId, threadId);
#else

   int startRow;
   int endRow;
   int startCol;
   int endCol;
   int n;
   int p;
   int q;
   int i;
   int j;
   int b;
   int inputIndex;
   int outputIndex;
   float factor;
   for(b = 0; b < batchSize; b++) {
      for (n = 0; n < channel; n++) {
         for (p = 0; p < row; p++) {
            for (q = 0; q < col; q++) {
               startRow = p*strideY - padUp;
               startCol = q*strideX - padLeft;
               endRow = startRow + scaleRow;
               endCol = startCol + scaleCol;
               if (startRow < 0) {
                  startRow = 0;
               }
               if (startCol < 0) {
                  startCol = 0;
               }
               if (endRow > prevRow) {
                  endRow = prevRow;
               }
               if (endCol > prevCol) {
                  endCol = prevCol;
               }
               factor = 1.0/((endRow - startRow)*(endCol - startCol));
               outputIndex = b*row*col*channel + n*row*col + p*col + q;
               for (i = startRow; i < endRow; i++) {
                  for (j = startCol; j < endCol; j++) {
                     inputIndex = b*prevRow*prevCol*channel + n*prevRow*prevCol + i*prevCol + j;
                     outputMap[outputIndex] += factor*inputMap[inputIndex];
                  }
               }
            }
         }
      }
   }
   return 0;

#endif
}
