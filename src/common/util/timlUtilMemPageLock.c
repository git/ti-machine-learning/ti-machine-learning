/*
 * timlUtilMemPageLock.c
 *
 *  Created on: Apr 21, 2015
 *      Author: eric
 */




#include "../api/timl.h"
#ifdef TIML_CPU_ALT
#include "../../alt/timlAlt.h"
#elif defined TIML_ARM_DSP
#include "../../dsp/timlDSP.h"
#endif


/******************************************************************************/
/*!
 * \ingroup util
 * \brief memory copy
 * \param[out] dest Dest ptr
 * \param[in] src  Src ptr
 * \param[in] size Size in byte
 * \return error code
 */
/******************************************************************************/

int timlUtilMemPageLock(void *ptr, size_t size)
{
#ifdef TIML_CPU_ALT
   return timlUtilMemPageLockAlt(ptr, size);
#else
   return 0;
#endif
}
