/******************************************************************************/
/*!
 * \file timlUtilMultinomialCrossEntropy.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"
#ifdef TIML_CPU_ALT
#include "../../alt/timlAlt.h"
#elif defined TIML_ARM_DSP
#include "../../dsp/timlDSP.h"
#endif


/******************************************************************************/
/*!
 * \ingroup   util
 * \brief     Calculate the mutlinomial cross entropy between x and label
 * \param[in] x     Input data
 * \param[in] label Label
 * \param[in] n     Data size
 * \param[in] inc   Label array increment
 * \return    Cross entropy
 */
/******************************************************************************/

float timlUtilMultinomialCrossEntropy1D(float *data, int label, int n, int inc)
{
   // label start from 0
   int   i;
   float logVal;
   // check for NAN
   for (i = 0; i < n; i++) {
      logVal = logf(data[i*inc]);
      if (isnan(logVal)) {
         return NAN;
      }
   }
   // check for INF
   logVal = logf(data[label*inc]);
   if (isinf(logVal)) {
      return +INFINITY;
   }
   else {
      return -logVal;
   }
}

/******************************************************************************/
/*!
 * \ingroup   util
 * \brief     Calculate the mutlinomial cross entropy between the feature maps and labels
 * \param[in]  featureMap Feature map
 * \param[in]  label      Label
 * \param[out] cost       Cost ptr
 * \param[in]  row        Row
 * \param[in]  col        Col
 * \param[in]  channel    Channel
 * \param[in]  batchSize  Batch size
 * \param[in]  deviceId   Device Id
 * \param[in]  threadId   Thread Id
 * \return     Error code
 */
/******************************************************************************/

int timlUtilMultinomialCrossEntropy(float *featureMap, int *label, float *cost, int row, int col, int channel, int batchSize, int deviceId, int threadId)
{
   // label start from 0
#ifdef TIML_CPU_ALT
   return timlUtilMultinomialCrossEntropyAlt(featureMap, label, cost, row, col, channel, batchSize, deviceId, threadId);
#else
   int r;
   int c;
   int dataOffset;
   int labelIndex;
   int b;
   for (b = 0; b < batchSize; b++) {
      for (r = 0; r < row; r++) {
         for (c = 0; c < col; c++) {
            dataOffset = b*row*col*channel + r*col + c;
            labelIndex = b*row*col + r*col + c;
            cost[labelIndex] = timlUtilMultinomialCrossEntropy1D(featureMap + dataOffset, label[labelIndex], channel, row*col);
         }
      }
   }
   return 0;

#endif
}

