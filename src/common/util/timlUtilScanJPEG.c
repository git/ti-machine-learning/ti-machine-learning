/******************************************************************************/
/*!
 * \file timlUtilScanJPEG.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"


/******************************************************************************/
/*!
 * \ingroup    util
 * \brief      Return an array of jpg image names in the directory
 * \param[in]  dirName  Directory name
 * \param[out] imageNum Image number
 * \return     Image name array
 */
/******************************************************************************/

char** timlUtilScanJPEG(const char *dirName, int *imageNum)
{
   char **imageNames;
   char *name;
   int  i;
   DIR  *dir;
   int  imageCount;
   struct dirent *ent;

   imageCount = 0;
   // count image number
   if ((dir = opendir(dirName)) != NULL) {
      while ((ent = readdir(dir)) != NULL) {
         if (strstr(ent->d_name, ".jpg") != NULL)
            imageCount++;
      }
      closedir(dir);
   }
   *imageNum = imageCount;

   // allocate imageNames
   imageNames = malloc(sizeof(char*)*imageCount);
   for (i = 0; i < imageCount; i++) {
      imageNames[i] = malloc(sizeof(char)*TIML_UTIL_MAX_STR);
   }

   // re-scan the directory
   i = 0;
   if ((dir = opendir(dirName)) != NULL) {
      while ((ent = readdir(dir)) != NULL) {
         if (strstr(ent->d_name, ".jpg") != NULL) {
            name = strtok(ent->d_name, ".");
            strcpy(imageNames[i], name);
            i++;
         }
      }
      closedir(dir);
   }

   return imageNames;
}
