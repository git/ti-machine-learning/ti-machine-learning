/******************************************************************************/
/*!
 * \file timlUtilTransform.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../api/timl.h"
#ifdef TIML_CPU_ALT
#include "../../alt/timlAlt.h"
#elif defined TIML_ARM_DSP
#include "../../dsp/timlDSP.h"
#endif


/******************************************************************************/
/*!
 * \ingroup util
 * \brief      Transform the raw input data with preprocessing
 * \param[out] dataOut    Output data, i.e. the input feature map
 * \param[out] dataIn     A copy of the input data
 * \param[in]  dataHost   Input data
 * \param[in]  channel    Input feature map channel
 * \param[in]  row        Input feature map row
 * \param[in]  col        Input feature map col
 * \param[in]  rowOffset  Row offset with regard to the raw input data
 * \param[in]  colOffset  Col offset with regard to the raw input data
 * \param[in]  rowIn      Raw input data row
 * \param[in]  colIn      Raw input data col
 * \param[in]  scale      Scaling factor
 * \param[in]  mean       Input data mean
 * \param[in]  mirrorType Whether to mirror the raw input data
 * \param[in]  deviceId   Device id
 * \param[in]  threadId   Thread id
 * \return     Error code
 */
/******************************************************************************/

int timlUtilTransform(float *dataOut, float *dataIn, int row, int col, int channel, int batchSize, int rowOffset, int colOffset, int rowIn, int colIn, float scale, float *mean, timlUtilMirrorType mirrorType, int deviceId, int threadId)
{
#ifdef TIML_CPU_ALT
   return timlUtilTransformAlt(dataOut, dataIn, row, col, channel, batchSize, rowOffset, colOffset, rowIn, colIn, scale, mean, mirrorType, deviceId, threadId);
#else
   int k;
   int i;
   int j;
   int b;
   int index;
   int rawIndex;
   for (b = 0; b < batchSize; b++) {
      for (k = 0; k < channel; k++) {
         for (i = 0; i < row; i++) {
            for (j = 0; j < col; j++) {
               switch (mirrorType) {
                  case Util_NoMirror:
                     rawIndex = (i + rowOffset)*colIn + (j + colOffset) + k*rowIn*colIn;
                     break;
                  case Util_RandomMirror:
                     if (timlUtilRandDiscreteUniformRNG(0, 1)) {
                        rawIndex = (i + rowOffset)*colIn + (col - 1 - j + colOffset) + k*rowIn*colIn;
                     }
                     else {
                        rawIndex = (i + rowOffset)*colIn + (j + colOffset) + k*rowIn*colIn;
                     }
                     break;
                  case Util_Mirror:
                     rawIndex = (i + rowOffset)*colIn + (col - 1 - j + colOffset) + k*rowIn*colIn;
                     break;
               }
               index = i*col + j + k*row*col + b*row*col*channel;
               dataOut[index] = scale*(dataIn[rawIndex + b*rowIn*colIn*channel] - mean[rawIndex]);
            }
         }
      }
   }
   return 0;
#endif
}
