/******************************************************************************/
/*!
 * \file testCNNSimpleTrainingOpenMP.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "testCNN.h"


/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define DATABASE_PATH    "./src/database/mnist"
#define BATCH_SIZE       10
#define MAX_BATCH_SIZE   100
#define BATCH_UPDATE     100
#define TEST_NUM         10000
#define TRAIN_NUM        60000
#define IMAGE_ROW        28
#define IMAGE_COL        28
#define IMAGE_CHANNEL    1
#define TOP_N            1


/******************************************************************************/
/*!
 * \ingroup testCNN
 * \brief   Simple training function test
 * \return  Error code
 */
/******************************************************************************/

int testCNNSimpleTrainingOpenMP()
{
   int                   i;
   int                   dim;
   int                   classifyNum;
   float                 classifyPercent;
   int                   label;
   struct timespec       startTime;
   struct timespec       endTime;
   long                  trainingTime;
   long                  testingTime;
   int                   err;
   long                  mem;
   int                   batchNum;
   int                   batchSize;
   int                   thread;
   timlUtilImageSet      training;
   timlUtilImageSet      testing;
   timlCNNInputParams    inputParams;
   timlCNNTrainingParams trainingParams;
   timlConvNeuralNetwork *cnn;
   timlCNNLayer          *layer;

   dim            = IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL;
   classifyNum    = 0;
   err            = 0;

   setbuf(stdout, NULL); // do not buffer the console output

   printf("[Test] CNN simple training\n");
   printf("1. Build up the CNN\n");
   trainingParams              = timlCNNTrainingParamsDefault();
   trainingParams.batchSize    = BATCH_SIZE;
   trainingParams.maxBatchSize = MAX_BATCH_SIZE;
   trainingParams.batchUpdate  = BATCH_UPDATE;
   trainingParams.learningRate = 0.1;
   cnn = timlCNNCreateConvNeuralNetwork(trainingParams);
   inputParams       = timlCNNInputParamsDefault();
   inputParams.scale = 1.0/256.0;
   timlCNNAddInputLayer(cnn, IMAGE_ROW, IMAGE_COL, IMAGE_CHANNEL, inputParams);            // input layer
   timlCNNAddConvLayer(cnn, 5, 5, 1, 1, 6, timlCNNConvParamsDefault());                    // conv layer
   timlCNNAddNonlinearLayer(cnn, Util_Relu);                                               // relu layer
   timlCNNAddPoolingLayer(cnn, 4, 4, 4, 4, CNN_MaxPooling, timlCNNPoolingParamsDefault()); // max pooling layer
   timlCNNAddNormLayer(cnn, timlCNNNormParamsDefault());                                   // norm layer
   timlCNNAddDropoutLayer(cnn, 0.5);                                                       // dropout layer
   timlCNNAddLinearLayer(cnn, 10, timlCNNLinearParamsDefault());                           // linear layer
   timlCNNAddSoftmaxCostLayer(cnn);                                                        // softmax cost layer
   timlCNNInitialize(cnn);
   timlCNNReset(cnn);
   timlCNNPrint(cnn);

   printf("2. Load the MNIST database\n");
   err = timlUtilReadMNIST(DATABASE_PATH, &training, &testing);
   if (err) {
      printf("MNIST database reading error\n");
   }

   // create cnnTeam
   thread = omp_get_max_threads();
   if (thread > BATCH_UPDATE/BATCH_SIZE) {
      thread = BATCH_UPDATE/BATCH_SIZE;
   }
   timlConvNeuralNetwork **cnnTeam = malloc(sizeof(timlConvNeuralNetwork*)*thread);
   cnnTeam[0] = cnn;
   for (i = 1; i < thread; i++) {
      cnnTeam[i] = timlCNNShareParams(cnn);
      timlCNNInitialize(cnnTeam[i]);
   }

   // training
   printf("3. Start training\n");
   timlCNNSetMode(cnn, Util_Train);
   batchNum = TRAIN_NUM/cnn->params.batchUpdate;
   clock_gettime(CLOCK_REALTIME, &startTime);
   for (i = 0; i < batchNum; i++) {
      printf("Batch id = %d, ", i);
      timlCNNSupervisedTrainingWithLabelTeamModeOpenMP(cnnTeam, thread, training.data + i*cnn->params.batchUpdate*dim, IMAGE_ROW, IMAGE_COL, IMAGE_CHANNEL, training.label + i*cnn->params.batchUpdate, 1, 1, cnn->params.batchUpdate);
   }
   clock_gettime(CLOCK_REALTIME, &endTime);
   trainingTime = timlUtilDiffTime(startTime, endTime);
   printf("Training time = %.3f s.\n", trainingTime/1000000.0);

   // testing
   printf("4. Start testing\n");
   // remove softmaxCost layer and add accuracy layer
   for (i = 0; i < thread; i++) {
      timlCNNDeleteLayer(cnnTeam[i]->tail);
      timlCNNAddAccuracyLayer(cnnTeam[i], TOP_N);
      timlCNNAccuracyInitialize(cnnTeam[i]->tail);
      timlCNNSetMode(cnnTeam[i], Util_Test);
   }

   clock_gettime(CLOCK_REALTIME, &startTime);
   timlCNNClassifyAccuracyTeamModeOpenMP(cnnTeam, thread, testing.data, IMAGE_ROW, IMAGE_COL, IMAGE_CHANNEL, testing.label, 1, 1, TEST_NUM, &classifyNum);
   clock_gettime(CLOCK_REALTIME, &endTime);
   testingTime = timlUtilDiffTime(startTime, endTime);
   classifyPercent = (float)classifyNum/(float)testing.num;
   printf("Testing time = %.3f s.\nSuccess percent = %.3f %%.\n", testingTime/1000000.0, classifyPercent*100.0);

   printf("5. Clean up\n");
   free(training.data);
   free(training.label);
   free(training.mean);
   free(testing.data);
   free(testing.label);
   for (i = 0; i < thread; i++) {
      timlCNNDelete(cnnTeam[i]);
   }
   return err;
}
