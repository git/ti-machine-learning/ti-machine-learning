/******************************************************************************/
/*!
 * \file testCNNSimpleShare.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "testCNN.h"


/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define MODEL_PATH         "src/database/test/cnn/databaseTestCNNSimpleShare1.m"
#define SHARED_MODEL_PATH  "src/database/test/cnn/databaseTestCNNSimpleShare2.m"
#define IMAGE_ROW          28
#define IMAGE_COL          28
#define IMAGE_CHANNEL      1
#define INT_FORMAT         "%10d"
#define FLOAT_FORMAT       "%12.4f"
#define CNN_NAME           "cnn"
#define PARAMS_LEVEL       Util_ParamsLevel3


/******************************************************************************/
/*!
 * \ingroup testCNN
 * \brief   Simple share function test
 * \return  Error code
 */
/******************************************************************************/

int testCNNSimpleShare()
{
   timlConvNeuralNetwork *cnn;
   timlConvNeuralNetwork *cnnShare;
   size_t mem1;
   size_t mem2;
   size_t mem3;

   printf("[Test] CNN simple share\n");
   printf("1. Build the CNN\n");
   cnn = timlCNNCreateConvNeuralNetwork(timlCNNTrainingParamsDefault());
   timlCNNAddInputLayer(cnn, IMAGE_ROW, IMAGE_COL, IMAGE_CHANNEL, timlCNNInputParamsDefault()); // input layer
   timlCNNAddConvLayer(cnn, 5, 5, 1, 1, 6, timlCNNConvParamsDefault());                         // conv layer
   timlCNNAddNonlinearLayer(cnn, Util_Sigmoid);                                                 // sigmoid layer
   timlCNNAddPoolingLayer(cnn, 2, 2, 2, 2, CNN_MaxPooling, timlCNNPoolingParamsDefault());      // max pooling layer
   timlCNNAddDropoutLayer(cnn, 0.5);                                                            // dropout layer
   timlCNNAddNormLayer(cnn, timlCNNNormParamsDefault());                                        // norm layer
   timlCNNAddLinearLayer(cnn, 10, timlCNNLinearParamsDefault());                                // linear layer
   timlCNNAddSoftmaxCostLayer(cnn);                                                             // softmax cost layer
   timlCNNInitialize(cnn);
   timlCNNReset(cnn);
   timlCNNPrint(cnn);
   mem1 = cnn->forwardMemory + cnn->backwardMemory + cnn->fixedMemory + cnn->paramsMemory;
   mem2 = cnn->forwardMemory + cnn->fixedMemory + cnn->paramsMemory;
   mem3 = cnn->memPoolSize + cnn->fixedMemory + cnn->paramsMemory;
   printf("CNN level 1 memory size = %d byte.\n", mem1);
   printf("CNN level 2 memory size = %d byte.\n", mem2);
   printf("CNN level 3 memory size = %d byte.\n", mem3);
   printf("CNN forward memory size = %d byte.\n", cnn->forwardMemory);
   printf("CNN memory pool size    = %d byte.\n", cnn->memPoolSize);
   printf("CNN params memory size  = %d byte.\n", cnn->paramsMemory);

   printf("2. Create shared CNN\n");
   cnnShare = timlCNNShareParams(cnn);
   timlCNNInitialize(cnnShare);

   // write to model file
   printf("3. Write to model file\n");
   timlCNNWriteToFile(MODEL_PATH, cnn, PARAMS_LEVEL, CNN_NAME, FLOAT_FORMAT, INT_FORMAT);

   // write to shared model file
   printf("4. Write to shared model file\n");
   timlCNNWriteToFile(SHARED_MODEL_PATH, cnnShare, PARAMS_LEVEL, CNN_NAME, FLOAT_FORMAT, INT_FORMAT);

   timlCNNDelete(cnn);
   timlCNNDelete(cnnShare);

   return 0;
}
