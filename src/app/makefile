################################################################################
#
# makefile
#
# Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#    Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
################################################################################

UNAME_M :=$(shell uname -m)
ifneq (,$(findstring 86, $(UNAME_M)))
    # In a cross compile environment we are assuming that the EVM file system
    # is located on the build host and necessary ARM libraries are installed
    # on that file system.
    ifneq ($(MAKECMDGOALS),clean)
       ifeq ($(TARGET_ROOTDIR),)
         $(error Environment variable TARGET_ROOTDIR must be defined. Set it to point at the EVM root file system)
       endif
    endif

    # gcc ARM cross compiler will not, by default, search the host's
    # /usr/include.  Explicitly specify here to find dependent vendor headers
CC = arm-linux-gnueabihf-gcc
CPPC =  arm-linux-gnueabihf-g++
AR = arm-linux-gnueabihf-ar
else
CC = gcc
CPPC = g++
AR = @ar
endif



C_INCLUDE_PATH = ../common/api ../common/cnn ../common/util \
$(TARGET_ROOTDIR)/usr/include
C_INCLUDE_PATH_FLAG = $(foreach d, $(C_INCLUDE_PATH), -I$d)
C_LIB = timl cblas_armplusdsp blis OpenCL stdc++ rt jpeg m
C_LIB_FLAG = $(foreach d, $(C_LIB), -l$d)
C_LIB_PATH = $(TARGET_ROOTDIR)/usr/lib ../../bin
C_LIB_PATH_FLAG = $(foreach d, $(C_LIB_PATH), -L$d)
CFLAGS += -g -O3 -fopenmp
LD_FLAGS=-L$(TARGET_ROOTDIR)/lib -L$(TARGET_ROOTDIR)/usr/lib -Wl,-rpath-link,$(TARGET_ROOTDIR)/lib -Wl,-rpath-link,$(TARGET_ROOTDIR)/usr/lib

ARFLAGS = -rcs
RM = @rm
RMFLAGS += -fr


# APP CNN CLASS
APP_CNN_CLASS_BIN_CFILES = $(shell find ./cnn/class -name "*.c")
APP_CNN_CLASS_BIN_OBJS =  $(patsubst %.c, %.o, $(APP_CNN_CLASS_BIN_CFILES))
APP_CNN_CLASS_BINS =  $(patsubst %.c, %.bin, $(APP_CNN_CLASS_BIN_CFILES))
APP_CNN_CLASS_HFILES = $(shell find ./cnn/class -name "*.h")

# APP CNN SL
APP_CNN_SL_BIN_HFILES = $(shell find ./cnn/scene -name "*.h")
APP_CNN_SL_BIN_CFILES = $(shell find ./cnn/scene/sbd -name "*.c")
APP_CNN_SL_BIN_OBJS =  $(patsubst %.c, %.o, $(APP_CNN_SL_BIN_CFILES))
APP_CNN_SL_BINS =  $(patsubst %.c, %.bin, $(APP_CNN_SL_BIN_CFILES))
APP_CNN_SL_CFILES = $(shell find ./cnn/scene -name "*.c")
APP_CNN_SL_OBJS = $(patsubst %.c, %.o, $(APP_CNN_SL_CFILES))
APP_CNN_SL_AUX_OBJS = $(filter-out $(APP_CNN_SL_BIN_OBJS), $(APP_CNN_SL_OBJS))

# APP CNN INTEROP CAFFE
APP_CNN_INTEROP_CAFFE_BIN_CFILES = $(shell find ./cnn/interop/caffe -name "*.cpp")
APP_CNN_INTEROP_CAFFE_BIN_OBJS =  $(patsubst %.cpp, %.o, $(APP_CNN_INTEROP_CAFFE_BIN_CFILES))
APP_CNN_INTEROP_CAFFE_BINS = ./cnn/interop/caffe/appCNNInteropCaffe.bin
APP_CNN_INTEROP_CAFFE_HFILES = $(shell find ./cnn/interop/caffe -name "*.hpp")

# APP CNN CONVERT IMAGENET
APP_CNN_CONVERT_IMAGENET_BIN_CFILES = $(shell find ./cnn/convert/imagenet -name "*.cpp")
APP_CNN_CONVERT_IMAGENET_BIN_OBJS =  $(patsubst %.cpp, %.o, $(APP_CNN_CONVERT_IMAGENET_BIN_CFILES))
APP_CNN_CONVERT_IMAGENET_BINS = ./cnn/convert/imagenet/appCNNConvertImageNet.bin
APP_CNN_CONVERT_IMAGENET_HFILES = $(shell find ./cnn/convert/imagenet -name "*.hpp")

# APP CNN CONVERT SBD
APP_CNN_CONVERT_SBD_BIN_CFILES = $(shell find ./cnn/convert/sbd -name "*.cpp")
APP_CNN_CONVERT_SBD_BIN_OBJS =  $(patsubst %.cpp, %.o, $(APP_CNN_CONVERT_SBD_BIN_CFILES))
APP_CNN_CONVERT_SBD_BINS = ./cnn/convert/sbd/appCNNConvertSBD.bin
APP_CNN_CONVERT_SBD_HFILES = $(shell find ./cnn/convert/sbd -name "*.hpp")


APP_CNN_BINS = \
$(APP_CNN_CLASS_BINS) \
$(APP_CNN_SL_BINS) \
$(APP_CNN_CONVERT_IMAGENET_BINS) \
$(APP_CNN_CONVERT_SBD_BINS) \
$(APP_CNN_INTEROP_CAFFE_BINS)

APP_CNN_OBJS = \
$(APP_CNN_CLASS_BIN_OBJS) \
$(APP_CNN_SL_OBJS) \
$(APP_CNN_INTEROP_CAFFE_BIN_OBJS) \
$(APP_CNN_CONVERT_IMAGENET_BIN_OBJS) \
$(APP_CNN_CONVERT_SBD_BIN_OBJS)

all: $(APP_CNN_BINS)

clean:
	$(RM) $(RMFLAGS) \
	$(APP_CNN_OBJS) \
	$(APP_CNN_BINS)

# appCNNClass bins
$(APP_CNN_CLASS_BINS): %.bin: %.o
	$(CC) $(CFLAGS) $(LD_FLAGS) -o $@ $^ \
	$(C_LIB_FLAG) $(C_LIB_PATH_FLAG)
	
# appCNNClass objs
$(APP_CNN_CLASS_BIN_OBJS): %.o: %.c $(APP_CNN_CLASS_HFILES)
	$(CC) -c $(CFLAGS) $(LD_FLAGS) -o $@ $< \
	$(C_INCLUDE_PATH_FLAG)
	
# appCNNScene bins
$(APP_CNN_SL_BINS): %.bin: %.o $(APP_CNN_SL_AUX_OBJS)
	$(CC) $(CFLAGS) $(LD_FLAGS) -o $@ $^ \
	$(C_LIB_FLAG) $(C_LIB_PATH_FLAG)

# appCNNScene objs
$(APP_CNN_SL_OBJS): %.o: %.c $(APP_CNN_SL_HFILES)
	$(CC) -c $(CFLAGS) $(LD_FLAGS) -o $@ $< \
	$(C_INCLUDE_PATH_FLAG)

# appCNNConvertImageNet bins 
$(APP_CNN_CONVERT_IMAGENET_BINS): $(APP_CNN_CONVERT_IMAGENET_BIN_OBJS)
	$(CPPC) $(CFLAGS) $(LD_FLAGS) -o $(APP_CNN_CONVERT_IMAGENET_BINS) $^ \
	-lopencv_core -lopencv_highgui -lopencv_imgproc \
	$(C_LIB_PATH_FLAG)
	
# appCNNConvertImageNet objs
$(APP_CNN_CONVERT_IMAGENET_BIN_OBJS): %.o: %.cpp $(APP_CNN_CONVERT_IMAGENET_HFILES)
	$(CPPC) -c $(CFLAGS) $(LD_FLAGS) -o $@ $< \
	$(C_INCLUDE_PATH_FLAG)

# appCNNConvertSBD bins 
$(APP_CNN_CONVERT_SBD_BINS): $(APP_CNN_CONVERT_SBD_BIN_OBJS)
	$(CPPC) $(CPPFLAGS) $(LD_FLAGS) -o $(APP_CNN_CONVERT_SBD_BINS) $^ \
	-lopencv_core -lopencv_highgui -lopencv_imgproc \
	$(C_LIB_PATH_FLAG)
	
# appCNNConvertSBD objs
$(APP_CNN_CONVERT_SBD_BIN_OBJS): %.o: %.cpp $(APP_CNN_CONVERT_SBD_HFILES)
	$(CPPC) -c $(CFLAGS) $(LD_FLAGS) -o $@ $< \
	$(C_INCLUDE_PATH_FLAG)

# appCNNInteropCaffe bins
$(APP_CNN_INTEROP_CAFFE_BINS): $(APP_CNN_INTEROP_CAFFE_BIN_OBJS)
	$(CPPC) $(CFLAGS) $(LD_FLAGS) -o $(APP_CNN_INTEROP_CAFFE_BINS) $^ \
	-lprotobuf \
	$(C_LIB_FLAG) $(C_LIB_PATH_FLAG)

# appCNNInteropCaffe objs
$(APP_CNN_INTEROP_CAFFE_BIN_OBJS): %.o: %.cpp $(APP_CNN_INTEROP_CAFFE_HFILES)
	$(CPPC) -c $(CFLAGS) $(LD_FLAGS) -o $@ $< \
	$(C_INCLUDE_PATH_FLAG)
