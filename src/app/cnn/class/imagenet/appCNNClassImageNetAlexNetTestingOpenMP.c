/******************************************************************************/
/*!
 * \file appCNNClassImageNetAlexNetTesting.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../appCNNClass.h"


/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define MODEL_PATH       "../../../../database/model/alexnet/databaseModelAlexNet.m"
#define LABEL_PATH       "../../../../database/imagenet/test/label.txt"
#define IMAGE_PATH       "../../../../database/imagenet/test/%010d.jpg"
#define TOP_N            5
#define IMAGE_NUM        1000
#define IMAGE_ROW        256
#define IMAGE_COL        256
#define IMAGE_CHANNEL    3


/*******************************************************************************
 *
 * main()
 *
 ******************************************************************************/

int main()
{
   return appCNNClassImageNetAlexNetTestingOpenMP();
}


/******************************************************************************/
/*!
 * \ingroup appCNNClass
 * \brief   AlexNet classification testing example
 */
/******************************************************************************/

int appCNNClassImageNetAlexNetTestingOpenMP()
{
   int             i;
   int             j;
   int             read;
   int             err;
   int             success;
   float           classifyPercent;
   long            mem1, mem2, mem3;
   FILE            *fp;
   struct timespec startTime;
   struct timespec endTime;
   long            testingTime;
   timlUtilImage   image;
   int             testNum;
   int             dim;
   int             *testLabel;
   float           *testImage;
   char            str[TIML_UTIL_MAX_STR];
   int             topN;

   // init
   err     = 0;
   testNum = IMAGE_NUM;
   dim     = IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL;
   topN    = TOP_N;

   setbuf(stdout, NULL);  // do not buffer the console output

   // read CNN config
   printf("1. Read CNN config\n");
   timlConvNeuralNetwork *cnn = timlCNNReadFromFile(MODEL_PATH);
   timlCNNAddAccuracyLayer(cnn, TOP_N);
   timlCNNInitialize(cnn);
   timlCNNLoadParamsFromFile(cnn, cnn->paramsFileName);

   timlCNNPrint(cnn);
   mem1 = cnn->forwardMemory + cnn->backwardMemory + cnn->fixedMemory + cnn->paramsMemory;
   mem2 = cnn->forwardMemory + cnn->fixedMemory + cnn->paramsMemory;
   mem3 = cnn->memPoolSize + cnn->fixedMemory + cnn->paramsMemory;
   printf("CNN level 1 memory size = %10.4f MB.\n", (float)mem1/1024.0/1024.0);
   printf("CNN level 2 memory size = %10.4f MB.\n", (float)mem2/1024.0/1024.0);
   printf("CNN level 3 memory size = %10.4f MB.\n", (float)mem3/1024.0/1024.0);
   printf("CNN forward memory size = %10.4f MB.\n", (float)cnn->forwardMemory/1024.0/1024.0);
   printf("CNN memory pool size    = %10.4f MB.\n", (float)cnn->memPoolSize/1024.0/1024.0);
   printf("CNN params memory size  = %10.4f MB.\n", (float)cnn->paramsMemory/1024.0/1024.0);
   timlCNNSetMode(cnn, Util_Test);

   int thread = 4;
   timlConvNeuralNetwork **cnnTeam = malloc(sizeof(timlConvNeuralNetwork*)*thread);
   cnnTeam[0] = cnn;
   for (i = 1; i < thread; i++){
      cnnTeam[i] = timlCNNShareParams(cnnTeam[0]);
      timlCNNInitialize(cnnTeam[i]);
   }
   // read test images
   printf("2. Read test images\n");
   testLabel = malloc(sizeof(int)*testNum);
   testImage = malloc(sizeof(float)*IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL*testNum);

   // read labels
   fp = fopen(LABEL_PATH, "rt");
   for (i = 0; i < testNum; i++) {
      read = fscanf(fp, "%d", testLabel + i);
   }
   fclose(fp);

   // read images
   for (i = 0; i < testNum; i++) {
      sprintf(str, IMAGE_PATH, i);
      image = timlUtilReadJPEG(str);
      if (image.channel == 1) { // duplicate channels
         for (j = 0; j < IMAGE_CHANNEL; j++) {
            cblas_scopy(IMAGE_ROW*IMAGE_COL, image.data, 1, testImage + i*dim + j*IMAGE_ROW*IMAGE_COL, 1);
         }
      }
      else {
         cblas_scopy(dim, image.data, 1, testImage + i*dim, 1);
      }
      free(image.data);
   }

   // testing
   printf("3. Start testing\n");
   clock_gettime(CLOCK_REALTIME, &startTime);
   timlCNNClassifyAccuracyTeamModeOpenMP(cnnTeam, thread, testImage, IMAGE_ROW, IMAGE_COL, IMAGE_CHANNEL, testLabel, 1, 1, testNum, &success);
   clock_gettime(CLOCK_REALTIME, &endTime);

   testingTime = timlUtilDiffTime(startTime, endTime);
   classifyPercent = (float)success/(float)testNum;
   printf("Testing time          = %.2f s.\n", testingTime/1000000.0);
   printf("Top%1d success percent  = %.3f %%\n", topN, classifyPercent*100.00);

   // cleaning up
   printf("4. Clean up\n");
   for (i = 0; i < thread; i++){
      timlCNNDelete(cnnTeam[i]);
   }
   free(testLabel);
   free(testImage);

   return err;
}
