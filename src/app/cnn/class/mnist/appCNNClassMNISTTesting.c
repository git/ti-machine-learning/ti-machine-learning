/******************************************************************************/
/*!
 * \file appCNNClassMNISTTesting.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../appCNNClass.h"

/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define DATABASE_PATH    "../../../../database/mnist"
#define MODEL_PATH       "../../../../database/model/mnist/databaseModelMNIST.m"
#define TOP_N            1
#define TEST_NUM         10000
#define IMAGE_ROW        28
#define IMAGE_COL        28
#define IMAGE_CHANNEL    1

/*******************************************************************************
 *
 * main()
 *
 ******************************************************************************/

int main()
{
   return appCNNClassMNISTTesting();
}


/******************************************************************************/
/*!
 * \ingroup appCNNClass
 * \brief   MNIST classification testing example
 */
/******************************************************************************/

int appCNNClassMNISTTesting()
{
   int              err;
   int              classifyNum;
   float            classifyPercent;
   int              dim;
   long             mem;
   struct timespec  startTime;
   struct timespec  endTime;
   long             testingTime;
   timlUtilImageSet testing;
   timlUtilImageSet training;
   int              topN;
   size_t           mem1;
   size_t           mem2;
   size_t           mem3;

   err   = 0;
   topN  = TOP_N;

   dim   = IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL;
   setbuf(stdout, NULL); // do not buffer the console output

   // read CNN config
   printf("1. Read CNN config\n");
   timlConvNeuralNetwork *cnn = timlCNNReadFromFile(MODEL_PATH);
   timlCNNSetMode(cnn, Util_Test);
   timlCNNAddAccuracyLayer(cnn, 1);
   timlCNNInitialize(cnn);
   timlCNNLoadParamsFromFile(cnn, cnn->paramsFileName);
   timlCNNPrint(cnn);
   mem1 = cnn->forwardMemory + cnn->backwardMemory + cnn->fixedMemory + cnn->paramsMemory;
   mem2 = cnn->forwardMemory + cnn->fixedMemory + cnn->paramsMemory;
   mem3 = cnn->memPoolSize + cnn->fixedMemory + cnn->paramsMemory;
   printf("CNN level 1 memory size = %10.4f MB.\n", (float)mem1/1024.0/1024.0);
   printf("CNN level 2 memory size = %10.4f MB.\n", (float)mem2/1024.0/1024.0);
   printf("CNN level 3 memory size = %10.4f MB.\n", (float)mem3/1024.0/1024.0);
   printf("CNN forward memory size = %10.4f MB.\n", (float)cnn->forwardMemory/1024.0/1024.0);
   printf("CNN memory pool size    = %10.4f MB.\n", (float)cnn->memPoolSize/1024.0/1024.0);
   printf("CNN params memory size  = %10.4f MB.\n", (float)cnn->paramsMemory/1024.0/1024.0);

   // read MNIST database
   printf("2. Read MNIST database\n");
   timlUtilReadMNIST(DATABASE_PATH, &training, &testing);

   // testing
   printf("3. Start testing\n");
   clock_gettime(CLOCK_REALTIME, &startTime);
   timlCNNClassifyAccuracy(cnn, testing.data, IMAGE_ROW, IMAGE_COL, IMAGE_CHANNEL, testing.label, 1, 1, testing.num, &classifyNum);
   clock_gettime(CLOCK_REALTIME, &endTime);
   testingTime = timlUtilDiffTime(startTime, endTime);
   classifyPercent = (float)classifyNum/(float)testing.num;
   printf("Testing time      = %.3f s\n", testingTime/1000000.0);
   printf("Classify accuracy = %.3f %%\n", classifyPercent*100.00);

   // cleanup
   printf("3. Clean up\n");
   free(training.data);
   free(training.label);
   free(testing.data);
   free(testing.label);
   timlCNNDelete(cnn);

   return err;
}
