/******************************************************************************/
/*!
 * \file appCNNClassMNISTTraining.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../appCNNClass.h"


/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define DATABASE_PATH    "../../../../database/mnist"
#define TRAIN_NUM        60000
#define IMAGE_ROW        28
#define IMAGE_COL        28
#define BATCH_SIZE       100
#define IMAGE_CHANNEL    1
#define LEARN_RATE       0.1
#define EPOCH            10

/*******************************************************************************
 *
 * main()
 *
 ******************************************************************************/

int main()
{
   return appCNNClassMNISTTraining();
}


/******************************************************************************/
/*!
 * \ingroup appCNNClass
 * \brief   MNIST training example
 */
/******************************************************************************/

int appCNNClassMNISTTraining()
{
   int                  i, j;
   int                  dim;
   long                 mem;
   struct timespec      startTime;
   struct timespec      endTime;
   long                 trainingTime;
   timlUtilImageSet     training;
   timlUtilImageSet     testing;
   int                  err;
   int                  batchSize;
   int                  batchNum;
   timlCNNInputParams   inputParams;
   timlCNNConvParams    convParams;
   timlCNNPoolingParams poolingParams;

   // init
   dim       = IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL;
   err       = 0;
   batchSize = BATCH_SIZE;
   batchNum  = TRAIN_NUM/BATCH_SIZE;
   setbuf(stdout, NULL); // do not buffer the console output

   // setup CNN
   printf("1. Build up the CNN\n");
   timlConvNeuralNetwork *cnn = timlCNNCreateConvNeuralNetwork(timlCNNTrainingParamsDefault());
   cnn->params.learningRate = LEARN_RATE;
   cnn->params.maxBatchSize    = BATCH_SIZE;
   cnn->params.batchSize    = BATCH_SIZE;
   cnn->params.batchUpdate    = BATCH_SIZE;
   inputParams = timlCNNInputParamsDefault();
   inputParams.scale = 1.0/256.0;
   timlCNNAddInputLayer(cnn, IMAGE_ROW, IMAGE_COL, IMAGE_CHANNEL, inputParams);             // input layer
   convParams = timlCNNConvParamsDefault();
   convParams.kernelInit.type = Util_Xavier;
   timlCNNAddConvLayer(cnn, 5, 5, 1, 1, 20, convParams);                                    // conv layer
   poolingParams = timlCNNPoolingParamsDefault();
   timlCNNAddPoolingLayer(cnn, 2, 2, 2, 2, CNN_MaxPooling, poolingParams);                  // max pooling layer
   convParams = timlCNNConvParamsDefault();
   convParams.kernelInit.type = Util_Xavier;
   timlCNNAddConvLayer(cnn, 5, 5, 1, 1, 50, convParams);                                    // conv layer
   timlCNNAddPoolingLayer(cnn, 2, 2, 2, 2, CNN_MaxPooling, timlCNNPoolingParamsDefault());  // max pooling layer
   timlCNNAddLinearLayer(cnn, 500, timlCNNLinearParamsDefault());                           // linear layer
   timlCNNAddNonlinearLayer(cnn, Util_Relu);                                                // relu layer
   timlCNNAddLinearLayer(cnn, 10, timlCNNLinearParamsDefault());                            // linear layer
   timlCNNAddSoftmaxCostLayer(cnn);                                                         // softmax cost layer
   timlCNNInitialize(cnn);
   timlCNNReset(cnn);
   timlCNNPrint(cnn);

   // read MNIST database
   printf("2. Read the MNIST database\n");
   timlUtilReadMNIST(DATABASE_PATH, &training, &testing);

   // training
   printf("3. Start training\n");
   clock_gettime(CLOCK_REALTIME, &startTime);
   for (j =0; j < EPOCH; j++) {
      for (i = 0; i < batchNum; i++) {
         timlCNNSupervisedTrainingWithLabel(cnn, training.data + i*batchSize*dim, IMAGE_ROW, IMAGE_COL, IMAGE_CHANNEL, training.label + i*batchSize, 1, 1, batchSize);
      }
   }
   clock_gettime(CLOCK_REALTIME, &endTime);
   trainingTime = timlUtilDiffTime(startTime, endTime);
   printf("Training time = %.2f s.\n", trainingTime/1000000.0);

   // clean up
   printf("4. Clean up\n");
   free(training.data);
   free(training.label);
   free(testing.data);
   free(testing.label);
   timlCNNDelete(cnn);

   return err;
}

