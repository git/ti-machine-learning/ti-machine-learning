/******************************************************************************/
/*!
 * \file appCNNClassCIFAR10Testing.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../appCNNClass.h"


/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define MODEL_PATH       "../../../../database/model/cifar10/databaseModelCIFAR10.m"
#define DATABASE_PATH    "../../../../database/cifar10"
#define IMAGE_PATH       "../../../../database/cifar10/%1d.jpg"
#define LABEL_PATH       "../../../../database/cifar10/label.txt"
#define IMAGE_NUM        3
#define TOP_N            1
#define IMAGE_ROW        32
#define IMAGE_COL        32
#define IMAGE_CHANNEL    3


/*******************************************************************************
 *
 * main()
 *
 ******************************************************************************/

int main()
{
   return appCNNClassCIFAR10Testing();
}


/******************************************************************************/
/*!
 * \ingroup appCNNClass
 * \brief   CIFAR10 testing example
 */
/******************************************************************************/

int appCNNClassCIFAR10Testing()
{
   int              err;
   int              classifyNum;
   float            classifyPercent;
   int              dim;
   long             mem;
   struct timespec  startTime;
   struct timespec  endTime;
   long             testingTime;
   int              topN;
   timlUtilImageSet testing;
   size_t           mem1;
   size_t           mem2;
   size_t           mem3;
   FILE             *fp;
   timlUtilImage    image;
   char             str[TIML_UTIL_MAX_STR];

   // init
   int i;
   err         = 0;
   dim         = IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL;
   classifyNum = 0;
   topN        = TOP_N;

   setbuf(stdout, NULL); // do not buffer the console output

   // read the CNN config file
   printf("1. Read the CNN config\n");
   timlConvNeuralNetwork *cnn = timlCNNReadFromFile(MODEL_PATH);
   timlCNNAddAccuracyLayer(cnn, TOP_N);
   timlCNNInitialize(cnn);
   timlCNNLoadParamsFromFile(cnn, cnn->paramsFileName);
   timlCNNSetMode(cnn, Util_Test);
   timlCNNSetBatchSize(cnn, 1);
   timlCNNPrint(cnn);

   mem1 = cnn->forwardMemory + cnn->backwardMemory + cnn->fixedMemory + cnn->paramsMemory;
   mem2 = cnn->forwardMemory + cnn->fixedMemory + cnn->paramsMemory;
   mem3 = cnn->memPoolSize + cnn->fixedMemory + cnn->paramsMemory;
   printf("CNN level 1 memory size = %10.4f MB.\n", (float)mem1/1024.0/1024.0);
   printf("CNN level 2 memory size = %10.4f MB.\n", (float)mem2/1024.0/1024.0);
   printf("CNN level 3 memory size = %10.4f MB.\n", (float)mem3/1024.0/1024.0);
   printf("CNN forward memory size = %10.4f MB.\n", (float)cnn->forwardMemory/1024.0/1024.0);
   printf("CNN memory pool size    = %10.4f MB.\n", (float)cnn->memPoolSize/1024.0/1024.0);
   printf("CNN params memory size  = %10.4f MB.\n", (float)cnn->paramsMemory/1024.0/1024.0);

//   // read CIFAR10 database
//   printf("2. Read CIFAR10 database\n");
//   timlUtilReadCIFAR10(DATABASE_PATH, &training, &testing);
    testing.data  = malloc(sizeof(float)*IMAGE_ROW*IMAGE_COL*IMAGE_CHANNEL*IMAGE_NUM);
    testing.label = malloc(sizeof(int)*IMAGE_NUM);
    testing.num   = IMAGE_NUM;

    // read labels
    fp = fopen(LABEL_PATH, "rt");
    for (i = 0; i < IMAGE_NUM; i++) {
       fscanf(fp, "%d", testing.label + i);
    }
    fclose(fp);

    // read images
    for (i = 0; i < IMAGE_NUM; i++) {
       sprintf(str, IMAGE_PATH, i);
       image = timlUtilReadJPEG(str);
       cblas_scopy(dim, image.data, 1, testing.data + i*dim, 1);
       free(image.data);
    }

   // testing
   printf("3. Start testing\n");
   clock_gettime(CLOCK_REALTIME, &startTime);
   timlCNNClassifyAccuracy(cnn, testing.data, IMAGE_ROW, IMAGE_COL, IMAGE_CHANNEL, testing.label, 1, 1, testing.num, &classifyNum);
   clock_gettime(CLOCK_REALTIME, &endTime);
   testingTime = timlUtilDiffTime(startTime, endTime);
   classifyPercent = (float)classifyNum/(float)testing.num;
   printf("Testing time      = %.3f s\n", testingTime/1000000.0);
   printf("Classify accuracy = %.3f %%\n", classifyPercent*100.00);

   // cleaning
   printf("4. Clean up\n");
   free(testing.data);
   free(testing.label);
   timlCNNDelete(cnn);

   return err;
}
