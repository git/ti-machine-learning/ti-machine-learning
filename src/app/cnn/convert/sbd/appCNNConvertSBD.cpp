/******************************************************************************/
/*!
 * \file     appCNNConvertSBD.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 *  INCLUDES
 *
 ******************************************************************************/

#include "appCNNConvertSBD.hpp"


/******************************************************************************/
/*!
 * \ingroup appCNNConvertSBD
 * \brief   Convert Stanford Background database to have uniform size 240*320
 * \details argv[0] = program name
 *          argv[1] = original image folder
 *          argv[2] = original label file
 *          argv[3] = original image index file
 *          argv[4] = converted training folder
 *          argv[5] = converted testing folder
 *          argv[6] = training image number (optional)
 *          argv[7] = testing image number (optional)
 * \return  Error code
 */
/******************************************************************************/

int main(int argc, char *argv[]){

   int   i;
   int   j;
   int   k;
   int   trainConvertNum;
   int   testConvertNum;
   char  *trainConvertDir;
   char  *testConvertDir;
   char  *imageDir;
   char  *labelDir;
   char  *imageIdxFile;
   FILE  *fp;
   char  **imageName;
   int   buffer1;
   int   buffer2;
   float buffer3;
   char  str[NAME_BUFFER_SIZE];
   char  str2[NAME_BUFFER_SIZE];
   Mat   image;
   Mat   resizedImage;
   Mat   label;
   Mat   resizedLabel;
   int   read;
   Size  size(IMAGE_COL, IMAGE_ROW);

   // argument checking
   if (argc < 6 || argc > 8) { // argument number error
      return ERROR_APP_CNN_CONVERT_ARG;
   }
   // arg checking
   if (argc == 6) {
      trainConvertNum = SBD_TRAIN_NUM;
      testConvertNum  = SBD_TEST_NUM;
   }
   else if (argc == 7) {
      trainConvertNum = atoi(argv[6]);
      testConvertNum  = SBD_TEST_NUM;
   }
   else { // argc == 8
      trainConvertNum = atoi(argv[6]);
      testConvertNum  = atoi(argv[7]);
   }
   if ((trainConvertNum +  testConvertNum) > SBD_IMAGE_NUM || trainConvertNum < 0 || testConvertNum < 0) {
      printf("Image convert num error");
      return ERROR_APP_CNN_CONVERT_IMAGE_NUM;
   }

   // argument assignment
   imageDir        = argv[1];
   labelDir        = argv[2];
   imageIdxFile    = argv[3];
   trainConvertDir = argv[4];
   testConvertDir  = argv[5];

   imageName = (char**)malloc(sizeof(char*)*SBD_IMAGE_NUM);

   // read the image names
   fp = fopen(imageIdxFile, "rt");
   for (i = 0; i < SBD_IMAGE_NUM; i++) {
      imageName[i] = (char*)malloc(sizeof(char)*NAME_BUFFER_SIZE);
      read = fscanf(fp, "%s %d %d %f", imageName[i], &buffer1, &buffer2, &buffer3);
   }
   fclose(fp);

   // shuffle all image names
   appCNNConvertSBDShuffle(imageName, SBD_IMAGE_NUM);

   // training folder
   printf("Converting %d training images\n", trainConvertNum);
   mkdir(trainConvertDir, SBD_TRAIN_CONVERT_FOLDER_MODE);
   for(i = 0; i < trainConvertNum; i++) {
	  // images
      strcpy(str, imageDir);
      strcat(str, imageName[i]);
      strcat(str, ".jpg");
      image = imread(str);
      if(image.cols > image.rows){
         resize(image, resizedImage, size);
      }
      else{
    	 transpose(image, resizedImage);
    	 resize(resizedImage, resizedImage, size);
      }
      sprintf(str2, "/%03d.jpg", i);
      strcpy(str, trainConvertDir);
      strcat(str, str2);
      imwrite(str, resizedImage);

      // labels
      label = Mat(image.rows, image.cols, CV_8SC1, -1);
      strcpy(str, labelDir);
      strcat(str, imageName[i]);
      strcat(str, ".regions.txt");
      fp = fopen(str, "rt");
      for(j = 0; j < label.rows; j ++){
    	  for(k = 0; k < label.cols; k ++){
    		  read = fscanf(fp, "%d", &buffer1);
    		  label.at<int8_t>(j,k) = buffer1;
    	  }
      }
      fclose(fp);
      if(label.cols > label.rows){
         resize(label, resizedLabel, size, 0, 0,  INTER_NEAREST);
      }
      else{
    	 transpose(label, resizedLabel);
    	 resize(resizedLabel, resizedLabel, size, 0, 0, INTER_NEAREST);
      }
      sprintf(str2, "/%03d.txt", i);
      strcpy(str, trainConvertDir);
      strcat(str, str2);
      fp = fopen(str, "wt");
      for(j = 0; j < resizedLabel.rows; j ++){
         for(k = 0; k < resizedLabel.cols; k ++){
            read = fprintf(fp, "%d ", label.at<int8_t>(j,k));
    	 }
         read = fprintf(fp, "\n");
      }
      fclose(fp);
   }

   // testing folder
   printf("Converting %d testing images\n", testConvertNum);
   mkdir(testConvertDir, SBD_TEST_CONVERT_FOLDER_MODE);
   for(i = 0; i < testConvertNum; i++) {
	  // images
      strcpy(str, imageDir);
      strcat(str, imageName[i + trainConvertNum]);
      strcat(str, ".jpg");
      image = imread(str);
      if(image.cols > image.rows){
         resize(image, resizedImage, size);
      }
      else{
    	 transpose(image, resizedImage);
    	 resize(resizedImage, resizedImage, size);
      }
      sprintf(str2, "/%03d.jpg", i);
      strcpy(str, testConvertDir);
      strcat(str, str2);
      imwrite(str, resizedImage);

      // labels
      label = Mat(image.rows, image.cols, CV_8SC1, -1);
      strcpy(str, labelDir);
      strcat(str, imageName[i + trainConvertNum]);
      strcat(str, ".regions.txt");
      fp = fopen(str, "rt");
      for(j = 0; j < label.rows; j ++){
    	  for(k = 0; k < label.cols; k ++){
    		  read = fscanf(fp, "%d", &buffer1);
    		  label.at<int8_t>(j,k) = buffer1;
    	  }
      }
      fclose(fp);
      if(label.cols > label.rows){
         resize(label, resizedLabel, size, 0, 0,  INTER_NEAREST);
      }
      else{
    	 transpose(label, resizedLabel);
    	 resize(resizedLabel, resizedLabel, size, 0, 0, INTER_NEAREST);
      }
      sprintf(str2, "/%03d.txt", i);
      strcpy(str, testConvertDir);
      strcat(str, str2);
      fp = fopen(str, "wt");
      for(j = 0; j < resizedLabel.rows; j ++){
         for(k = 0; k < resizedLabel.cols; k ++){
            read = fprintf(fp, "%d ", label.at<int8_t>(j,k));
    	 }
         read = fprintf(fp, "\n");
      }
      fclose(fp);
   }
   // clean up
   for (i = 0; i < SBD_IMAGE_NUM; i++) {
      free(imageName[i]);
   }
   free(imageName);

   return 0;
}
