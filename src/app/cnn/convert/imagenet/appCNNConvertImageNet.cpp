/******************************************************************************/
/*!
 * \file     appCNNConvertImageNet.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 *  INCLUDES
 *
 ******************************************************************************/

#include "appCNNConvertImageNet.hpp"


/******************************************************************************/
/*!
 * \ingroup appCNNConvertImageNet
 * \brief   Convert ImageNet database to have uniform size 256*256
 * \details argv[0] = program name
 *          argv[1] = original training image folder
 *          argv[2] = original training label file
 *          argv[3] = converted training image folder
 *          argv[4] = original testing image folder
 *          argv[5] = original testing label file
 *          argv[6] = converted testing image folder
 *          argv[7] = training image number (optional)
 *          argv[8] = testing image number (optional)
 * \return  Error code
 */
/******************************************************************************/

int main(int argc, char *argv[])
{
   int  i;
   int  trainConvertNum;
   int  testConvertNum;
   char **trainImageName;
   int  *trainLabel;
   char **testImageName;
   int  *testLabel;
   char *trainConvertDir;
   char *testConvertDir;
   char *trainDir;
   char *testDir;
   char *trainLabelFile;
   char *testLabelFile;
   FILE *fp;
   char str[NAME_BUFFER_SIZE];
   char str2[NAME_BUFFER_SIZE];
   Mat  image;
   Mat  resizedImage;
   int  read;
   Size size(IMAGE_ROW, IMAGE_COL);

   // argument checking
   if (argc < 7 || argc > 9) { // argument number error
      return ERROR_APP_CNN_CONVERT_ARG;
   }
   // arg checking
   if (argc == 7) {
      trainConvertNum =  IMAGENET_2012_TRAIN_NUM;
      testConvertNum = IMAGENET_2012_VAL_NUM;
   }
   else if (argc == 8) {
      trainConvertNum = atoi(argv[7]);
      testConvertNum = IMAGENET_2012_VAL_NUM;
   }
   else { // argc == 9
      trainConvertNum = atoi(argv[7]);
      testConvertNum = atoi(argv[8]);
   }
   if (trainConvertNum > IMAGENET_2012_TRAIN_NUM || trainConvertNum < 0) {
      printf("Training image convert num error");
      return ERROR_APP_CNN_CONVERT_IMAGE_NUM;
   }
   if (testConvertNum > IMAGENET_2012_VAL_NUM || testConvertNum < 0) {
      printf("Testing Image Convert num error");
      return ERROR_APP_CNN_CONVERT_IMAGE_NUM;
   }

   // argument assignment
   trainDir        = argv[1];
   trainLabelFile  = argv[2];
   trainConvertDir = argv[3];
   testDir         = argv[4];
   testLabelFile   = argv[5];
   testConvertDir  = argv[6];

   trainImageName = (char**)malloc(sizeof(char*)*IMAGENET_2012_TRAIN_NUM);
   trainLabel = (int*)malloc(sizeof(int)*IMAGENET_2012_TRAIN_NUM);

   // read the training image names and labels
   fp = fopen(trainLabelFile, "rt");
   for (i = 0; i < IMAGENET_2012_TRAIN_NUM; i++) {
      trainImageName[i] = (char*)malloc(sizeof(char)*NAME_BUFFER_SIZE);
      read = fscanf(fp, "%s %d", trainImageName[i], trainLabel+i);
   }
   fclose(fp);

   // training folder
   printf("Converting %d training images\n", trainConvertNum);
   mkdir(trainConvertDir, IMAGENET_2012_TRAIN_CONVERT_FOLDER_MODE);
   strcpy(str, trainConvertDir);
   strcat(str, "label.txt");
   fp = fopen(str, "wt");
   appCNNConvertImageNetShuffle(trainImageName, trainLabel, IMAGENET_2012_TRAIN_NUM);
   for(i = 0; i < trainConvertNum; i++) {
      strcpy(str, trainDir);
      strcat(str, trainImageName[i]);
      image = imread(str);
      resize(image, resizedImage, size);
      sprintf(str2, "/%010d.jpg", i);
      strcpy(str, trainConvertDir);
      strcat(str, str2);
      imwrite(str, resizedImage);
      fprintf(fp, "%d\n", trainLabel[i]);
   }
   fclose(fp);

   // free training image names and labels
   for (i = 0; i < IMAGENET_2012_TRAIN_NUM; i++) {
      free(trainImageName[i]);
   }
   free(trainImageName);
   free(trainLabel);

   testImageName = (char**)malloc(sizeof(char*)*testConvertNum);
   testLabel = (int*)malloc(sizeof(int)*testConvertNum);

   // read the testing image names and labels
   fp = fopen(testLabelFile, "rt");
   for (i = 0; i < testConvertNum; i++) {
      testImageName[i] = (char*)malloc(sizeof(char)*NAME_BUFFER_SIZE);
      read = fscanf(fp, "%s %d", testImageName[i], testLabel+i);
   }
   fclose(fp);

   // testing folder
   printf("Converting %d testing images\n", testConvertNum);
   mkdir(testConvertDir, IMAGENET_2012_VAL_CONVERT_FOLDER_MODE);
   strcpy(str, testConvertDir);
   strcat(str, "label.txt");
   fp = fopen(str, "wt");
   for(i = 0; i < testConvertNum; i++) {
      strcpy(str, testDir);
      strcat(str, testImageName[i]);
      image = imread(str);
      resize(image, resizedImage, size);
      sprintf(str2, "/%010d.jpg", i);
      strcpy(str, testConvertDir);
      strcat(str, str2);
      imwrite(str, resizedImage);
      fprintf(fp, "%d\n", testLabel[i]);
   }
   fclose(fp);

   // free testing image names and labels
   for (i = 0; i < testConvertNum; i++) {
      free(testImageName[i]);
   }
   free(testImageName);
   free(testLabel);


   return 0;
}
