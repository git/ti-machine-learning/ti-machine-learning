#ifndef APPCNNCONVERTIMAGENET_HPP_
#define APPCNNCONVERTIMAGENET_HPP_


/******************************************************************************/
/*!
 * \file     appCNNConvertImageNet.hpp
 * \defgroup appCNNConvertImageNet appCNNConvertImageNet
 * \brief    ImageNet 2012 database conversion applications
 * \ingroup  appCNN
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 *  INCLUDES
 *
 ******************************************************************************/


#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cstdlib>

using namespace std;
using namespace cv;


/*******************************************************************************
 *
 *  DEFINES
 *
 ******************************************************************************/

typedef enum {
   ERROR_APP_CNN_CONVERT_ARG,
   ERROR_APP_CNN_CONVERT_IMAGE_NUM
} appCNNConvertError;

#define IMAGE_ROW                               256
#define IMAGE_COL                               256
#define RAND_SEED                               1
#define NAME_BUFFER_SIZE                        50

//#define IMAGENET_2012_TRAIN_IMAGE_PATH          "/media/eric/Eric/ILSVRC2012_img_train/"
//#define IMAGENET_2012_TRAIN_LABEL_FILE          "./train.txt"
#define IMAGENET_2012_TRAIN_NUM                 1281167
//#define IMAGENET_2012_TRAIN_CONVERT_FOLDER_NAME "./train"
#define IMAGENET_2012_TRAIN_CONVERT_FOLDER_MODE 0777
//#define IMAGENET_2012_TRAIN_CONVERT_LABEL       "./train/label.txt"

//#define IMAGENET_2012_VAL_IMAGE_PATH            "/media/eric/Eric/ILSVRC2012_img_val/"
//#define IMAGENET_2012_VAL_LABEL_FILE            "./val.txt"
#define IMAGENET_2012_VAL_NUM                   50000
//#define IMAGENET_2012_VAL_CONVERT_FOLDER_NAME   "./test"
#define IMAGENET_2012_VAL_CONVERT_FOLDER_MODE   0777
//#define IMAGENET_2012_VAL_CONVERT_LABEL         "./test/label.txt"


/*******************************************************************************
 *
 *  FUNCTION PROTOTYPES0
 *
 ******************************************************************************/

int appCNNConvertImageNetShuffle(char **names, int *labels, int n);

#endif /* APPCNNCONVERTIMAGENET_HPP_ */
