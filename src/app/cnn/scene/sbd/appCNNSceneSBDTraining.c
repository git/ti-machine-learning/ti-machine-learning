/******************************************************************************/
/*!
 * \file appCNNSceneSBDTraining.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "../appCNNScene.h"


/*******************************************************************************
 *
 * DEFINES
 *
 ******************************************************************************/

#define IMAGE_ROW        240
#define IMAGE_COL        320
#define IMAGE_CHANNEL    3
#define PATCH_SIZE       133
#define TRAIN_IMAGE_PATH "../../../../database/sbd/train/%03d.jpg"
#define TRAIN_IMAGE_NUM  450
#define TRAIN_LABEL_PATH "../../../../database/sbd/train/%03d.txt"


/*******************************************************************************
 *
 * main()
 *
 ******************************************************************************/

int main()
{
   return appCNNSceneSBDTraining();
}


/******************************************************************************/
/*!
 * \ingroup appCNNScene
 * \brief   Scene labeling training example
 */
/******************************************************************************/

int appCNNSceneSBDTraining()
{
   struct timespec       startTime;
   struct timespec       endTime;
   long                  trainingTime;
   int                   err;
   long                  mem;
   int                   resolutionLossRow;
   int                   resolutionLossCol;
   int                   resizeRow;
   int                   resizeCol;
   appCNNSceneDataSet    slTraining;
   timlConvNeuralNetwork *cnn;

   // set up database params
   err = 0;
   slTraining.num              = TRAIN_IMAGE_NUM;
   slTraining.row              = IMAGE_ROW;
   slTraining.col              = IMAGE_COL;
   slTraining.channel          = IMAGE_CHANNEL;
   slTraining.patchSize        = PATCH_SIZE;
   slTraining.imageFileNameStr = TRAIN_IMAGE_PATH;
   slTraining.labelFileNameStr = TRAIN_LABEL_PATH;

   setbuf(stdout, NULL); // do not buffer the console output

   // build up the CNN
   printf("1. Build up the CNN\n");
   cnn = timlCNNCreateConvNeuralNetwork(timlCNNTrainingParamsDefault());
   cnn->params.batchSize = 100;
   cnn->params.batchUpdate = 100;
   cnn->params.maxBatchSize = 100;
   cnn->params.allocatorLevel = Util_AllocatorLevel1;
   timlCNNAddInputLayer(cnn, PATCH_SIZE, PATCH_SIZE, IMAGE_CHANNEL, timlCNNInputParamsDefault());
   timlCNNAddConvLayer(cnn, 6, 6, 1, 1, 25, timlCNNConvParamsDefault());                    // conv layer
   timlCNNAddNonlinearLayer(cnn, Util_Tanh);                                                // tanh layer
   timlCNNAddPoolingLayer(cnn, 8, 8, 8, 8, CNN_MaxPooling, timlCNNPoolingParamsDefault());  // max pooling layer
   timlCNNAddConvLayer(cnn, 3, 3, 1, 1, 50, timlCNNConvParamsDefault());                    // conv layer
   timlCNNAddNonlinearLayer(cnn, Util_Tanh);                                                // tanh layer
   timlCNNAddPoolingLayer(cnn, 2, 2, 2, 2, CNN_MaxPooling, timlCNNPoolingParamsDefault());  // max pooling layer
   timlCNNAddLinearLayer(cnn, 8, timlCNNLinearParamsDefault());                             // linear layer
   timlCNNAddSoftmaxCostLayer(cnn);                                                         // softmax cost layer
   timlCNNInitialize(cnn);
   timlCNNReset(cnn);
   timlCNNPrint(cnn);

   // training
   printf("2. Start training\n");
   clock_gettime(CLOCK_REALTIME, &startTime);
   appCNNSceneSupervisedTraining(cnn, &slTraining);
   clock_gettime(CLOCK_REALTIME, &endTime);
   trainingTime = timlUtilDiffTime(startTime, endTime);
   printf("Training time = %.2fs\n", trainingTime/1000000.0);

   // resizing
   printf("3. Resize the feature maps\n");
   resolutionLossRow = 8*2; // resolution loss due to max pooling
   resolutionLossCol = 8*2; // resolution loss due to max pooling
   resizeRow = slTraining.row + (slTraining.patchSize/2)*2 - (resolutionLossRow - 1);
   resizeCol = slTraining.col + (slTraining.patchSize/2)*2 - (resolutionLossCol - 1);
   timlCNNResize(cnn, resizeRow, resizeCol, slTraining.channel);

   // clean up
   printf("4. Clean up\n");
   timlCNNDelete(cnn);
   return err;

}

