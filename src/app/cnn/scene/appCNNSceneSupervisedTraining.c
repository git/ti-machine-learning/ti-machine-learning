/******************************************************************************/
/*!
 * \file appCNNSceneSupervisedTraining.c
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "appCNNScene.h"


/******************************************************************************/
/*!
 * \ingroup       appCNNScene
 * \brief         Supervised training on the dataset
 * \param[in,out] cnn     CNN
 * \param[in]     dataSet Data set
 * \return        Error code
 */
/******************************************************************************/

int appCNNSceneSupervisedTraining(timlConvNeuralNetwork *cnn, appCNNSceneDataSet *dataSet)
{

   int          i;
   int          j;
   int          index;
   int          err;
   timlCNNLayer *bpStartLayer;
   int          *label;
   int          labelTemp;
   int          iter;
   int          patchDim;
   int          *imageIdx;
   int          *rowIdx;
   int          *colIdx;
   float        *patch;
   int          epoch;
   int          batchUpdate;
   int          batchNum;
   float        cost;
   float        batchCost;
   int          batchIndex;

   // init
   err        = 0;
   iter       = dataSet->row*dataSet->col*dataSet->num;
   patchDim   = dataSet->channel*dataSet->patchSize*dataSet->patchSize;
   imageIdx   = malloc(sizeof(int)*iter);
   rowIdx     = malloc(sizeof(int)*iter);
   colIdx     = malloc(sizeof(int)*iter);
   patch      = malloc(sizeof(float)*patchDim*cnn->params.batchUpdate);
   label      = malloc(sizeof(int)*cnn->params.batchUpdate);
   epoch      = cnn->params.epoch;
   batchUpdate  = cnn->params.batchUpdate;
   batchNum   = iter/batchUpdate;
   batchIndex = 0;
   batchCost  = 0;
   index      = 0;

   // shuffle the training pixels
   appCNNSceneShuffleIdx(imageIdx, rowIdx, colIdx, dataSet);

   // training loop

   for (i = 0; i < epoch; i++) {
      for (j = 0; j < iter; j++) {
         labelTemp = appCNNSceneGetLabel(imageIdx[j], rowIdx[j], colIdx[j], dataSet);
         // make image and label batch
         if (labelTemp != -1) {
            label[index] = labelTemp;
            appCNNSceneGetPatch(imageIdx[j], rowIdx[j], colIdx[j], dataSet, patch + index*patchDim);
            index ++;
         }
         // call cnn training
         if (index == cnn->params.batchUpdate) {
            timlCNNSupervisedTrainingWithLabel(cnn, patch, dataSet->patchSize, dataSet->patchSize, dataSet->channel, label, 1, 1, cnn->params.batchUpdate);
            index = 0;
         }
      }
   }

   free(imageIdx);
   free(rowIdx);
   free(colIdx);

   return err;

}

