#ifndef APPCNNSCENE_H_
#define APPCNNSCENE_H_


/******************************************************************************/
/*!
 * \file     appCNNScene.h
 * \defgroup appCNNScene appCNNScene
 * \ingroup  appCNN
 * \brief    CNN scene labeling application
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "timl.h"


/*******************************************************************************
 *
 * STRUCTURE DEFINITIONS
 *
 ******************************************************************************/

//! \ingroup appCNNScene
//@{

typedef struct {
   int        num;
   int        row;
   int        col;
   int        channel;
   const char *imageFileNameStr;
   const char *labelFileNameStr;
   int        patchSize; /**< image patch(square) size */
} appCNNSceneDataSet;


/*******************************************************************************
 *
 * FUNCTION PROTOTYPES
 *
 ******************************************************************************/

float appCNNSceneAccuracy(int *labelMatrix, int *trueLabelMatrix, int dim);

int appCNNSceneSupervisedTraining(timlConvNeuralNetwork *cnn, appCNNSceneDataSet *dataSet);

int appCNNSceneSBDTraining();

int appCNNSceneSBDTesting();

int appCNNSceneClassify(timlConvNeuralNetwork *cnn, float *image, int row, int col, int channel, int *labelMatrix, int scale);

int appCNNSceneShuffleIdx(int *imageIdx, int *rowIdx, int *colIdx, appCNNSceneDataSet *dataSet);

int appCNNSceneGetLabel(int imageIdx, int rowIdx, int colIdx, appCNNSceneDataSet *dataSet);

int appCNNSceneGetPatch(int imageIdx, int rowIdx, int colIdx, appCNNSceneDataSet *dataSet, float *patch);

int appCNNSceneClassifyOpenMP(timlConvNeuralNetwork **cnnTeam, int teamNum, float *data, int row, int col, int channel, int *labelMatrix, int scale);

int appCNNSceneLabelMatrix(float *map, int row, int col, int channel, int m, int k, int *labelMatrix, int numRow, int numCol);

//@}

#endif /* APPCNNSCENE_H_ */
