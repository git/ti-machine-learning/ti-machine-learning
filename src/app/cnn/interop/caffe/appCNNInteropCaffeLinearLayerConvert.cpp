/******************************************************************************/
/*!
 * \file appCNNInteropCaffeLinearLayerConvert.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRIC * \param[in]     m Rows
 * \param[in]     n ColsT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 *  INCLUDES
 *
 ******************************************************************************/

#include "appCNNInteropCaffe.hpp"


/******************************************************************************/
/*!
 * \ingroup   appCNNInteropCaffe
 * \brief     Caffe linear layer conversion
 * \param[in] cnn            CNN
 * \param[in] layerStructure Layer structure
 * \param[in] layerParam     Layer param
 * \return    Error code
 */
/******************************************************************************/

int appCNNInteropCaffeLinearLayerConvert(timlConvNeuralNetwork *cnn, LayerParameter layerStructure, LayerParameter layerParam)
{
   int          dim;
   int          size;
   timlCNNLayer *layer;
   int          i;

   dim = layerStructure.inner_product_param().num_output();
   timlCNNAddLinearLayer(cnn, dim, timlCNNLinearParamsDefault());
   layer = cnn->tail;
   timlCNNLinearInitialize(layer);

   // weight
   size = layerParam.blobs(0).data_size();
   for (i = 0; i < size; i++) {
      layer->linearParams.weight[i] = layerParam.blobs(0).data(i);
   }

   // bias
   size = layerParam.blobs(1).data_size();
   for (i = 0; i < size; i++) {
      layer->linearParams.bias[i] = layerParam.blobs(1).data(i);
   }

   return 0;
}
