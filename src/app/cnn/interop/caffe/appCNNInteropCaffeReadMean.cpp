/******************************************************************************/
/*!
 * \file appCNNInteropCaffeReadMean.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRIC * \param[in]     m Rows
 * \param[in]     n ColsT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 *  INCLUDES
 *
 ******************************************************************************/

#include "appCNNInteropCaffe.hpp"


/******************************************************************************/
/*!
 * \ingroup   appCNNInteropCaffe
 * \brief     Read Caffe mean binary file
 * \param[in] layer    Input layer ptr
 * \param[in] fileName File name
 * \return    Error code
 */
/******************************************************************************/

int appCNNInteropCaffeReadMean(timlCNNLayer *layer, const char *fileName)
{
   BlobProto blob;
   int       i;
   int       row;
   int       col;
   int       channel;

   appCNNInteropCaffeReadProtoFromBinaryFile(fileName, &blob);
   channel = blob.channels();
   row   = blob.height();
   col    = blob.width();

   timlUtilFreeAcc(layer->inputParams.mean);
   timlUtilMallocAcc((void**)&(layer->inputParams.mean), sizeof(float)*row*col*channel);
   layer->inputParams.row = row;
   layer->inputParams.col = col;
   layer->inputParams.channel = channel;

   for (i = 0; i < channel*col*row; i++) {
      layer->inputParams.mean[i] = blob.data(i);
   }
   appCNNInteropCaffePermuteMean(layer->inputParams.mean, row, col, channel);

   return 0;
}
