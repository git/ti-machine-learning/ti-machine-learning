/******************************************************************************/
/*!
 * \file appCNNInteropCaffeConvLayerConvert.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 *  INCLUDES
 *
 ******************************************************************************/

#include "appCNNInteropCaffe.hpp"


/******************************************************************************/
/*!
 * \ingroup   appCNNInteropCaffe
 * \brief     Convert Caffe conv layer
 * \param[in] cnn CNN
 * \param[in] layerStructure Layer structure
 * \param[in] layerParam     Layer params
 * \return    Error code
 */
/******************************************************************************/

int appCNNInteropCaffeConvLayerConvert(timlConvNeuralNetwork *cnn, LayerParameter layerStructure, LayerParameter layerParam)
{
   float             *kernel;
   int               pad;
   int               stride;
   int               kernelSize;
   int               channel;
   int               group;
   int               size;
   timlCNNLayer      *layer;
   timlCNNConvParams params;

   pad        = layerStructure.convolution_param().pad();
   stride     = layerStructure.convolution_param().stride();
   kernelSize = layerStructure.convolution_param().kernel_size();
   channel    = layerStructure.convolution_param().num_output();
   group      = layerStructure.convolution_param().group();

   params          = timlCNNConvParamsDefault();
   params.padUp    = pad;
   params.padDown  = pad;
   params.padLeft  = pad;
   params.padRight = pad;
   params.type     = Util_Corr2D;

   // add conv layer
   timlCNNAddConvLayer(cnn, kernelSize, kernelSize, stride, stride, channel, params);
   layer = cnn->tail;
   timlCNNConvInitialize(layer);

   // read kernel
   size = layerParam.blobs(0).data_size();
   kernel = (float*)malloc(sizeof(float)*size);
   for (int i = 0; i < size; i++) {
      kernel[i] = layerParam.blobs(0).data(i);
   }

   // convert grouped kernel
   appCNNInteropCaffeFillBlockDiagonalMatrix(layer->convParams.kernel, layer->convParams.outputFeatureMapChannel, layer->convParams.inputFeatureMapChannel*layer->convParams.kernelRow*layer->convParams.kernelCol, group, kernel);
   free(kernel);

//   // flip all the kernels
//   if (layer->convParams.type  == Util_Conv2D) {
//      appCNNInteropCaffeFlipKernelMatrix(layer->convParams.kernel, kernelSize, kernelSize, layer->convParams.inputFeatureMapChannel, layer->convParams.outputFeatureMapChannel);
//   }

   // read bias
   size = layerParam.blobs(1).data_size();
   for (int i = 0; i < size; i++) {
      layer->convParams.bias[i] = layerParam.blobs(1).data(i);
   }

   return 0;
}
