/******************************************************************************/
/*!
 * \file     appCNNInteropCaffe.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/


/*******************************************************************************
 *
 *  INCLUDES
 *
 ******************************************************************************/

#include "appCNNInteropCaffe.hpp"


/*******************************************************************************
 *
 *  DEFINES
 *
 ******************************************************************************/

#define FLOAT_FORMAT "%12.6f"
#define INT_FORMAT   "%5d"

/******************************************************************************/
/*!
 * \ingroup appCNNInteropCaffe
 * \brief   Caffe to TIML CNN model converter
 * \details argv[0] = program name
 *          argv[1] = saved timl CNN model file name
 *          argv[2] = Caffe model text file name
 *          argv[3] = Caffe model binary file name
 *          argv[4] = Caffe mean binary file name (optional)
 * \return  Error code
 */
/******************************************************************************/

int main(int argc, char *argv[])
{

   timlConvNeuralNetwork *cnn;
   // arg checking
   if (argc < 4 || argc > 5) { // argument error
      return ERROR_APP_CNN_INTEROP_ARG;
   }

   cnn = appCNNInteropCaffeConvert(argv[2], argv[3]);
   appCNNInteropCaffeConvLayerPermuteKernel(cnn->head->next);

   if (argc == 5) { // include mean file
      appCNNInteropCaffeReadMean(cnn->head, argv[4]);
   }

   timlCNNWriteToFile(argv[1], cnn, Util_ParamsLevel2, "cnn", FLOAT_FORMAT, INT_FORMAT);
   timlCNNDelete(cnn);
}

